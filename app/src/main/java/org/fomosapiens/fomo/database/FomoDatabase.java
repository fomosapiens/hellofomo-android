package org.fomosapiens.fomo.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.content.Context;

import org.fomosapiens.fomo.database.news_favor.NewsFavorDao;
import org.fomosapiens.fomo.database.news_favor.NewsFavorEnity;

/**
 * Created by HuyMTB on 7/18/18.
 */

@Database(entities = {NewsFavorEnity.class}, version = 1)
public abstract class FomoDatabase extends android.arch.persistence.room.RoomDatabase {

    public abstract NewsFavorDao newsFavorDao();

    private static FomoDatabase INSTANCE;

    public static FomoDatabase getDatabase(Context context) {
        if (INSTANCE == null) {
            INSTANCE = Room.databaseBuilder(context.getApplicationContext(), FomoDatabase.class, "fomo_db").build();
        }

        return INSTANCE;
    }
}
