package org.fomosapiens.fomo.database.news_favor;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Maybe;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

/**
 * Created by HuyMTB on 7/18/18.
 */
@Dao
public interface NewsFavorDao {

    @Query("SELECT * FROM newsfavor")
    Flowable<List<NewsFavorEnity>> getAllNewsFavor();

    @Query("SELECT * FROM newsfavor WHERE news_id = :news_id")
    Flowable<NewsFavorEnity> newsFavorInfo(long news_id);

    @Insert(onConflict = REPLACE)
    void insertNewsFavor(NewsFavorEnity newsFavorEnity);

    @Delete
    void deleteNewsFavor(NewsFavorEnity newsFavorEnity);
}
