package org.fomosapiens.fomo.database.news_favor;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

/**
 * Created by HuyMTB on 7/18/18.
 */
@Entity(tableName = "newsfavor")
public class NewsFavorEnity {

    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "news_id")
    private long news_id;

    @ColumnInfo(name = "title")
    private String title;

    @ColumnInfo(name = "introduction")
    private String introduction;

    @ColumnInfo(name = "description")
    private String description;

    @ColumnInfo(name = "available_date")
    private String available_date;

    @ColumnInfo(name = "primary_image")
    private String primary_image;

    public NewsFavorEnity() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getNews_id() {
        return news_id;
    }

    public void setNews_id(long news_id) {
        this.news_id = news_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIntroduction() {
        return introduction;
    }

    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAvailable_date() {
        return available_date;
    }

    public void setAvailable_date(String available_date) {
        this.available_date = available_date;
    }

    public String getPrimary_image() {
        return primary_image;
    }

    public void setPrimary_image(String primary_image) {
        this.primary_image = primary_image;
    }
}
