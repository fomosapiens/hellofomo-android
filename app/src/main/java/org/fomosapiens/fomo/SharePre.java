package org.fomosapiens.fomo;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by PC162 on 8/7/2017.
 */

public class SharePre {
    private final static String KEY_DATA_NAME = "Fomo";
    public final static String KEY_ACCESS_TOKEN = "access_token";
    public final static String KEY_REFRESH_TOKEN = "refresh_token";
    public final static String KEY_SAVE_NEWS = "news_favor_";
    public final static String KEY_FONT_TYPE = "font_type";
    public final static String KEY_NOTIFICATION = "notification_on_off";
    public final static String KEY_FONT_SIZE = "news_font_size";
    public final static String KEY_USERNAME = "username";
    public final static String KEY_PASSWORD = "password";
    public final static String KEY_STORE_PASS = "save_pass";
    public final static String KEY_IS_LOGIN = "is_login";
    public final static String KEY_IS_LEGALLY_REGISTER = "is_legally_register";
    public final static String KEY_CUSTOMER_ACCESS_TOKEN = "customer_access_token";

    private static Context context;

    public SharePre(Context context){
        this.context = context;
    }

    public static void saveString(String key, String container) {
        SharedPreferences sharePre = context.getSharedPreferences(KEY_DATA_NAME, context.MODE_PRIVATE);
        SharedPreferences.Editor edit = sharePre.edit();
        edit.putString(key, container);
        edit.apply();
    }

    public static String getString(String key){
        SharedPreferences sharePre = context.getSharedPreferences(KEY_DATA_NAME, context.MODE_PRIVATE);
        return sharePre.getString(key, "");
    }

    public static void saveInt(String key, int value) {
        SharedPreferences sharePre = context.getSharedPreferences(KEY_DATA_NAME, context.MODE_PRIVATE);
        SharedPreferences.Editor edit = sharePre.edit();
        edit.putInt(key, value);
        edit.apply();
    }

    public static int getInt(String key) {
        SharedPreferences sharePre = context.getSharedPreferences(KEY_DATA_NAME, context.MODE_PRIVATE);
        return sharePre.getInt(key, 0);
    }

    public static void saveBoolean(String key, boolean value) {
        SharedPreferences sharePre = context.getSharedPreferences(KEY_DATA_NAME, context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharePre.edit();
        editor.putBoolean(key, value).apply();
    }

    public static Boolean getBoolean(String key) {
        SharedPreferences sharePre = context.getSharedPreferences(KEY_DATA_NAME, context.MODE_PRIVATE);
        boolean value = sharePre.getBoolean(key, true);
        return value;
    }

    public static Boolean getBooleanFalse(String key) {
        SharedPreferences sharePre = context.getSharedPreferences(KEY_DATA_NAME, context.MODE_PRIVATE);
        boolean value = sharePre.getBoolean(key, false);
        return value;
    }

}
