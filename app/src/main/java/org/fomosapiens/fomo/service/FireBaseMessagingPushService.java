package org.fomosapiens.fomo.service;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;

import org.fomosapiens.fomo.BuildConfig;
import org.fomosapiens.fomo.R;
import org.fomosapiens.fomo.SharePre;
import org.fomosapiens.fomo.Utils;
import org.fomosapiens.fomo.model.Login;
import org.fomosapiens.fomo.net.APIService;
import org.fomosapiens.fomo.net.APIUtils;
import org.fomosapiens.fomo.net.entity.RefreshTokenEntity;
import org.fomosapiens.fomo.view.ui.newsdetail.NewsDetailActivity;
import org.fomosapiens.fomo.view.ui.newsdetail.RefactorNewsDetailActivity;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by PC162 on 7/30/2017.
 *
 */

public class FireBaseMessagingPushService extends FirebaseMessagingService {

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        createNotification(remoteMessage);
    }


    private void createNotification(RemoteMessage remoteMessage)  {
        Context context = getApplicationContext();
        Map<String, String> params = remoteMessage.getData();
        if (params == null) {
            return;
        }

        try {
            long id = -1;
            if (params.size() != 0) {
                JSONObject object = new JSONObject(params.get("data"));
                id = object.optLong("post_id");
            }

            String title = remoteMessage.getNotification().getTitle();
            String body = remoteMessage.getNotification().getBody();

            Intent notificationIntent = new Intent();
            if (id != -1) {
                notificationIntent = new Intent(context, RefactorNewsDetailActivity.class);
                notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                notificationIntent.putExtra(Utils.KEY_NEWS_ID, id);
            }

            PendingIntent contentIntent = PendingIntent.getActivity(this, 0, notificationIntent, PendingIntent.FLAG_ONE_SHOT);
            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

            String CHANNEL_ID = "channel_id";
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                NotificationChannel notificationChannel = new NotificationChannel(CHANNEL_ID, "My Notifications", NotificationManager.IMPORTANCE_HIGH);

                // Configure the notification channel.
                notificationChannel.setDescription("Channel description");
                notificationChannel.enableLights(true);
                notificationChannel.setVibrationPattern(new long[]{0, 1000, 500, 1000});
                notificationChannel.enableVibration(true);
                notificationManager.createNotificationChannel(notificationChannel);
            }

            android.support.v4.app.NotificationCompat.Builder mBuilder = new android.support.v4.app.NotificationCompat.Builder(this, CHANNEL_ID)
                    .setContentTitle(title)
                    .setContentText(body)
                    .setContentIntent(contentIntent)
                    .setAutoCancel(true);

            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                mBuilder.setSmallIcon(R.drawable.ic_notification);
                mBuilder.setColor(getResources().getColor(R.color.transparent));
            } else {
                mBuilder.setSmallIcon(R.drawable.ic_notification);
            }

            notificationManager.notify(1234, mBuilder.build());

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onNewToken(String s) {
//        super.onNewToken(s);
        sendRegistrationToServer(s);
    }

    private void sendRegistrationToServer(String token) {

        RefreshTokenEntity refreshTokenEntity = new RefreshTokenEntity.Builder()
                .setRefresh_token(token)
                .setClient_id(BuildConfig.CLIENT_ID)
                .setClient_secret(BuildConfig.CLIENT_SECRET).build();

        APIService service = APIUtils.getAPIService();
        service.refreshToken(refreshTokenEntity)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Login>() {
                    @Override
                    public void onNext(@NonNull Login login) {
                        saveLogin(login);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    private void saveLogin(Login login){
        SharePre.saveString(SharePre.KEY_ACCESS_TOKEN, login.getAccess_token());
        SharePre.saveString(SharePre.KEY_REFRESH_TOKEN, login.getRefresh_token());
    }

}
