package org.fomosapiens.fomo.service;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import org.fomosapiens.fomo.BuildConfig;
import org.fomosapiens.fomo.SharePre;
import org.fomosapiens.fomo.model.Login;
import org.fomosapiens.fomo.net.APIService;
import org.fomosapiens.fomo.net.APIUtils;
import org.fomosapiens.fomo.net.entity.RefreshTokenEntity;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by PC162 on 7/30/2017.
 */

public class FireBaseInstanceIDPushService extends FirebaseInstanceIdService {
    @Override
    public void onTokenRefresh() {
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        sendRegistrationToServer(refreshedToken);
    }

    private void sendRegistrationToServer(String token) {

        RefreshTokenEntity refreshTokenEntity = new RefreshTokenEntity.Builder()
                .setRefresh_token(token)
                .setClient_id(BuildConfig.CLIENT_ID)
                .setClient_secret(BuildConfig.CLIENT_SECRET).build();

        APIService service = APIUtils.getAPIService();
        service.refreshToken(refreshTokenEntity)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Login>() {
                    @Override
                    public void onNext(@NonNull Login login) {
                        saveLogin(login);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    private void saveLogin(Login login){
        SharePre.saveString(SharePre.KEY_ACCESS_TOKEN, login.getAccess_token());
        SharePre.saveString(SharePre.KEY_REFRESH_TOKEN, login.getRefresh_token());
    }
}
