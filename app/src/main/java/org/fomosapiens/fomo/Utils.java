package org.fomosapiens.fomo;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.provider.Settings;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import org.fomosapiens.fomo.model.Author;
import org.fomosapiens.fomo.view.customview.FontChange;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by PC162 on 8/7/2017.
 */

public class Utils {

    public static final String KEY_RIGHT_MENU_POSITION = "kjsfdfsd";
    public static final String KEY_NEWS_ID = "sdhfask";
    public static final String KEY_CATEGORY_LIST = "lsdvcsdkj";
    public static final String KEY_CATEGORY_BUNDLE = "ksjdhvksdjvkjasnv";
    public static final String KEY_CATEGORY_FAVOR = "key_category_favor";
    public static final String KEY_FILTER_OPTION = "key_filter_option";
    public static final String KEY_SERVICE_DETAIL = "key_service_detail";

    public static final String KEY_CATEGORY_TYPE_ALL = "all";
    public static final int KEY_PUSH_REQUEST_CODE = 02222;
    public static final int KEY_REQUEST_OPEN_NEWS_DETAIL = 1000;
    public static final String registerUrl = "https://veranstaltungen.rgc-manager.de/api/v2/getlpregister";

    public static String FONT_NAME = "";

    public static String getDeviceId(Context context) {
        return Settings.Secure.getString(context.getContentResolver(),
                Settings.Secure.ANDROID_ID);
    }

    public static String getLanguage() {
        String language = Locale.getDefault().getLanguage();
        return language.equals("en") ? language : "de";
    }

    public static String getAuthorName(List<Author> authors) {
        String authorName = "";
        for (Author author : authors) {
            if (authors.indexOf(author) == 0)
                authorName = author.getName();
            else {
                authorName = authorName + " ";
                authorName = authorName + author.getName();
            }

        }
        return authorName;
    }

    public static boolean getIsShowPicture(String isShow) {
        return isShow.equals("0") ? false : true;
    }

    public static String getIdFromUrl(String url) {

        String keyId = "=";
        if (url == null || url.equals("") || url.equals(" "))
            return null;

        int cut = url.lastIndexOf(keyId);

        if (cut == -1)
            return null;

        String id = url.substring(cut + 1, url.length());
        return id;
    }

    public static String getDate(String date) {
        String space = " ";
        if (date == null || date.equals(""))
            return space;

        int cut = date.indexOf(space);
        String mDate = date.substring(0, cut);
        return mDate;
    }


    public static String getHour(String date) {
        String space = " ";
        if (date == null || date.equals(""))
            return space;
        int cut = date.indexOf(space);
        String mDate = date.substring(cut + 1, date.length());
        return mDate;
    }

    public static String getFileName(String fileName) {
        String[] cutString = fileName.split("/");
        String mFileName = cutString[cutString.length - 1];
        return mFileName;
    }

    public static String convertFontName(String fontName) {
        String replaceSpace = fontName.replace(" ", "");
        String replaceCross = replaceSpace.replace("-", "_");
        String lowerText = replaceCross.toLowerCase();
        String name = lowerText + ".ttf";
        return name;
    }

    public static List<String> listFontName() {
        List<String> stringList = new ArrayList<>();
        stringList.add("arial.ttf");
        stringList.add("arialhebrew.ttf");
        stringList.add("arialhebrew_bold.ttf");
        stringList.add("courier.ttf");
        stringList.add("courier_bold.ttf");
        stringList.add("helvetica.ttf");
        stringList.add("titillium.ttf");
        stringList.add("open_sans.ttf");
        stringList.add("lato_bold.ttf");
        stringList.add("lato.ttf");
        stringList.add("lato_light.ttf");
        stringList.add("work_sans.ttf");
        return stringList;
    }

    public static void showProgressDialog(Dialog dialog) {
        dialog.show();
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_fragment_loading);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }

    public static void hideProgressDialog(Dialog dialog) {
        if (dialog.isShowing()) {
            dialog.dismiss();
        }
    }

    public static void hideKeyboard(Context context, EditText editText) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.hideSoftInputFromWindow(editText.getWindowToken(),
                    InputMethodManager.RESULT_UNCHANGED_SHOWN);
        }
    }

    public static void functionLogout() {
        SharePre.saveBoolean(SharePre.KEY_IS_LOGIN, false);
        SharePre.saveInt(SharePre.KEY_IS_LEGALLY_REGISTER, 0);
        SharePre.saveString(SharePre.KEY_CUSTOMER_ACCESS_TOKEN, "");
    }


    public static void setFontName(Context context, ViewGroup viewGroup) {
        for (int i = 0; i < Utils.listFontName().size(); i++) {
            if (Utils.FONT_NAME.equals(Utils.listFontName().get(i))) {
                FontChange fontChange = new FontChange(context.getAssets(), "fonts/" + Utils.FONT_NAME);
                fontChange.replaceFonts(viewGroup);
            }
        }
    }

    public static String getAccessToken() {
        String deviceToken = SharePre.getString(SharePre.KEY_ACCESS_TOKEN);
        String customerToken = SharePre.getString(SharePre.KEY_CUSTOMER_ACCESS_TOKEN);
        String accessToken;
        if (customerToken != null && !customerToken.equals("")) {
            accessToken = customerToken;
        } else {
            accessToken = deviceToken;
        }
        return accessToken;
    }
}
