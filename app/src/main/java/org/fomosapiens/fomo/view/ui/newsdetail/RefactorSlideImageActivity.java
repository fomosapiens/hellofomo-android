package org.fomosapiens.fomo.view.ui.newsdetail;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import org.fomosapiens.fomo.R;
import org.fomosapiens.fomo.view.ui.BaseActivity;

import java.util.ArrayList;
import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;

public class RefactorSlideImageActivity extends BaseActivity {

    @BindView(R.id.refactor_slide_image_close)
    ImageView closeImg;
    @BindView(R.id.refactor_slide_image_view_pager)
    ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.refactor_activity_slide_image);
        ButterKnife.bind(this);

        List<String> imageList = (ArrayList<String>) getIntent().getSerializableExtra("image_list");
        int position = getIntent().getIntExtra("image_position", 0);

        AdapterSlidingImage adapterSlidingImage = new AdapterSlidingImage(imageList);
        viewPager.setAdapter(adapterSlidingImage);
        viewPager.setCurrentItem(position);

        closeImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    class AdapterSlidingImage extends PagerAdapter {

        private List<String> imageList;

        public AdapterSlidingImage(List<String> imageList) {
            this.imageList = imageList;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {

            View view = LayoutInflater.from(getBaseContext()).inflate(R.layout.refactor_sliding_image_item, container, false);
            assert view != null;
            ImageView imageView = (ImageView) view.findViewById(R.id.refactor_sliding_image);
            Glide.with(getBaseContext()).load(imageList.get(position)).into(imageView);
            container.addView(view);
            return view;
        }

        @Override
        public int getCount() {
            return imageList.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view.equals(object);
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }
    }
}
