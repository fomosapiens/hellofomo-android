package org.fomosapiens.fomo.view.customview;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.fomosapiens.fomo.R;
import org.fomosapiens.fomo.Utils;
import org.fomosapiens.fomo.model.News;
import org.fomosapiens.fomo.view.interfacelistener.OnClickNewsDetailListener;
import org.fomosapiens.fomo.view.ui.ViewUtil;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by PC162 on 8/9/2017.
 */

public class ViewSingleItem extends LinearLayout implements View.OnClickListener {

    @BindView(R.id.vsiImgImage)
    protected ImageView vsiImgImage;

    @BindView(R.id.vsiTxtDate)
    protected TextView vsiTxtDate;

    @BindView(R.id.vsiTxtTitle)
    protected TextView vsiTxtTitle;

    private OnClickNewsDetailListener onClickNewsDetailListener;
    private boolean isShowPicture;
    private Context context;
    private View root;
    private News news;

    public ViewSingleItem(Context context, News news, OnClickNewsDetailListener onClickNewsDetailListener, boolean isShowPicture) {
        super(context);
        this.onClickNewsDetailListener = onClickNewsDetailListener;
        this.isShowPicture = isShowPicture;
        this.context = context;
        this.news = news;
        initLayout();
    }

    public ViewSingleItem(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public ViewSingleItem(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
    private void initLayout(){
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        root = inflater.inflate(R.layout.view_single_item, this, true);
        ButterKnife.bind(this, root);
        root.setOnClickListener(this);
        initData();
        Utils.setFontName(context, (ViewGroup)root);
    }

    private void initData(){
        String imageUrl = news.getPrimary_image();
        vsiTxtDate.setText(Utils.getDate(news.getAvailable_date()));
        vsiTxtTitle.setText(news.getTitle());
        if (isShowPicture)
            ViewUtil.setImageForImageView(context, imageUrl, vsiImgImage);

        else
            vsiImgImage.setVisibility(View.GONE);

    }

    @Override
    public void onClick(View view) {
        onClickNewsDetailListener.onClickItem(news);
    }
}
