package org.fomosapiens.fomo.view.ui;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import org.fomosapiens.fomo.BuildConfig;
import org.fomosapiens.fomo.R;
import org.fomosapiens.fomo.SharePre;
import org.fomosapiens.fomo.Utils;
import org.fomosapiens.fomo.model.Login;
import org.fomosapiens.fomo.net.entity.LoginEntity;
import org.fomosapiens.fomo.net.APIService;
import org.fomosapiens.fomo.net.APIUtils;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.Timer;
import java.util.TimerTask;
import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

public class SplashActivity extends BaseActivity implements View.OnClickListener {

    @BindView(R.id.aspTxtTryAgain)
    protected TextView aspTxtTryAgain;

    private Timer timer;
    private TimerTask timerTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);
        checkToNetToRequest();
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopTimer();
    }

    private void checkToNetToRequest(){
        if (isOnline())
            initFireBase();
        else
            setShowTextTryAgain();
    }

    private void setShowTextTryAgain(){
        aspTxtTryAgain.setVisibility(View.VISIBLE);
        aspTxtTryAgain.setOnClickListener(this);
    }

    private void initFireBase(){
        if (!isOnline())
            return;
        boolean isNoti = SharePre.getBoolean(SharePre.KEY_NOTIFICATION);
        if (isNoti) {
            FirebaseApp.initializeApp(this);
            FirebaseMessaging.getInstance().setAutoInitEnabled(true);
            FirebaseMessaging.getInstance().subscribeToTopic(BuildConfig.FIREBASE_TOPIC);
        } else {
            FirebaseMessaging.getInstance().unsubscribeFromTopic(BuildConfig.FIREBASE_TOPIC);
        }
        aspTxtTryAgain.setVisibility(View.INVISIBLE);

        String token= FirebaseInstanceId.getInstance().getToken();
        String deviceID = Utils.getDeviceId(this);
//        if (token == null) {
//            timerRequestToken();
//            return;
//        }
//        stopTimer();
        sendLogin(deviceID, token);
    }

    private void saveLogin(Login login){
        SharePre.saveString(SharePre.KEY_ACCESS_TOKEN, login.getAccess_token());
        SharePre.saveString(SharePre.KEY_REFRESH_TOKEN, login.getRefresh_token());
    }

    private void timerRequestToken(){
        timer = new Timer();
        timerTask = new TimerTask() {
            @Override
            public void run() {
                initFireBase();
            }
        };
        timer.scheduleAtFixedRate(timerTask, 20000, 20000);
    }

    private void stopTimer(){
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
        if (timerTask != null){
            timerTask.cancel();
            timerTask = null;
        }
    }

    // TODO: OnClickListener
    @Override
    public void onClick(View view) {
        checkToNetToRequest();
    }

    private void sendLogin(String deviceId, String token){
        APIService service = APIUtils.getAPIService();
        service.login(new LoginEntity(BuildConfig.CLIENT_ID, BuildConfig.CLIENT_SECRET, deviceId, token, "android"))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Login>() {
                    @Override
                    public void onNext(@NonNull Login login) {
                        saveLogin(login);
                        goToDashBoard();
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        Log.e("SplashActivity",""+e.toString());
                        showDialogWhenServerError();
                    }

                    @Override
                    public void onComplete() {
                        stopTimer();
                    }
                });
    }

    private void goToDashBoard(){
        Intent intent = new Intent(this, RefactorPrimaryBaseActivity.class);
        Bundle b = getIntent().getExtras();
        if (b != null) {
            try {
                JSONObject object = new JSONObject(b.getString("data"));
                long idNews = object.getLong("post_id");
                intent.putExtra("data_from_notification", idNews);
            } catch (JSONException e) {
                e.printStackTrace();
                Log.e("JSOnP", e.getMessage());
            } catch (Exception error){
                Log.e("JSOnPArase", error.getMessage());
            }
        }
        startActivity(intent);
    }

    private void showDialogWhenServerError() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(SplashActivity.this);
        builder.setTitle("Fehler! ");
        builder.setMessage("Es liegt ein Problem mit der Internetverbindung oder dem Server vor. Bitte versuchen Sie es später noch einmal.");
        builder.setCancelable(false);
        builder.setNegativeButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                checkToNetToRequest();
            }
        });
        builder.show();
    }

}
