package org.fomosapiens.fomo.view.ui.newsartical;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.PagerTabStrip;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import org.fomosapiens.fomo.R;
import org.fomosapiens.fomo.Utils;
import org.fomosapiens.fomo.model.Category;
import org.fomosapiens.fomo.model.News;
import org.fomosapiens.fomo.view.fragment.FragmentNewsArticleItem;
import org.fomosapiens.fomo.view.interfacelistener.OnClickNewsDetailListener;
import org.fomosapiens.fomo.view.interfacelistener.OnPostMenuPositionEvent;
import org.fomosapiens.fomo.view.ui.BaseActivity;
import org.fomosapiens.fomo.view.adapter.AdapterPagerNewsArticle;
import org.fomosapiens.fomo.view.ui.PrimaryBaseActivity;
import org.fomosapiens.fomo.view.ui.ViewUtil;
import org.fomosapiens.fomo.view.ui.newsdetail.NewsDetailActivity;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;

/**
 * Created by PC162 on 8/15/2017.
 */

public class NewsArticleActivity extends PrimaryBaseActivity implements OnClickNewsDetailListener {

    public static String TAG = "org.fomosapiens.fomo.view.ui.newsartical.NewsArticleActivity";

    protected View rootView;
    protected LinearLayout anaLinearRoot;
    protected ViewPager anaViewPagerContainer;
    protected PagerTabStrip anaPagerTabStrip;
    protected List<Category> categoryList;
    private int position;
    private boolean isAdd = false;
    private Dialog progressDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        rootView = getLayoutInflater().inflate(R.layout.activity_news_article, getContainerView());
        progressDialog = new Dialog(this);
        Bundle extras = getIntent().getBundleExtra(Utils.KEY_CATEGORY_BUNDLE);
        categoryList = extras.getParcelableArrayList(Utils.KEY_CATEGORY_LIST);
        position = extras.getInt(Utils.KEY_RIGHT_MENU_POSITION, 0);
        setToolBarTitle();
        initLayout();
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(OnPostMenuPositionEvent positionEvent) {
        if (positionEvent == null)
            return;
        int position = positionEvent.getPosition();
        anaViewPagerContainer.setCurrentItem(position);
    }

    @Override
    protected String getActivityName() {
        return TAG;
    }

    protected void setToolBarTitle() {
        abTxtToolBarTitle.setText(getResources().getString(R.string.news));
    }

    private void initLayout(){
        anaViewPagerContainer = ButterKnife.findById(rootView, R.id.anaViewPagerContainer);
        anaPagerTabStrip = ButterKnife.findById(rootView, R.id.anaPagerTabStrip);
        anaLinearRoot = ButterKnife.findById(rootView, R.id.anaLinearRoot);
        anaPagerTabStrip.setTabIndicatorColor(getResources().getColor(R.color.color_white));
        anaLinearRoot.setOnClickListener(this);
        initViewPager();
    }

    private void initViewPager(){
        if (!isOnline()) {
            if (!isAdd) {
                anaLinearRoot.addView(ViewUtil.getNoInternetTextView(this));
                isAdd = true;
            }
            return;
        }

        Utils.showProgressDialog(progressDialog);
        List<FragmentNewsArticleItem> newsArticleItems = new ArrayList<>();
        for(Category category : categoryList) {
            FragmentNewsArticleItem articleItem = FragmentNewsArticleItem.newInstant(this, category, this);
            newsArticleItems.add(articleItem);
        }
        visibleViewPager();
        AdapterPagerNewsArticle pagerNewsArticle = new AdapterPagerNewsArticle(getSupportFragmentManager(), categoryList, newsArticleItems);
        anaViewPagerContainer.setAdapter(pagerNewsArticle);
        anaViewPagerContainer.setCurrentItem(position);
    }

    private void goToNewsDetail(News news){
        if (news == null)
            return;
        Intent intent = new Intent(this, NewsDetailActivity.class);
        intent.putExtra(Utils.KEY_NEWS_ID, news.getId());
        startActivity(intent);
    }

    public boolean getCheckInternetConnection(){
        return isOnline();
    }

    protected void visibleViewPager(){
        anaViewPagerContainer.setVisibility(View.VISIBLE);
        anaPagerTabStrip.setVisibility(View.VISIBLE);
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        switch (view.getId()){
            case R.id.anaLinearRoot:
                initViewPager();
                break;
        }
    }

    @Override
    public void onClickItem(News news) {
        if (isOnline())
            goToNewsDetail(news);
    }
}
