package org.fomosapiens.fomo.view.ui.news;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.fomosapiens.fomo.R;
import org.fomosapiens.fomo.Utils;
import org.fomosapiens.fomo.model.Category;
import org.fomosapiens.fomo.view.ui.newsartical.RefactorNewsArticleFragment;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * A simple {@link Fragment} subclass.
 */
public class NewsCategoryFragment extends Fragment{

    @BindView(R.id.refactor_category_news_tab_layout)
    TabLayout titleTabLayout;
    @BindView(R.id.refactor_category_news_view_pager)
    ViewPager viewPager;

    private List<Category> newCategoryList = new ArrayList<>();
    private int selectPosition;

    public NewsCategoryFragment() {
        // Required empty public constructor
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Utils.setFontName(getContext(), (ViewGroup)this.getView());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.refactor_fragment_news_category, container, false);
        ButterKnife.bind(this, view);

        // get data
        Bundle bundle = getArguments();
        newCategoryList = bundle.getParcelableArrayList("news_category_list");
        selectPosition = bundle.getInt("news_category_position", 0);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        PagerAdapter adapter = new PagerAdapter(getChildFragmentManager());
        viewPager.setOffscreenPageLimit(0);
        viewPager.setAdapter(adapter);
        titleTabLayout.setupWithViewPager(viewPager);
        titleTabLayout.getTabAt(selectPosition).select();
    }

    public class PagerAdapter extends FragmentPagerAdapter {

        public PagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return RefactorNewsArticleFragment.newsArticleFragment(newCategoryList.get(position));
        }

        @Override
        public int getCount() {
            return  newCategoryList.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return newCategoryList.get(position).getName();
        }
    }

}
