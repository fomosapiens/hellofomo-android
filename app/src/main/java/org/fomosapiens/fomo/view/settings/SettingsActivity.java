package org.fomosapiens.fomo.view.settings;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;

import org.fomosapiens.fomo.R;
import org.fomosapiens.fomo.Utils;
import org.fomosapiens.fomo.view.customview.FontChange;
import org.fomosapiens.fomo.view.ui.PrimaryBaseActivity;

public class SettingsActivity extends PrimaryBaseActivity {

    public static final String TAG = "org.fomosapiens.fomo.view.ui.news.SettingsActivity";

    protected View rootView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        rootView = getLayoutInflater().inflate(R.layout.activity_settings, getContainerView());
        setTitleToolbar();
        Utils.setFontName(this, (ViewGroup)this.findViewById(android.R.id.content));
    }

    @Override
    protected String getActivityName() {
        return TAG;
    }

    private void setTitleToolbar() {
        abTxtToolBarTitle.setText(getResources().getString(R.string.settings));
    }
}
