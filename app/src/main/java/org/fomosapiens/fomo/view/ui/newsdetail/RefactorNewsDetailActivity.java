package org.fomosapiens.fomo.view.ui.newsdetail;

import android.Manifest;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.URLSpan;
import android.util.Log;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import com.bumptech.glide.Glide;
import org.fomosapiens.fomo.R;
import org.fomosapiens.fomo.SharePre;
import org.fomosapiens.fomo.Utils;
import org.fomosapiens.fomo.database.FomoDatabase;
import org.fomosapiens.fomo.database.news_favor.NewsFavorEnity;
import org.fomosapiens.fomo.model.NewsDetail;
import org.fomosapiens.fomo.model.response.ResponseNewsDetail;
import org.fomosapiens.fomo.net.APIService;
import org.fomosapiens.fomo.net.APIUtils;
import org.fomosapiens.fomo.view.adapter.AdapterDetailCategory;
import org.fomosapiens.fomo.view.customview.FontChange;
import org.fomosapiens.fomo.view.customview.PicassoImageGetter;
import org.fomosapiens.fomo.view.ui.BaseActivity;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Completable;
import io.reactivex.CompletableObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;

public class RefactorNewsDetailActivity extends BaseActivity implements View.OnClickListener{

    @BindView(R.id.refactor_news_detail_content)
    TextView contentTv;
    @BindView(R.id.refactor_news_detail_date)
    TextView dateTv;
    @BindView(R.id.refactor_news_detail_hour)
    TextView hourTv;
    @BindView(R.id.refactor_news_detail_image)
    ImageView mainImg;
    @BindView(R.id.refactor_news_detail_title)
    TextView titleTv;
    @BindView(R.id.refactor_news_detail_back_img)
    ImageView backImg;
    @BindView(R.id.refactor_news_detail_toolbar_layout)
    Toolbar toolbar;
    @BindView(R.id.refactor_news_detail_favo_img)
    ImageView favoImg;
    @BindView(R.id.refactor_news_detail_share_img)
    ImageView shareImg;
    @BindView(R.id.refactor_news_list_category)
    RecyclerView recyclerView;
    @BindView(R.id.refactor_news_detail_scroll)
    ScrollView scrollView;
    @BindView(R.id.refactor_news_detail_introduction)
    TextView introductionTv;
    @BindView(R.id.refactor_news_copy_right_img)
    TextView copyRightTv;

    private long newsIdSave = 0;
    private long newsId = 0;
    private FomoDatabase database = null;
    private boolean isAdded = false;
    private NewsDetail newsDetail = null;
    private NewsFavorEnity newsFavor = null;
    private int categoryId;
    private Dialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.refactor_activity_news_detail_1);
        ButterKnife.bind(this);
        database = FomoDatabase.getDatabase(this);
        progressDialog = new Dialog(RefactorNewsDetailActivity.this);
        // change font
        Utils.setFontName(this, (ViewGroup)this.findViewById(android.R.id.content));

        // get data from News list
        setSupportActionBar(toolbar);
        Bundle extras = getIntent().getExtras();
        assert extras != null;
        newsId = extras.getLong(Utils.KEY_NEWS_ID);
        if (newsId == -1) {
            onBackPressed();
            return;
        }
        categoryId = extras.getInt(Utils.KEY_CATEGORY_FAVOR);
        getNewsDetail(newsId);
        recyclerView.setFocusable(false);

        // check save data on favourite or not
        getNewsFavor(newsId);

        // click event
        backImg.setOnClickListener(this);
        favoImg.setOnClickListener(this);
        shareImg.setOnClickListener(this);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
//        Typeface typeface = Typeface.createFromAsset(getAssets(),"fonts/" +  Utils.FONT_NAME);
//        SpannableStringBuilder title = new SpannableStringBuilder(getString(R.string.font_size_normal));
//        title.setSpan(typeface, 0, title.length(), 0);
//        menu.add(Menu.NONE, R.id.news_detail_normal_font_size, 0, title);
        getMenuInflater().inflate(R.menu.news_detail_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.news_detail_normal_font_size:
                changeFontSize(16f, 14f, 12f);
                SharePre.saveString(SharePre.KEY_FONT_SIZE, "small");
                return true;

            case R.id.news_detail_big_font_size:
                changeFontSize(20f, 18f, 16f);
                SharePre.saveString(SharePre.KEY_FONT_SIZE, "normal");
                return true;

            case R.id.news_detail_extra_font_size:
                changeFontSize(24f, 22f,20f);
                SharePre.saveString(SharePre.KEY_FONT_SIZE, "big");
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.refactor_news_detail_back_img:
                onBackPressed();
                break;

            case R.id.refactor_news_detail_favo_img:
                if (isAdded) {
                    favoImg.setImageResource(R.drawable.ic_fav_2x);
                    isAdded = false;
                    Completable.fromAction(new Action() {
                        @Override
                        public void run() throws Exception {
                            database.newsFavorDao().deleteNewsFavor(newsFavor);
                        }
                    }).subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(new CompletableObserver() {
                                @Override
                                public void onSubscribe(Disposable d) {

                                }

                                @Override
                                public void onComplete() {

                                }

                                @Override
                                public void onError(Throwable e) {

                                }
                            });

                } else {
                    favoImg.setImageResource(R.drawable.ic_added_fav_2x);
                    isAdded = true;
                    newsFavor = new NewsFavorEnity();
                    newsFavor.setNews_id(newsId);
                    newsFavor.setAvailable_date(newsDetail.getAvailable_date());
                    newsFavor.setDescription(newsDetail.getDescription());
                    newsFavor.setPrimary_image(newsDetail.getPrimary_image());
                    newsFavor.setTitle(newsDetail.getTitle());
                    Completable.fromAction(new Action() {
                        @Override
                        public void run() throws Exception {
                            database.newsFavorDao().insertNewsFavor(newsFavor);
                        }
                    }).observeOn(AndroidSchedulers.mainThread())
                            .subscribeOn(Schedulers.io())
                            .subscribe(new CompletableObserver() {
                                @Override
                                public void onSubscribe(Disposable d) {

                                }

                                @Override
                                public void onComplete() {

                                }

                                @Override
                                public void onError(Throwable e) {

                                }
                            });
                }
                break;

            case R.id.refactor_news_detail_share_img:
                if (newsDetail.getShareInformation().getUrl() != null) {
                    Spannable sequence;
                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                        sequence = (Spannable) Html.fromHtml(newsDetail.getShareInformation().getDescription(), Html.FROM_HTML_MODE_LEGACY, null, null);
                    } else {
                        sequence = (Spannable) Html.fromHtml(newsDetail.getShareInformation().getDescription(), null, null);
                    }

                    SpannableStringBuilder strBuilder = new SpannableStringBuilder(sequence);
                    Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                    sharingIntent.setType("text/plain");
                    sharingIntent.putExtra(Intent.EXTRA_TEXT, sequence + " " + newsDetail.getShareInformation().getUrl());
                    startActivity(Intent.createChooser(sharingIntent, strBuilder));
                } else {
                    Toast.makeText(this, "Share link is not available", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    // get data from server
    private void getNewsDetail(long newsId) {
        if (!isOnline()) {
//            addTextNoInternet();
            return;
        }
        Utils.showProgressDialog(progressDialog);
        APIService service = APIUtils.getAPIService();
        String accessToken = Utils.getAccessToken();
        String lang = Utils.getLanguage();
        service.getSingleNewsById("Bearer " + accessToken, String.valueOf(newsId), lang)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<ResponseNewsDetail>() {
                    @Override
                    public void onNext(@NonNull ResponseNewsDetail responseNewsDetail) {
                        newsDetail = responseNewsDetail.getData();
                        if (newsDetail == null)
                            return;
                        setData(responseNewsDetail.getData());
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        AlertDialog.Builder builder;
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            builder = new AlertDialog.Builder(RefactorNewsDetailActivity.this, android.R.style.Theme_Material_Dialog_Alert);
                        } else {
                            builder = new AlertDialog.Builder(RefactorNewsDetailActivity.this);
                        }
                        builder.setMessage("Die Nachrichten existieren nicht")
                                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        finish();
                                    }
                                })
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .show();
                        Utils.hideProgressDialog(progressDialog);
                        Log.e("NewsDetailActivity", e.getMessage());
                    }

                    @Override
                    public void onComplete() {
                        Utils.hideProgressDialog(progressDialog);
                    }
                });
    }

    private void setData(NewsDetail newsDetail) {
        String authorName = getResources().getString(R.string.autor) + " ";
        Glide.with(getBaseContext()).load(newsDetail.getPrimary_image()).into(mainImg);
        String date = newsDetail.getAvailable_date();
        dateTv.setText(Utils.getDate(date));
        hourTv.setText(Utils.getHour(date));
        titleTv.setText(newsDetail.getTitle());
        if (newsDetail.getCopyright_primary_image() != null) {
            if (!newsDetail.getCopyright_primary_image().equals("")) {
                copyRightTv.setText("Foto: " + newsDetail.getCopyright_primary_image());
            }
        }
        setAutoLinkForTextContent(newsDetail);
//        introductionTv.setText(newsDetail.getIntroduction());

        // set list detail categories
        AdapterDetailCategory adapterDetailCategory = new AdapterDetailCategory(newsDetail, RefactorNewsDetailActivity.this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(adapterDetailCategory);

        String fontStyle = SharePre.getString(SharePre.KEY_FONT_SIZE);
        if (fontStyle.equals("") || fontStyle.equals("small")) {
            changeFontSize(16f, 14f, 12f);
        } else if (fontStyle.equals("normal")) {
            changeFontSize(20f, 18f, 16f);
        } else if (fontStyle.equals("big")) {
            changeFontSize(24f, 22f,20f);
        }

        Utils.hideProgressDialog(progressDialog);
    }


    private void setAutoLinkForTextContent(NewsDetail newsDetail) {
        PicassoImageGetter imageGetter = new PicassoImageGetter(contentTv, getBaseContext());
        Spannable sequence, sequence1;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            if (newsDetail.getDescription() != null) {
                sequence = (Spannable) Html.fromHtml(newsDetail.getDescription(), Html.FROM_HTML_MODE_LEGACY, imageGetter, null);
                setTextHTML(sequence, contentTv);
            }
            if (newsDetail.getIntroduction() != null) {
                sequence1 = (Spannable) Html.fromHtml(newsDetail.getIntroduction(), Html.FROM_HTML_MODE_LEGACY, imageGetter, null);
                setTextHTML(sequence1, introductionTv);
            }
        } else {
            if (newsDetail.getDescription() != null) {
                sequence = (Spannable) Html.fromHtml(newsDetail.getDescription(), imageGetter, null);
                setTextHTML(sequence, contentTv);
            }
            if (newsDetail.getIntroduction() != null) {
                sequence1 = (Spannable) Html.fromHtml(newsDetail.getIntroduction(), imageGetter, null);
                setTextHTML(sequence1, introductionTv);
            }
        }
    }

    private void setTextHTML(Spannable sequence, TextView textView) {
        SpannableStringBuilder strBuilder = new SpannableStringBuilder(sequence);
        URLSpan[] urls = strBuilder.getSpans(0, sequence.length(), URLSpan.class);
        for (URLSpan span : urls) {
            makeLinkClickable(strBuilder, span);
        }
        textView.setText(strBuilder);
        textView.setMovementMethod(LinkMovementMethod.getInstance());
    }

    protected void makeLinkClickable(SpannableStringBuilder strBuilder, final URLSpan span) {
        int start = strBuilder.getSpanStart(span);
        int end = strBuilder.getSpanEnd(span);
        int flags = strBuilder.getSpanFlags(span);
        ClickableSpan clickable = new ClickableSpan() {
            public void onClick(View view) {
//                String id = Utils.getIdFromUrl(span.getURL());
                startNewsOtherDetailFromThisDetail(span.getURL());
            }
        };
        strBuilder.setSpan(clickable, start, end, flags);
        strBuilder.removeSpan(span);
    }

    private void startNewsOtherDetailFromThisDetail(String url) {
        if (url==null || url.equals(""))
            return;
//        Intent intent = new Intent(this, RefactorNewsDetailActivity.class);
//        long mId = Long.valueOf(id);
//        intent.putExtra(Utils.KEY_NEWS_ID, mId);
//        startActivity(intent);
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        startActivity(browserIntent);
    }

    // change font size
    private void changeFontSize(float fontTitle, float fontIntro, float fontContent) {
        titleTv.setTextSize(TypedValue.COMPLEX_UNIT_DIP, fontTitle);
        contentTv.setTextSize(TypedValue.COMPLEX_UNIT_DIP, fontContent);
        introductionTv.setTextSize(TypedValue.COMPLEX_UNIT_DIP, fontIntro);
    }

    private void getNewsFavor(long newsId) {
        database.newsFavorDao().newsFavorInfo(newsId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<NewsFavorEnity>() {
                    @Override
                    public void accept(NewsFavorEnity newsFavorEnity) throws Exception {
                        newsFavor = newsFavorEnity;
                        if (newsFavor != null) {
                            favoImg.setImageResource(R.drawable.ic_added_fav_2x);
                            isAdded = true;
                        } else {
                            favoImg.setImageResource(R.drawable.ic_fav_2x);
                            isAdded = false;
                        }
                    }
                });
    }

    @Override
    public void onBackPressed() {
        if (categoryId == 100) {
            setResult(RESULT_OK);
        } else {
            setResult(RESULT_CANCELED);
        }
        super.onBackPressed();
    }

    public void downloadFileAttach(final String url, final String name) {
        Utils.showProgressDialog(progressDialog);
        if (isStoragePermissionGrand()) {
            APIService service = APIUtils.getAPIService();
            service.getImage(url)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new DisposableObserver<ResponseBody>() {
                        @Override
                        public void onNext(ResponseBody responseBody) {
                            boolean fileDownloaded = downloadImageSuccess(responseBody, name);
                            if (fileDownloaded) {
//                                goToShareAttach(name);
                                goToShareAcitivity(url, name);
                            }
                        }

                        @Override
                        public void onError(Throwable e) {

                        }

                        @Override
                        public void onComplete() {

                        }
                    });
        }
    }

    private boolean downloadImageSuccess(ResponseBody body, String name) {
        try {
            InputStream in = null;
            FileOutputStream out = null;

            File file = new File(getExternalFilesDir(null) + File.separator + name);

            try {
                byte[] fileReader = new byte[4096];

                in = body.byteStream();
                out = new FileOutputStream(file);

                while (true) {
                    int read = in.read(fileReader);

                    if (read == -1) {
                        break;
                    }
                    out.write(fileReader, 0, read);
                }

                out.flush();

                return true;
            } catch (IOException e) {
                return false;
            } finally {
                if (in != null) {
                    in.close();
                }
                if (out != null) {
                    out.close();
                }
            }
        } catch (IOException e) {
            return false;
        }
    }

    public void goToShareAcitivity(String url, String name) {
        Intent intent = new Intent(this, RefactorShareAttachmentActivity.class);
        intent.putExtra("SEND_URL", url);
        intent.putExtra("SEND_NAME", name);
        startActivity(intent);
        Utils.hideProgressDialog(progressDialog);
    }

    // check permission write storage
    public boolean isStoragePermissionGrand() {
        if (Build.VERSION.SDK_INT > 23) {
            if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) ==
                    PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        } else {
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @android.support.annotation.NonNull String[] permissions, @android.support.annotation.NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

}
