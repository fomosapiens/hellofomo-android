package org.fomosapiens.fomo.view.ui.newsdetail;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.URLSpan;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.fomosapiens.fomo.R;
import org.fomosapiens.fomo.SharePre;
import org.fomosapiens.fomo.Utils;
import org.fomosapiens.fomo.model.Author;
import org.fomosapiens.fomo.model.News;
import org.fomosapiens.fomo.model.NewsDetail;
import org.fomosapiens.fomo.model.response.ResponseNewsDetail;
import org.fomosapiens.fomo.net.APIService;
import org.fomosapiens.fomo.net.APIUtils;
import org.fomosapiens.fomo.view.ui.BaseActivity;
import org.fomosapiens.fomo.view.ui.PrimaryBaseActivity;
import org.fomosapiens.fomo.view.ui.ViewUtil;
import org.fomosapiens.fomo.view.ui.news.NewsActivity;

import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by PC162 on 8/1/2017.
 */

public class NewsDetailActivity extends PrimaryBaseActivity {

    public static String TAG = "org.fomosapiens.fomo.view.ui.newsdetail.NewsDetailActivity";

    protected View rootView;
    protected TextView andTxtDate;
    protected TextView andTxtHour;
    protected TextView andTxtTitle;
    protected TextView andTxtContent;
    protected TextView andTxtAuthor;
    protected TextView andTxtBack;
    protected ImageView andImgAuthor;
    protected ImageView andImgPrimaryImage;
    protected LinearLayout andLinearContainer;

    protected TextView textView;

    private long newsId;
    private Dialog progressDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle extras = getIntent().getExtras();
        newsId = extras.getLong(Utils.KEY_NEWS_ID);
        rootView = getLayoutInflater().inflate(R.layout.activity_news_detail, getContainerView());
        progressDialog = new Dialog(this);
        setTitleToolbar();
        initLayout();
        getNewsDetail();
    }

    private void initLayout() {
        andTxtDate = ButterKnife.findById(rootView, R.id.andTxtDate);
        andTxtHour = ButterKnife.findById(rootView, R.id.andTxtHour);
        andTxtTitle = ButterKnife.findById(rootView, R.id.andTxtTitle);
        andTxtContent = ButterKnife.findById(rootView, R.id.andTxtContent);
        andTxtAuthor = ButterKnife.findById(rootView, R.id.andTxtAuthor);
        andTxtBack = ButterKnife.findById(rootView, R.id.andTxtBack);
        andImgAuthor = ButterKnife.findById(rootView, R.id.andImgAuthor);
        andImgPrimaryImage = ButterKnife.findById(rootView, R.id.andImgPrimaryImage);
        andLinearContainer = ButterKnife.findById(rootView, R.id.andLinearContainer);

        andTxtBack.setOnClickListener(this);
    }

    private void setData(NewsDetail newsDetail) {
        if (textView != null)
            andLinearContainer.removeView(textView);
        String authorName = getResources().getString(R.string.autor) + " ";
        authorName = authorName + Utils.getAuthorName(newsDetail.getAuthors());
        String date = newsDetail.getAvailable_date();
        andTxtDate.setText(Utils.getDate(date));
        andTxtHour.setText(Utils.getHour(date));
        andTxtTitle.setText(newsDetail.getTitle());
        setAutoLinkForTextContent(newsDetail);
        ViewUtil.setImageForImageView(this, newsDetail.getPrimary_image(), andImgPrimaryImage);
        andTxtAuthor.setText(authorName);
        setTextUnderLine(andTxtBack, getResources().getString(R.string.zuruck));
        andImgAuthor.setVisibility(View.VISIBLE);
        Utils.hideProgressDialog(progressDialog);
    }


    private void setAutoLinkForTextContent(NewsDetail newsDetail) {
        CharSequence sequence;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            sequence = Html.fromHtml(newsDetail.getDescription(), Html.FROM_HTML_MODE_LEGACY);
        } else {
            sequence = Html.fromHtml(newsDetail.getDescription());
        }

        SpannableStringBuilder strBuilder = new SpannableStringBuilder(sequence);
        URLSpan[] urls = strBuilder.getSpans(0, sequence.length(), URLSpan.class);
        for (URLSpan span : urls) {
            makeLinkClickable(strBuilder, span);
        }

        andTxtContent.setText(strBuilder);
        andTxtContent.setMovementMethod(LinkMovementMethod.getInstance());
    }

    protected void makeLinkClickable(SpannableStringBuilder strBuilder, final URLSpan span) {
        int start = strBuilder.getSpanStart(span);
        int end = strBuilder.getSpanEnd(span);
        int flags = strBuilder.getSpanFlags(span);
        ClickableSpan clickable = new ClickableSpan() {
            public void onClick(View view) {
                String id = Utils.getIdFromUrl(span.getURL());
                startNewsOtherDetailFromThisDetail(id);
            }
        };
        strBuilder.setSpan(clickable, start, end, flags);
        strBuilder.removeSpan(span);
    }


    private void addTextNoInternet() {
        if (textView != null)
            return;

        andLinearContainer.setOnClickListener(this);
        textView = ViewUtil.getNoInternetTextView(this);
        andLinearContainer.addView(textView);
    }

    private void startNewsOtherDetailFromThisDetail(String id) {
        if (id==null || id.equals(""))
            return;
        Intent intent = new Intent(this, NewsDetailActivity.class);
        long mId = Long.valueOf(id);
        intent.putExtra(Utils.KEY_NEWS_ID, mId);
        startActivity(intent);
    }

    private void setTextUnderLine(TextView textView, String content) {
        textView.setPaintFlags(textView.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        textView.setText(content);
    }

    @Override
    protected String getActivityName() {
        return TAG;
    }

    private void setTitleToolbar() {
        abTxtToolBarTitle.setText(getResources().getString(R.string.news));
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        switch (view.getId()) {
            case R.id.andTxtBack:
                finish();
                break;
            case R.id.andLinearContainer:
                getNewsDetail();
                break;
        }
    }

    private void getNewsDetail() {
        if (!isOnline()) {
            addTextNoInternet();
            return;
        }
        Utils.showProgressDialog(progressDialog);
        APIService service = APIUtils.getAPIService();
        String accessToken = Utils.getAccessToken();
        String lang = Utils.getLanguage();
        service.getSingleNewsById("Bearer " + accessToken, String.valueOf(newsId), lang)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<ResponseNewsDetail>() {
                    @Override
                    public void onNext(@NonNull ResponseNewsDetail responseNewsDetail) {
                        NewsDetail newsDetail = responseNewsDetail.getData();
                        if (newsDetail == null)
                            return;
                        setData(responseNewsDetail.getData());
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        Utils.hideProgressDialog(progressDialog);
                        Log.e("NewsDetailActivity", "onError");
                    }

                    @Override
                    public void onComplete() {
                        Utils.hideProgressDialog(progressDialog);
                    }
                });
    }
}
