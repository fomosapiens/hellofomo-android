package org.fomosapiens.fomo.view.ui;

import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;

import org.fomosapiens.fomo.R;
import org.fomosapiens.fomo.model.Category;
import org.fomosapiens.fomo.view.customview.CustomTextView;
import org.fomosapiens.fomo.view.interfacelistener.OnClickMenuItemListener;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by PC162 on 8/11/2017.
 */

public class ViewUtil {
    private static final AtomicInteger sNextGeneratedId = new AtomicInteger(1);

    public static int generateViewId() {
        for (; ; ) {
            final int result = sNextGeneratedId.get();
            int newValue = result + 1;
            if (newValue > 0x00FFFFFF) newValue = 1;
            if (sNextGeneratedId.compareAndSet(result, newValue)) {
                return result;
            }
        }
    }

    public static void setBackgroundForImageView(Context context, String url, final ImageView imageView){
        RequestOptions options = new RequestOptions();
        options.centerCrop();

        if(context==null || imageView == null)
            return;

        Glide.with(context)
                .load(url)
                .apply(options)
                .into(imageView);

    }

    public static void setImageForImageView(Context context, String url, ImageView imageView){
        RequestOptions options = new RequestOptions();
        options.centerCrop();
        if (context == null || imageView == null)
            return;

        Glide.with(context)
                .load(url)
                .apply(options)
                .into(imageView);
    }

    public static List<Integer> generateRadioButtonList(Context context, int size, RadioGroup radioGroup){

        List<Integer> radioButtonID = new ArrayList<>();
        for (int i = 0; i< size; i++ ) {
            int id = ViewUtil.generateViewId();
            RadioButton radioButton = new RadioButton(context);
            radioButton.setScaleX(0.50f);
            radioButton.setScaleY(0.50f);
            radioButton.setPadding(3,0,0,0);
            radioButton.setButtonDrawable(context.getResources().getDrawable(R.drawable.custom_radio_button));
            radioButton.setId(id);
            radioButtonID.add(id);
            if (i == 0)
                radioButton.setChecked(true);

            radioGroup.addView(radioButton);
        }
        return radioButtonID;
    }

    public static TextView getNoInternetTextView(Context context){
        TextView textView = new TextView(context);
        textView.setTypeface(null, Typeface.ITALIC);
        textView.setGravity(View.TEXT_ALIGNMENT_GRAVITY);
        textView.setTextSize(14);
        textView.setTextColor(context.getResources().getColor(R.color.color_black_text_FF373B3E));
        textView.setText("No internet connection, please touch to get data again");
        return textView;
    }

    public static List<TextView> generateTxtForRightMenu(Context context, List<Category> categoryList, OnClickMenuItemListener clickMenuItem){
        List<TextView> textViewList = new ArrayList<>();
        for (Category category : categoryList){
            CustomTextView textView = new CustomTextView(context, category, clickMenuItem);
            if (Build.VERSION.SDK_INT < 23)
                textView.setTextAppearance(context, R.style.MenuRightContentEachItem);

            else
                textView.setTextAppearance(R.style.MenuRightContentEachItem);

            textViewList.add(textView);
        }

        return textViewList;
    }

}
