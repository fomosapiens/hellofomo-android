package org.fomosapiens.fomo.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.fomosapiens.fomo.R;
import org.fomosapiens.fomo.Utils;
import org.fomosapiens.fomo.model.Category;
import org.fomosapiens.fomo.view.customview.FontChange;
import org.fomosapiens.fomo.view.ui.RefactorPrimaryBaseActivity;

import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by HuyMTB on 7/10/18.
 */

public class AdapterSubMenuItem extends RecyclerView.Adapter<AdapterSubMenuItem.ViewHolder> {

    private List<Category> subMenuList;
    private Context context;


    public AdapterSubMenuItem(Context context, List<Category> subMenuList) {
        this.subMenuList = subMenuList;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.refactor_menu_sub_item, parent, false);
        Utils.setFontName(context, (ViewGroup) view);
        return new ViewHolder(view);

    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final Category item = subMenuList.get(position);

        holder.subMenuTitle.setText(item.getName());
        final int id = item.getId();

        holder.subMenuTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((RefactorPrimaryBaseActivity) context).isOnline()) {
                    if (id == 101) {
                        ((RefactorPrimaryBaseActivity) context).gotoAgb();
                    } else if (id == 102) {
                        ((RefactorPrimaryBaseActivity) context).gotoDatenschutz();
                    } else if (id == 103) {
                        ((RefactorPrimaryBaseActivity) context).gotoImpressum();
                    } else if (id == 104) {
                        ((RefactorPrimaryBaseActivity) context).gotoKontaktActivity();
                    } else if (id == 201) {
                        ((RefactorPrimaryBaseActivity) context).goToRegulation();
                    } else if (id == 202) {
                        ((RefactorPrimaryBaseActivity) context).goToHistoryRegulation();
                    } else if (id == 301) {
                        ((RefactorPrimaryBaseActivity) context).goToCompanies();
                    } else if (id == 302) {
                        ((RefactorPrimaryBaseActivity) context).goToMarketPlace();
                    } else {
                        ((RefactorPrimaryBaseActivity) context).loadCategoryNews(holder.getAdapterPosition());
                    }
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return subMenuList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.refactor_sub_menu_item)
        TextView subMenuTitle;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
