package org.fomosapiens.fomo.view.fragment;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.fomosapiens.fomo.R;
import org.fomosapiens.fomo.SharePre;
import org.fomosapiens.fomo.Utils;
import org.fomosapiens.fomo.model.Category;
import org.fomosapiens.fomo.model.News;
import org.fomosapiens.fomo.model.response.ResponseListNews;
import org.fomosapiens.fomo.net.APIUtils;
import org.fomosapiens.fomo.view.adapter.AdapterNewsArticleItem;
import org.fomosapiens.fomo.view.customview.FontChange;
import org.fomosapiens.fomo.view.interfacelistener.OnClickNewsDetailListener;
import org.fomosapiens.fomo.view.interfacelistener.OnRecyclerViewScrollListener;
import org.fomosapiens.fomo.view.ui.newsartical.NewsArticleActivity;


import java.lang.ref.WeakReference;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by PC162 on 8/15/2017.
 */

public class FragmentNewsArticleItem extends Fragment implements SwipeRefreshLayout.OnRefreshListener, OnRecyclerViewScrollListener {

    @BindView(R.id.fgneiSwipeContainer)
    protected SwipeRefreshLayout fgneiSwipeContainer;

    @BindView(R.id.fgneiRecyclerView)
    protected RecyclerView fgneiRecyclerView;

    private int page = 0;
    private Category category;
    private String accessToken;
    private LinearLayoutManager mLayoutManager;
    private AdapterNewsArticleItem adapterNewsArticleItem;
    private OnClickNewsDetailListener onClickNewsDetailListener;
    protected NewsArticleActivity articleActivity;
    private WeakReference<NewsArticleActivity> newsArticleActivityRef;
    private Dialog progressDialog;

    public static FragmentNewsArticleItem newInstant(NewsArticleActivity articleActivity, Category category, OnClickNewsDetailListener onClickNewsDetailListener){
        FragmentNewsArticleItem articleItem = new FragmentNewsArticleItem();
        articleItem.newsArticleActivityRef = new WeakReference<>(articleActivity);
        articleItem.onClickNewsDetailListener = onClickNewsDetailListener;
        articleItem.category = category;
        return articleItem;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Utils.setFontName(getContext(), (ViewGroup)this.getView());
    }

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_news_article_item, container, false);
        ButterKnife.bind(this, rootView);
        progressDialog = new Dialog(getContext());
        accessToken = Utils.getAccessToken();
        articleActivity = newsArticleActivityRef.get();
        fgneiSwipeContainer.setOnRefreshListener(this);
        getNewsList(false);

        return rootView;
    }

    private void initRecycleView(List<News> newsList){
        if (newsList == null || newsList.size() == 0)
            return;

        mLayoutManager = new LinearLayoutManager(getContext());
        adapterNewsArticleItem = new AdapterNewsArticleItem(getContext(), newsList, onClickNewsDetailListener);
        adapterNewsArticleItem.setLinearLayoutManager(mLayoutManager);
        adapterNewsArticleItem.setRecyclerView(fgneiRecyclerView);
        adapterNewsArticleItem.setOnRecyclerViewListener(this);
        fgneiRecyclerView.setLayoutManager(mLayoutManager);
        fgneiRecyclerView.setAdapter(adapterNewsArticleItem);
    }

    private void addMoreItemForAdapter(List<News> newsList){
        if (newsList == null || newsList.size()==0){
            adapterNewsArticleItem.removeProgressMore();
            return;
        }
        adapterNewsArticleItem.addMoreItem(newsList);
    }

    private void dismissProgress(){
        if (fgneiSwipeContainer.isRefreshing())
            fgneiSwipeContainer.setRefreshing(false);
        Utils.hideProgressDialog(progressDialog);
    }

    // TODO: OnRefreshListener
    @Override
    public void onRefresh() {
        getNewsList(false);
        adapterNewsArticleItem.setRefreshData();
    }

    // TODO: OnRecyclerViewScrollListener
    @Override
    public void onLoadMoreListener() {
        getNewsList(true);
    }

    private void getNewsList(final boolean isLoadMore){
        if (category == null)
            return;

        if (accessToken == null)
            return;

        if (!articleActivity.getCheckInternetConnection())
            return;

        if (!isLoadMore && !fgneiSwipeContainer.isRefreshing())
            Utils.showProgressDialog(progressDialog);

        String valueLanguage = Utils.getLanguage();
        String valueCategory = String.valueOf(category.getId());
        String valueLimit = "50";
        if (isLoadMore)
            page++;
        else
            page = 0;
        String valuePage = String.valueOf(page);
        APIUtils.getAPIService().getListNewsArticle("Bearer " + accessToken, valueLanguage, valueCategory, valueLimit, valuePage)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<ResponseListNews>() {
                    @Override
                    public void onNext(@NonNull ResponseListNews responseListNews) {
                        List<News> newsList = responseListNews.getData();
                        if (isLoadMore)
                            addMoreItemForAdapter(newsList);
                        else
                            initRecycleView(newsList);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        dismissProgress();
                        page--;
                    }

                    @Override
                    public void onComplete() {
                        dismissProgress();
                    }
                });
    }

}
