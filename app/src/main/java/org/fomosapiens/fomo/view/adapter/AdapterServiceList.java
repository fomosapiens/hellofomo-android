package org.fomosapiens.fomo.view.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;

import org.fomosapiens.fomo.R;
import org.fomosapiens.fomo.Utils;
import org.fomosapiens.fomo.model.service.ServiceData;
import org.fomosapiens.fomo.view.ui.RefactorPrimaryBaseActivity;
import org.fomosapiens.fomo.view.ui.service.ServiceDetailActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AdapterServiceList extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int ITEM = 0;
    private static final int LOADING = 1;
    private boolean isLoadingAdded = false;
    private int filterOption;

    private List<ServiceData> listResponses;
    private Context context;

    public AdapterServiceList(Context context, int filterOption) {
        this.context = context;
        this.filterOption = filterOption;
        listResponses = new ArrayList<>();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());


        switch (viewType) {
            case ITEM:
                viewHolder = getViewHolder(parent, inflater);
                break;

            case LOADING:
                View v2 = inflater.inflate(R.layout.item_progress, parent, false);
                viewHolder = new AdapterServiceList.LoadingVH(v2);
        }
        return viewHolder;
    }

    @NonNull
    private RecyclerView.ViewHolder getViewHolder(ViewGroup parent, LayoutInflater inflater) {
        RecyclerView.ViewHolder viewHolder;
        View v1 = inflater.inflate(R.layout.item_service_list, parent, false);
        viewHolder = new AdapterServiceList.ServiceVH(v1);
        Utils.setFontName(context, parent);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final ServiceData data = listResponses.get(position);

        switch (getItemViewType(position)) {
            case ITEM:
                final ServiceVH serviceVH = (ServiceVH) holder;

                if (filterOption == 0) {
                    serviceVH.name.setText(data.getName());
                    serviceVH.product.setVisibility(View.GONE);
                } else {
                    serviceVH.product.setText(data.getName());
                    serviceVH.name.setText(data.getMarketplace().getProduct());
                    serviceVH.product.setVisibility(View.VISIBLE);
                }

                serviceVH.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (((RefactorPrimaryBaseActivity)context).isOnline()) {
                            Intent intent = new Intent(context, ServiceDetailActivity.class);
                            Gson gson = new Gson();
                            String object = gson.toJson(data);
                            intent.putExtra(Utils.KEY_SERVICE_DETAIL, object);
                            intent.putExtra(Utils.KEY_FILTER_OPTION, filterOption);
                            context.startActivity(intent);
                        }
                    }
                });
                break;

            case LOADING:
                break;
        }
    }

    @Override
    public int getItemCount() {
        return listResponses == null ? 0 : listResponses.size();
    }

    @Override
    public int getItemViewType(int position) {
        return (position == listResponses.size() - 1 && isLoadingAdded) ? LOADING : ITEM;
    }

    /*
        Helper
    */
    public void add(ServiceData data) {
        listResponses.add(data);
        notifyItemInserted(listResponses.size() - 1);
    }

    public void addAll(List<ServiceData> listData) {
        for (ServiceData data : listData) {
            add(data);
        }
    }

    public void removeAll() {
        listResponses = new ArrayList<>();
        notifyDataSetChanged();
    }

    public void addLoadingFooter() {
        isLoadingAdded = true;
        add(new ServiceData());
    }

    public void removeLoadingFooter() {
        isLoadingAdded = false;

        int position = listResponses.size() - 1;
        ServiceData data = getItem(position);

        if (data != null) {
            listResponses.remove(position);
            notifyItemRemoved(position);
        }
    }

    public ServiceData getItem(int position) { return listResponses.get(position); }

    protected class ServiceVH extends RecyclerView.ViewHolder {

        @BindView(R.id.service_item_name)
        TextView name;
        @BindView(R.id.service_item_view_bottom)
        View viewBottom;
        @BindView(R.id.service_item_product)
        TextView product;

        public ServiceVH(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    protected class LoadingVH extends RecyclerView.ViewHolder {
        public LoadingVH(View itemView) {super(itemView);}
    }
}
