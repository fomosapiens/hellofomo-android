package org.fomosapiens.fomo.view.ui.newsdetail;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.FileProvider;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.github.barteksc.pdfviewer.PDFView;

import org.fomosapiens.fomo.BuildConfig;
import org.fomosapiens.fomo.R;
import org.fomosapiens.fomo.view.ui.BaseActivity;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RefactorShareAttachmentActivity extends BaseActivity {

    @BindView(R.id.refactor_share_attach_back)
    ImageView backImg;
    @BindView(R.id.refactor_share_image)
    ImageView imageView;
    @BindView(R.id.refactor_share_attach)
    ImageView shareImg;
    @BindView(R.id.refactor_share_title)
    TextView shareTitle;
    @BindView(R.id.pdfView)
    PDFView pdfView;
    @BindView(R.id.docsView)
    WebView docsView;

    private File file;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.refactor_activity_share_attachment);
        ButterKnife.bind(this);

        String url = getIntent().getStringExtra("SEND_URL");
        final String name = getIntent().getStringExtra("SEND_NAME");

        file = new File(getExternalFilesDir(null) + File.separator + name);
        Uri uri = Uri.fromFile(file);
        String type = getTypeOfFile(name);
        if (url != null) {
            if (type.equals("jpg") || type.equals("png")) {
                Glide.with(RefactorShareAttachmentActivity.this).load(uri).into(imageView);
                pdfView.setVisibility(View.GONE);
                imageView.setVisibility(View.VISIBLE);
                docsView.setVisibility(View.GONE);
            }
            else if (type.equals("pdf") || type.equals("docs") || type.equals("doc")) {
                pdfView.setVisibility(View.GONE);
                imageView.setVisibility(View.GONE);
                docsView.setVisibility(View.VISIBLE);
                String docUrl = "https://docs.google.com/gview?embedded=true&url=";
                readDocFile(docUrl + url);
            }
            else {
                docsView.setVisibility(View.VISIBLE);
                pdfView.setVisibility(View.GONE);
                imageView.setVisibility(View.GONE);
                readDocFile(url);
            }

        }
        shareTitle.setText(name);

        shareImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToShareAttach(name);
            }
        });

        backImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }

    private void goToShareAttach(String name) {

//        Uri uri = Uri.parse("file://" + shareImgFile.getAbsolutePath());
        Uri uri = FileProvider.getUriForFile(RefactorShareAttachmentActivity.this,
                BuildConfig.APPLICATION_ID + ".provider",
                file);
        Intent shareImg = new Intent(Intent.ACTION_SEND);
        shareImg.putExtra(Intent.EXTRA_STREAM, uri);
        shareImg.setType("image/*");
        shareImg.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        startActivity(Intent.createChooser(shareImg, "Share attach file"));
    }

    private String getTypeOfFile(String name) {
        String[] cutString = name.split("\\.");
        String mFileName = cutString[cutString.length-1];
        return mFileName;
    }

    private void readDocFile(String url) {

        docsView.getSettings().setJavaScriptEnabled(true);
        docsView.getSettings().setSupportZoom(true);
        docsView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
            }
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
            }
        });
        docsView.loadUrl(url);
    }
}
