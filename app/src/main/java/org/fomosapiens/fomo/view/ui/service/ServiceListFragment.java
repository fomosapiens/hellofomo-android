package org.fomosapiens.fomo.view.ui.service;


import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.fomosapiens.fomo.R;
import org.fomosapiens.fomo.Utils;
import org.fomosapiens.fomo.model.service.FilterLetter;
import org.fomosapiens.fomo.model.service.LetterResponse;
import org.fomosapiens.fomo.model.service.ServiceData;
import org.fomosapiens.fomo.model.service.ServiceResponse;
import org.fomosapiens.fomo.net.APIService;
import org.fomosapiens.fomo.net.APIUtils;
import org.fomosapiens.fomo.view.adapter.AdapterLetterList;
import org.fomosapiens.fomo.view.adapter.AdapterServiceList;
import org.fomosapiens.fomo.view.adapter.PaginationScrollListener;
import org.fomosapiens.fomo.view.ui.SplashActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * A simple {@link Fragment} subclass.
 */
public class ServiceListFragment extends Fragment {


    public ServiceListFragment() {
        // Required empty public constructor
    }

    // init variable
    private APIService apiService;
    private String accessToken = "";
    private Dialog progressDialog;
    private AdapterServiceList adapterServiceListLoadMore;
    private List<ServiceData> listData;
    private int filterOption;

    // load more
    private int PAGE_START = 1;
    private boolean isLoading = false;
    private boolean isLastPage = false;
    private int TOTAL_PAGES = 0;
    private int currentPage = PAGE_START;
    private int totalData = 0;

    // bind view
    @BindView(R.id.rcLetter)
    RecyclerView rcLetter;
    @BindView(R.id.rcService)
    RecyclerView rcService;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_service_list, container, false);
        ButterKnife.bind(this, view);
        apiService = APIUtils.getAPIService();
        accessToken = Utils.getAccessToken();
        progressDialog = new Dialog(getContext());
        filterOption = getArguments().getInt(Utils.KEY_FILTER_OPTION);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Utils.setFontName(getContext(), (ViewGroup) this.getView());
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setUpAdapter();
        getListLetter();
    }

    private void setUpAdapter() {
        // set adapter
        adapterServiceListLoadMore = new AdapterServiceList(getContext(), filterOption);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        rcService.setLayoutManager(layoutManager);
        rcService.setAdapter(adapterServiceListLoadMore);
        rcService.addOnScrollListener(new PaginationScrollListener(layoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                currentPage += 1;

                // mocking network delay for API call
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
//                        getNextPageListRegulation();
                    }
                }, 1000);
            }

            @Override
            public int getTotalPageCount() {
                return currentPage;
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });
    }

    private void getListLetter() {
        Utils.showProgressDialog(progressDialog);
        apiService.getLetters("Bearer " + accessToken, filterOption)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<LetterResponse>() {
                    @Override
                    public void onNext(LetterResponse letterResponse) {
                        List<FilterLetter> letters = letterResponse.getData();
                        if (letters.size() > 0) {
                            AdapterLetterList.LetterClick click = new AdapterLetterList.LetterClick() {
                                @Override
                                public void itemClick(String letter) {
                                    Utils.showProgressDialog(progressDialog);
                                    adapterServiceListLoadMore.removeAll();
                                    if (letter.equals("Alle")) {
                                        letter = "";
                                    }
                                    getFirstPageServiceList(filterOption, letter);
                                    isLoading = false;
                                    currentPage = PAGE_START;
                                }
                            };

                            // set adapter register list
                            AdapterLetterList adapterRegisterList = new AdapterLetterList(getContext(), letters, click);
                            LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
                            rcLetter.setLayoutManager(layoutManager);
                            rcLetter.setAdapter(adapterRegisterList);
                        } else  {
                            getFirstPageServiceList(filterOption, "");
                        }
                        Utils.hideProgressDialog(progressDialog);
                    }

                    @Override
                    public void onError(Throwable e) {
//                        getFirstPageServiceList(filterOption, "");
                        final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                        builder.setTitle("Fehler! ");
                        builder.setMessage("Die nachrichten existieren nicht");
                        builder.setCancelable(false);
                        builder.setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        });
                        builder.show();
                        Utils.hideProgressDialog(progressDialog);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    private void getFirstPageServiceList(int filterOption, String letter) {
        Utils.showProgressDialog(progressDialog);
        apiService.getListService("Bearer " + accessToken, filterOption, letter)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<ServiceResponse>() {
                    @Override
                    public void onNext(ServiceResponse serviceResponse) {
                        totalData = serviceResponse.getTotal();
                        if (totalData != 0) {
                            TOTAL_PAGES = Math.round(totalData / 10) + 1;
                        } else {
                            TOTAL_PAGES = 0;
                        }

                        listData = serviceResponse.getData();
                        adapterServiceListLoadMore.addAll(listData);

                        isLastPage = true;
//                        if (currentPage <= TOTAL_PAGES) {
//                            adapterServiceListLoadMore.addLoadingFooter();
//                            isLastPage = false;
//                        } else {
//                            isLastPage = true;
//                        }

                        Utils.hideProgressDialog(progressDialog);
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }
}
