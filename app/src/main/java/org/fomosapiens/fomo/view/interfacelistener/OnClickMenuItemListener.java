package org.fomosapiens.fomo.view.interfacelistener;

import org.fomosapiens.fomo.model.Category;

/**
 * Created by PC162 on 8/15/2017.
 */

public interface OnClickMenuItemListener {
    void onClickMenuItem(Category category);
}
