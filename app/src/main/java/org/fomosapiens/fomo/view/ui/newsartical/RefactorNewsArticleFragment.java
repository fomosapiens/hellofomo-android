package org.fomosapiens.fomo.view.ui.newsartical;


import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.fomosapiens.fomo.R;
import org.fomosapiens.fomo.SharePre;
import org.fomosapiens.fomo.Utils;
import org.fomosapiens.fomo.database.FomoDatabase;
import org.fomosapiens.fomo.database.news_favor.NewsFavorEnity;
import org.fomosapiens.fomo.model.Category;
import org.fomosapiens.fomo.model.DashBoard;
import org.fomosapiens.fomo.model.News;
import org.fomosapiens.fomo.model.NewsDetail;
import org.fomosapiens.fomo.model.response.ResponseListNews;
import org.fomosapiens.fomo.model.response.ResponseNewsDetail;
import org.fomosapiens.fomo.net.APIHelper;
import org.fomosapiens.fomo.net.APIService;
import org.fomosapiens.fomo.net.APIUtils;
import org.fomosapiens.fomo.view.adapter.AdapterDasBoardItem;
import org.fomosapiens.fomo.view.adapter.AdapterNewsArticleItem;
import org.fomosapiens.fomo.view.customview.FontChange;
import org.fomosapiens.fomo.view.interfacelistener.OnClickNewsDetailListener;
import org.fomosapiens.fomo.view.interfacelistener.OnRecyclerViewScrollListener;
import org.fomosapiens.fomo.view.interfacelistener.OnResponseGetDashBoardListener;
import org.fomosapiens.fomo.view.interfacelistener.OnResponseGetListNewsListener;
import org.fomosapiens.fomo.view.interfacelistener.OnSetDataFinishListener;
import org.fomosapiens.fomo.view.ui.RefactorPrimaryBaseActivity;
import org.fomosapiens.fomo.view.ui.newsdetail.RefactorNewsDetailActivity;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Completable;
import io.reactivex.CompletableObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

import static android.app.Activity.RESULT_OK;


/**
 * A simple {@link Fragment} subclass.
 */
public class RefactorNewsArticleFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener, OnRecyclerViewScrollListener, OnClickNewsDetailListener,
        OnSetDataFinishListener {

    private Category category;
    private AdapterNewsArticleItem adapterNewsArticleItem;
    private int page = 0;
    private int currentList = 0;
    private FomoDatabase fomoDatabase = null;

    private List<DashBoard> dashBoards;
    private int size = 0;
    private int requestCount = 0;
    private Dialog progressDialog;

    @BindView(R.id.refactor_new_article_recycle_view)
    RecyclerView articleRecycleView;
    @BindView(R.id.refactor_new_article_swipe_layout)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.refactor_new_article_no_post)
    TextView noPostFound;

    public static RefactorNewsArticleFragment newsArticleFragment(Category category) {
        RefactorNewsArticleFragment fragment = new RefactorNewsArticleFragment();
        fragment.category = category;
        return fragment;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Utils.setFontName(getContext(), (ViewGroup)this.getView());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.refactor_fragment_news_article, container, false);
        ButterKnife.bind(this, view);
        progressDialog = new Dialog(getContext());
        fomoDatabase = FomoDatabase.getDatabase(getContext());
        swipeRefreshLayout.setOnRefreshListener(this);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (category != null) {
            if (category.getId() == 0) {
                getListNewsHolder();
            } else if (category.getId() != 0 && category.getId() != -1){
                getListCategoryHolder();
            } else if (category.getId() == -1) {
                loadNewsFavorList();
            }
        }
    }

    @Override
    public void onRefresh() {
        if (category.getId() == 0) {
            getListNewsHolder();
        } else if (category.getId() != 0 && category.getId() != -1){
            getListCategoryHolder();
        } else {
            loadNewsFavorList();
        }
    }

    private void initRecycleView(final List<News> newsList){
        if (newsList == null) {
            return;
        }

        OnClickNewsDetailListener onClickNewsDetailListener = new OnClickNewsDetailListener() {
            @Override
            public void onClickItem(News news) {
                goToNewsDetail(news);
            }
        };
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        adapterNewsArticleItem = new AdapterNewsArticleItem(getContext(), newsList, onClickNewsDetailListener);
        adapterNewsArticleItem.setLinearLayoutManager(linearLayoutManager);
        adapterNewsArticleItem.setRecyclerView(articleRecycleView);
        adapterNewsArticleItem.setOnRecyclerViewListener(this);
        articleRecycleView.setLayoutManager(linearLayoutManager);
        articleRecycleView.setAdapter(adapterNewsArticleItem);
        noPostFound.setVisibility(View.GONE);

        if (newsList.size() == 0) {
            noPostFound.setVisibility(View.VISIBLE);
        }
        adapterNewsArticleItem.notifyDataSetChanged();
    }

    private void dismissProgress(){
        if (swipeRefreshLayout.isRefreshing())
            swipeRefreshLayout.setRefreshing(false);

        Utils.hideProgressDialog(progressDialog);
    }

    private void getNewsList(final boolean isLoadMore){
        if (category == null)
            return;

        if (Utils.getAccessToken() == null)
            return;

        if (!isLoadMore && !swipeRefreshLayout.isRefreshing())
            Utils.showProgressDialog(progressDialog);
        loadNewsFavorList();
        dismissProgress();
    }

    @Override
    public void onLoadMoreListener() {
//        getNewsList(true);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Utils.KEY_REQUEST_OPEN_NEWS_DETAIL) {
            if (resultCode == RESULT_OK) {
                loadNewsFavorList();
            }
        }
    }

    private void goToNewsDetail(News news){
        if (news == null)
            return;
        Intent intent = new Intent(getContext(), RefactorNewsDetailActivity.class);
        intent.putExtra(Utils.KEY_NEWS_ID, news.getId());
        intent.putExtra(Utils.KEY_CATEGORY_FAVOR, category.getId());
        startActivityForResult(intent, Utils.KEY_REQUEST_OPEN_NEWS_DETAIL);
    }

    /**
     * load favorite and check existing on server
     * */
    private void loadNewsFavorList() {
        fomoDatabase.newsFavorDao().getAllNewsFavor()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Consumer<List<NewsFavorEnity>>() {
                    @Override
                    public void accept(List<NewsFavorEnity> newsFavorEnities) throws Exception {
                        if (newsFavorEnities != null) {
                            getAllNews(newsFavorEnities);
                        } else {
                        }
                    }
                });
    }

    private void getAllNews(final List<NewsFavorEnity> newsFavorEnities) {

        APIService service = APIUtils.getAPIService();
        String accessToken = Utils.getAccessToken();
        String lang = Utils.getLanguage();
        service.getListNewsArticleWithOutCategory("Bearer " + accessToken, lang, "50")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<ResponseListNews>() {
                    @Override
                    public void onNext(ResponseListNews responseListNews) {
                        List<News> listNews = new ArrayList<>();
                        if (responseListNews.getData() != null) {
                            for (int i = 0; i < responseListNews.getData().size(); i++) {
                                for (int j = 0; j < newsFavorEnities.size(); j++) {
                                    if (responseListNews.getData().get(i).getId() ==  newsFavorEnities.get(j).getNews_id()) {
                                        News news = new News();
                                        news.setTitle(responseListNews.getData().get(i).getTitle());
                                        news.setId(responseListNews.getData().get(i).getId());
                                        news.setAvailable_date(responseListNews.getData().get(i).getAvailable_date());
                                        news.setDescription(responseListNews.getData().get(i).getDescription());
                                        news.setPrimary_image(responseListNews.getData().get(i).getPrimary_image());
                                        listNews.add(news);
                                    }
                                }
                            }
                            initRecycleView(listNews);
                            List<Long> list1 = new ArrayList<>();
                            for (int k = 0; k < newsFavorEnities.size(); k++) {
                                list1.add(newsFavorEnities.get(k).getNews_id());
                            }
                            List<Long> list2 = new ArrayList<>();
                            for (int m = 0; m < listNews.size(); m++) {
                                list2.add(listNews.get(m).getId());
                            }
                            // Prepare a union
                            Set<Long> union = new HashSet<Long>(list1);
                            union.addAll(list2);
                            // Prepare an intersection
                            Set<Long> intersection = new HashSet<Long>(list1);
                            intersection.retainAll(list2);
                            // Subtract the intersection from the union
                            union.removeAll(intersection);
                            // Print the result
                            for (long n : union) {
                                final int i = (int) n;
                                fomoDatabase.newsFavorDao().newsFavorInfo(i)
                                        .subscribeOn(Schedulers.io())
                                        .observeOn(AndroidSchedulers.mainThread())
                                        .subscribe(new Consumer<NewsFavorEnity>() {
                                            @Override
                                            public void accept(NewsFavorEnity newsFavorEnity) throws Exception {
                                                final NewsFavorEnity newsFavor = newsFavorEnity;
                                                Completable.fromAction(new Action() {
                                                    @Override
                                                    public void run() throws Exception {
                                                        fomoDatabase.newsFavorDao().deleteNewsFavor(newsFavor);
                                                    }
                                                }).subscribeOn(Schedulers.io())
                                                        .observeOn(AndroidSchedulers.mainThread())
                                                        .subscribe(new CompletableObserver() {
                                                            @Override
                                                            public void onSubscribe(Disposable d) {

                                                            }

                                                            @Override
                                                            public void onComplete() {

                                                            }

                                                            @Override
                                                            public void onError(Throwable e) {

                                                            }
                                                        });
                                            }
                                        });

                            }
                        }

                    }

                    @Override
                    public void onError(Throwable e) {
                        dismissProgress();
                    }

                    @Override
                    public void onComplete() {
                        dismissProgress();
                    }
                });
    }



    /**
     * Add layout news holder
     * */
    private void getListNewsHolder() {
        if (!((RefactorPrimaryBaseActivity)getActivity()).isOnline())
            return;
        if (!swipeRefreshLayout.isRefreshing()) {
            Utils.showProgressDialog(progressDialog);
        }

        requestCount = 0;
        APIHelper.getListNewsHolder(getDashBoardListener());
    }

    /**
     * Add layout category holder
     * */
    private void getListCategoryHolder() {
        if (!((RefactorPrimaryBaseActivity)getActivity()).isOnline())
            return;
        if (!swipeRefreshLayout.isRefreshing()) {
            Utils.showProgressDialog(progressDialog);
        }

        requestCount = 0;
        APIHelper.getListCategoryHolder(getDashBoardListener());
    }

    private OnResponseGetDashBoardListener getDashBoardListener() {
        OnResponseGetDashBoardListener dashBoardListener = new OnResponseGetDashBoardListener() {
            @Override
            public void OnRequestSuccess(List<DashBoard> mDashBoards) {
                if (mDashBoards==null || mDashBoards.size() == 0) {
                    dismissProgress();
                    return;
                }

                size = mDashBoards.size();
                dashBoards = mDashBoards;
                int position = 0;
                for (DashBoard dashBoard : mDashBoards){
                    if (!dashBoard.getType().equals("top_news")) {
                        dashBoard.setCategory(String.valueOf(category.getId()));
                    }
                    getListNewsWithCategoryOrTag(dashBoard, position);
                    position++;
                }
            }

            @Override
            public void OnRequestError(Throwable e) {
                dismissProgress();
            }
        };
        return dashBoardListener;
    }

    private void getListNewsWithCategoryOrTag(DashBoard dashBoard, int position) {
        APIHelper.getListNewsWithCategoryOrTag(dashBoard, getOnResponseListNews(position));
    }

    private OnResponseGetListNewsListener getOnResponseListNews(final int position) {
        OnResponseGetListNewsListener responseGetListNewsListener = new OnResponseGetListNewsListener() {
            @Override
            public void OnRequestSuccess(final List<News> newsList, final DashBoard dashBoard) {
                requestCount++;
                dashBoards.get(position).setNewsList(newsList);

                if (requestCount == size) {
                    setAdapterForRecycler(dashBoards);
                    dismissProgress();
                }
            }

            @Override
            public void OnRequestError(Throwable e) {
                dismissProgress();
            }
        };
        return responseGetListNewsListener;
    }

    private void setAdapterForRecycler(List<DashBoard> dashBoards){
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        AdapterDasBoardItem adapterDasBoardItem = new AdapterDasBoardItem(dashBoards, getContext(), this, this);
        articleRecycleView.setLayoutManager(mLayoutManager);
        articleRecycleView.setAdapter(adapterDasBoardItem);
    }

    private void goToNewsDetailActivity(News news) {
        Intent intent = new Intent(getContext(), RefactorNewsDetailActivity.class);
        intent.putExtra(Utils.KEY_NEWS_ID, news.getId());
        startActivity(intent);
    }

    @Override
    public void onClickItem(News news) {
        if (((RefactorPrimaryBaseActivity)getActivity()).isOnline()) {
            goToNewsDetailActivity(news);
        }
    }

    @Override
    public void onLoadFinish() {
        dismissProgress();
    }
}
