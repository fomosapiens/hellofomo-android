package org.fomosapiens.fomo.view.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.fomosapiens.fomo.R;
import org.fomosapiens.fomo.Utils;
import org.fomosapiens.fomo.model.service.FilterLetter;
import org.fomosapiens.fomo.view.ui.RefactorPrimaryBaseActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AdapterLetterList extends RecyclerView.Adapter<AdapterLetterList.ViewHolder> {

    private List<FilterLetter> letters;
    private LetterClick letterClick;
    private Context context;
    private int rowIndex = 0;

    public AdapterLetterList(Context context, List<FilterLetter> letters, LetterClick letterClick) {
        this.letters = letters;
        this.context = context;
        this.letterClick = letterClick;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_header_text, parent, false);
        Utils.setFontName(context, (ViewGroup)view);
        return new AdapterLetterList.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final FilterLetter letter = letters.get(position);
        holder.letter.setText(letter.getLetter().toUpperCase());
        holder.letter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (((RefactorPrimaryBaseActivity)context).isOnline()) {
                    rowIndex = holder.getAdapterPosition();
                    letterClick.itemClick(letter.getLetter());
                    notifyDataSetChanged();
                }

            }
        });
        if (rowIndex == position) {
            holder.letter.setBackground(ContextCompat.getDrawable(context, R.drawable.custom_black_btn_bg));
            holder.letter.setTextColor(ContextCompat.getColor(context, R.color.color_white));
        } else {
            holder.letter.setBackground(ContextCompat.getDrawable(context, android.R.color.transparent));
            holder.letter.setTextColor(ContextCompat.getColor(context, R.color.color_black));
        }
    }

    @Override
    public int getItemCount() {
        return letters.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.item_register_tv)
        TextView letter;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface LetterClick {
        void itemClick(String letter);
    }
}
