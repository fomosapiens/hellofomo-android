package org.fomosapiens.fomo.view.customview;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.fomosapiens.fomo.R;
import org.fomosapiens.fomo.Utils;
import org.fomosapiens.fomo.model.News;
import org.fomosapiens.fomo.view.interfacelistener.OnClickNewsDetailListener;
import org.fomosapiens.fomo.view.ui.ViewUtil;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by PC162 on 8/9/2017.
 */

public class ViewListItem extends RelativeLayout implements View.OnClickListener {

    @BindView(R.id.vListItemImgIcon)
    protected ImageView vListItemImgIcon;

    @BindView(R.id.vListItemTxtDate)
    protected TextView vListItemTxtDate;

    @BindView(R.id.vListItemTxtTitle)
    protected TextView vListItemTxtTitle;

    @BindView(R.id.vliViewLastLine)
    View vliViewLastLine;

    private OnClickNewsDetailListener onClickNewsDetailListener;
    private boolean isShowPicture;
    private boolean isLastItem;
    private Context context;
    private News news;
    private View root;

    public ViewListItem(Context context, News news, OnClickNewsDetailListener onClickNewsDetailListener, boolean isShowPicture, boolean isLastItem) {
        super(context);
        this.onClickNewsDetailListener = onClickNewsDetailListener;
        this.isShowPicture = isShowPicture;
        this.isLastItem = isLastItem;
        this.context = context;
        this.news = news;
        initLayout();
    }

    public ViewListItem(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ViewListItem(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    private void initLayout(){
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        root = inflater.inflate(R.layout.view_list_item, this, true);
        ButterKnife.bind(this, root);
        root.setOnClickListener(this);
        initData();
        Utils.setFontName(context, (ViewGroup)root);
    }

    private void initData(){
        String imageUrl = news.getPrimary_image();
        vListItemTxtDate.setText(Utils.getDate(news.getAvailable_date()));
        vListItemTxtTitle.setText(news.getTitle());
        vListItemTxtTitle.setOnClickListener(this);

        if (isShowPicture)
            ViewUtil.setBackgroundForImageView(context, imageUrl, vListItemImgIcon);

        else {
            LayoutParams params = (LayoutParams) vListItemImgIcon.getLayoutParams();
            params.width = 0;
            params.setMargins(0,0,0,0);
            vListItemImgIcon.setLayoutParams(params);
            vListItemImgIcon.setVisibility(View.INVISIBLE);
        }
        if (isLastItem)
            vliViewLastLine.setVisibility(View.GONE);

    }

    @Override
    public void onClick(View view) {
        onClickNewsDetailListener.onClickItem(news);
    }
}
