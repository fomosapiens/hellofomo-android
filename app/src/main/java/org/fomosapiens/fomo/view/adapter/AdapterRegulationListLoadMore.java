package org.fomosapiens.fomo.view.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;

import org.fomosapiens.fomo.R;
import org.fomosapiens.fomo.Utils;
import org.fomosapiens.fomo.model.regulation.RegulationListData;
import org.fomosapiens.fomo.view.customview.FontChange;
import org.fomosapiens.fomo.view.ui.regulation.RegulationDetailActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AdapterRegulationListLoadMore extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int ITEM = 0;
    private static final int LOADING = 1;
    private boolean isLoadingAdded = false;

    private List<RegulationListData> listResponses;
    private Context context;

    public AdapterRegulationListLoadMore(Context context) {
        this.context = context;
        listResponses = new ArrayList<>();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());


        switch (viewType) {
            case ITEM:
                viewHolder = getViewHolder(parent, inflater);
                break;

            case LOADING:
                View v2 = inflater.inflate(R.layout.item_progress, parent, false);
                viewHolder = new LoadingVH(v2);
        }
        return viewHolder;
    }

    @NonNull
    private RecyclerView.ViewHolder getViewHolder(ViewGroup parent, LayoutInflater inflater) {
        RecyclerView.ViewHolder viewHolder;
        View v1 = inflater.inflate(R.layout.refactor_item_regulation_list, parent, false);
        viewHolder = new RegulationVH(v1);
        Utils.setFontName(context, parent);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        final RegulationListData data = listResponses.get(position);

        switch (getItemViewType(position)) {
            case ITEM:
                final RegulationVH regulationVH = (RegulationVH) holder;

                Spannable sequence;
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                    sequence = (Spannable) Html.fromHtml(data.getDetails().get(0).getDescription(), Html.FROM_HTML_MODE_LEGACY, null, null);
                } else {
                    sequence = (Spannable) Html.fromHtml(data.getDetails().get(0).getDescription(), null, null);
                }
                SpannableStringBuilder strBuilder = new SpannableStringBuilder(sequence);
                if (data.getState().equals("valid")) {
                    regulationVH.kindOfRegulation.setVisibility(View.INVISIBLE);
                } else {
                    regulationVH.kindOfRegulation.setVisibility(View.VISIBLE);
                    regulationVH.kindOfRegulation.setText(context.getString(R.string.state_not_valid));
                    regulationVH.kindOfRegulation.setBackgroundColor(ContextCompat.getColor(context, R.color.regulation_not_valid_color_bg));
                }
                regulationVH.title.setText(strBuilder);
                regulationVH.regulationItem.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Gson gson = new Gson();
                        String object = gson.toJson(data);
                        Intent intent = new Intent(context, RegulationDetailActivity.class);
                        intent.putExtra("regulation_detail_id", object);
                        context.startActivity(intent);
                    }
    });
                break;

            case LOADING:
                break;

        }

    }

    @Override
    public int getItemCount() {
        return listResponses == null ? 0 : listResponses.size();
    }

    @Override
    public int getItemViewType(int position) {
        return (position == listResponses.size() - 1 && isLoadingAdded) ? LOADING : ITEM;
    }

    /*
        Helper
    */
    public void add(RegulationListData data) {
        listResponses.add(data);
        notifyItemInserted(listResponses.size() - 1);
    }

    public void addAll(List<RegulationListData> listData) {
        for (RegulationListData data : listData) {
            add(data);
        }
    }

    public void removeAll() {
        listResponses = new ArrayList<>();
        notifyDataSetChanged();
    }

    public void addLoadingFooter() {
        isLoadingAdded = true;
        add(new RegulationListData());
    }

    public void removeLoadingFooter() {
        isLoadingAdded = false;

        int position = listResponses.size() - 1;
        RegulationListData data = getItem(position);

        if (data != null) {
            listResponses.remove(position);
            notifyItemRemoved(position);
        }
    }

    public RegulationListData getItem(int position) { return listResponses.get(position); }

    protected class RegulationVH extends RecyclerView.ViewHolder {

        @BindView(R.id.regulation_item_kind_result)
        TextView kindOfRegulation;
        @BindView(R.id.regulation_item_title)
        TextView title;
        @BindView(R.id.regulation_item_view_bottom)
        View viewBottom;
        @BindView(R.id.regulation_item)
        LinearLayout regulationItem;

        public RegulationVH(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    protected class LoadingVH extends RecyclerView.ViewHolder {
        public LoadingVH(View itemView) {super(itemView);}
    }

}
