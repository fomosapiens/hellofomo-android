package org.fomosapiens.fomo.view.interfacelistener;

import org.fomosapiens.fomo.model.DashBoard;
import org.fomosapiens.fomo.model.News;

import java.util.List;

/**
 * Created by PC162 on 8/27/2017.
 */

public interface OnResponseGetListNewsListener {
    void OnRequestSuccess(List<News> newsList, DashBoard dashBoard);
    void OnRequestError(Throwable e);
}
