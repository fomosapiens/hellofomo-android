package org.fomosapiens.fomo.view.interfacelistener;

import org.fomosapiens.fomo.model.DashBoard;

import java.util.List;

/**
 * Created by PC162 on 8/27/2017.
 */

public interface OnResponseGetDashBoardListener {
    void OnRequestSuccess(List<DashBoard> mDashBoards);
    void OnRequestError(Throwable e);
}
