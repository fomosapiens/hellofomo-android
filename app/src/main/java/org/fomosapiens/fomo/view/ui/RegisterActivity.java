package org.fomosapiens.fomo.view.ui;

import android.app.Dialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;

import org.fomosapiens.fomo.R;
import org.fomosapiens.fomo.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RegisterActivity extends BaseActivity {

    @BindView(R.id.register_webview)
    WebView webView;
    @BindView(R.id.register_toolbar_layout)
    android.support.v7.widget.Toolbar toolbar;
    @BindView(R.id.register_back_img)
    ImageView backBtn;

    private Dialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);
        progressDialog = new Dialog(RegisterActivity.this);
        setSupportActionBar(toolbar);
        Utils.showProgressDialog(progressDialog);

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        // load webview
        WebViewClient client = new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                Utils.hideProgressDialog(progressDialog);
            }
        };
        webView.setWebViewClient(client);

        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        webView.loadUrl("https://veranstaltungen.rgc-manager.de/api/v2/getlpregister");
    }
}
