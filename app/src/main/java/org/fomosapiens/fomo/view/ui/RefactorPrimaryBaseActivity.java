package org.fomosapiens.fomo.view.ui;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import org.fomosapiens.fomo.R;
import org.fomosapiens.fomo.SharePre;
import org.fomosapiens.fomo.Utils;
import org.fomosapiens.fomo.model.Category;
import org.fomosapiens.fomo.model.SubMenuItem;
import org.fomosapiens.fomo.model.response.ResponseCategory;
import org.fomosapiens.fomo.net.APIService;
import org.fomosapiens.fomo.net.APIUtils;
import org.fomosapiens.fomo.view.adapter.AdapterMenuItem;
import org.fomosapiens.fomo.view.ui.authen.LoginActivity;
import org.fomosapiens.fomo.view.ui.dashboard.DashBoardFragment;
import org.fomosapiens.fomo.view.ui.news.NewsCategoryFragment;
import org.fomosapiens.fomo.view.ui.news.NewsFragment;
import org.fomosapiens.fomo.view.ui.newsdetail.RefactorNewsDetailActivity;
import org.fomosapiens.fomo.view.ui.regulation.HistoryRegulationFragments;
import org.fomosapiens.fomo.view.ui.regulation.RegulationsFragment;
import org.fomosapiens.fomo.view.ui.service.ServiceListFragment;
import org.fomosapiens.fomo.view.ui.settings.SettingsFragment;

import java.util.ArrayList;
import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;

public class RefactorPrimaryBaseActivity extends BaseActivity implements View.OnClickListener {

    @BindView(R.id.refactor_primary_toolbar_title)
    TextView titleTv;
    @BindView(R.id.refactor_drawer_layout)
    DrawerLayout drawerLayout;
    @BindView(R.id.refactor_primary_toolbar_menu)
    ImageView drawerIcon;
    @BindView(R.id.drawer)
    View includeDrawer;

    // Fragment Tag
    private static final String TAG_DASHBOARD = "org.fomosapiens.fomo.view.ui.dashboard.DashBoardActivity";
    private static final String TAG_NEWS = "org.fomosapiens.fomo.view.ui.dashboard.NewsActivity";
    private static final String TAG_SETTINGS = "org.fomosapiens.fomo.view.ui.dashboard.SettingsActivity";
    private static final String TAG_NEWS_CATEGORY = "org.fomosapiens.fomo.view.ui.dashboard.NewsActivityCategory";
    private static final String TAG_REGULATIONS = "org.fomosapiens.fomo.view.ui.dashboard.RegulationsActivity";
    private static final String TAG_UPDATE_REGULATIONS = "org.fomosapiens.fomo.view.ui.dashboard.UpdateRegulationsActivity";
    private static final String TAG_COMPANIES = "org.fomosapiens.fomo.view.ui.service.Company";
    private static final String TAG_MARKET_PLACE = "org.fomosapiens.fomo.view.ui.service.Marketplace";
    private static final String TAG_EVENTS = "org.fomosapiens.fomo.view.ui.dashboard.EventFragments";
    private static String CURRENT_TAG = TAG_DASHBOARD;

    private static final int INDEX_DASHBOARD = 0;
    private static final int INDEX_NEWS = 1;
    private static final int INDEX_SETTINGS = 2;
    private static final int INDEX_CATEGORY = 3;
    private static final int INDEX_REGULATION = 4;
    private static final int INDEX_EVENTS = 5;
    private static final int INDEX_UPDATE_REGULATION = 6;
    private static final int INDEX_COMPANIES = 7;
    private static final int INDEX_MARKET_PLACE = 8;

    private Handler hander;
    private boolean shouldLoadHomeFragOnBackPress = true;
    List<Category> newList = new ArrayList<>();
    private RecyclerView menuRecycleView;
    private Dialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.refactor_activity_primary_base);
        ButterKnife.bind(this);
        progressDialog = new Dialog(this);

        // change font
        Utils.setFontName(this, (ViewGroup)this.findViewById(android.R.id.content));

        getListCategories();

        hander = new Handler();

        // set drawer layout
        menuRecycleView = includeDrawer.findViewById(R.id.refactor_menu_recycle_view);
        ImageView closerDrawerIcon = includeDrawer.findViewById(R.id.refactor_menu_icon);
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, null, R.string.open_drawer, R.string.close_drawer) {

            @Override
            public void onDrawerClosed(View drawerView) {
                // Code here will be triggered once the drawer closes as we dont want anything to happen so we leave this blank
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                // Code here will be triggered once the drawer open as we dont want anything to happen so we leave this blank
                super.onDrawerOpened(drawerView);
            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
            }
        };
        drawerLayout.setDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
        loadFragment(0, TAG_DASHBOARD);

        drawerIcon.setOnClickListener(this);
        closerDrawerIcon.setOnClickListener(this);

        Bundle b = getIntent().getExtras();
        if (b != null) {
            long id = b.getLong("data_from_notification");
            Intent intent = new Intent(this, RefactorNewsDetailActivity.class);
            intent.putExtra(Utils.KEY_NEWS_ID, id);
            startActivity(intent);
        }
    }

    @Override
    public void setContentView(View view) {
        super.setContentView(view);
        getListCategories();
        loadFragment(0, TAG_DASHBOARD);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.refactor_primary_toolbar_menu:
                drawerLayout.openDrawer(Gravity.END);
                break;

            case R.id.refactor_menu_icon:
                drawerLayout.closeDrawers();
                break;

        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        Utils.hideProgressDialog(progressDialog);
    }

    @Override
    protected void onStop() {
        super.onStop();
        Utils.hideProgressDialog(progressDialog);
    }

    // get list category
    protected void getListCategories(){
        String accessToken = Utils.getAccessToken();
        APIUtils.getAPIService().getListCategories("Bearer " + accessToken)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<ResponseCategory>() {
                    @Override
                    public void onNext(@NonNull ResponseCategory responseCategory) {
                        newList = new ArrayList<>();
                        List<Category> categoryList = responseCategory.getData();
                        newList.add(new Category(0, "Alle"));
                        for (int i = 0; i < categoryList.size(); i++) {
                            newList.add(new Category(categoryList.get(i).getId(), categoryList.get(i).getName()));
                        }
                        newList.add(new Category(-1, "Favoriten"));
                        menuListRefactor(newList);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        Log.e("BaseActivity","onError");
                    }

                    @Override
                    public void onComplete() {
                        Log.e("BaseActivity","onComplete");
                    }
                });
    }

    private void menuListRefactor(List<Category> categories) {
        List<SubMenuItem> menuList = new ArrayList<>();
        String[] array = getResources().getStringArray(R.array.main_menu_item);
        for (String s : array) {
            menuList.add(new SubMenuItem(s));
        }

        AdapterMenuItem.MenuItemClick menuItemClick = new AdapterMenuItem.MenuItemClick() {
            @Override
            public void titleClick(String title) {
                if (title.equals(getString(R.string.settings))) {
                    if (!CURRENT_TAG.equals(TAG_SETTINGS)) {
                        loadFragment(INDEX_SETTINGS, TAG_SETTINGS);
                        CURRENT_TAG = TAG_SETTINGS;
                    } else {
                        drawerLayout.closeDrawers();
                    }
                } else if (title.equals(getString(R.string.dashboard))) {
                    if (!CURRENT_TAG.equals(TAG_DASHBOARD)) {
                        loadFragment(INDEX_DASHBOARD, TAG_DASHBOARD);
                        CURRENT_TAG = TAG_DASHBOARD;
                    } else {
                        drawerLayout.closeDrawers();
                    }
                } else if (title.equals(getString(R.string.news))) {
                    if (!CURRENT_TAG.equals(TAG_NEWS)) {
                        loadFragment(INDEX_NEWS, TAG_NEWS);
                        CURRENT_TAG = TAG_NEWS;
                    } else {
                        drawerLayout.closeDrawers();
                    }
                } else if (title.equals(getString(R.string.login_title))){
                    logout();
                    hander.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            drawerLayout.closeDrawers();
                        }
                    }, 100);
                } else if (title.equals(getString(R.string.event_title))) {
                    if (!CURRENT_TAG.equals(TAG_EVENTS)) {
                        loadFragment(INDEX_EVENTS, TAG_EVENTS);
                        CURRENT_TAG = TAG_EVENTS;
                    } else {
                        drawerLayout.closeDrawers();
                    }
                }
            }
        };

        AdapterMenuItem adapterMenuItem = new AdapterMenuItem(this, menuList, categories, menuItemClick);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        menuRecycleView.setLayoutManager(mLayoutManager);
        menuRecycleView.setAdapter(adapterMenuItem);
    }

    // load dashboard fragment
    private void loadFragment(final int currentFragment, final String currentTag) {
//        if (getSupportFragmentManager().findFragmentByTag(currentTag) != null) {
//            drawerLayout.closeDrawers();
//            return;
//        }
        Runnable mPendingRunnable = new Runnable() {
            @Override
            public void run() {
                Fragment fragment = getFragment(currentFragment);
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.refactor_primary_container, fragment, currentTag);
                fragmentTransaction.commitAllowingStateLoss();
            }
        };
        if (mPendingRunnable != null) {
            hander.postDelayed(mPendingRunnable, 300);
        }
        drawerLayout.closeDrawers();
        invalidateOptionsMenu();
    }

    // load category fragment
    public void loadCategoryNews(final int position) {
        CURRENT_TAG = TAG_NEWS_CATEGORY;
        shouldLoadHomeFragOnBackPress = true;
        titleTv.setText(getBaseContext().getString(R.string.news));
        Runnable mPendingRunnable = new Runnable() {
            @Override
            public void run() {
                NewsCategoryFragment fragment = new NewsCategoryFragment();
                Bundle bundle=new Bundle();
                bundle.putParcelableArrayList("news_category_list", (ArrayList<? extends Parcelable>) newList);
                bundle.putInt("news_category_position", position);
                fragment.setArguments(bundle);
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.refactor_primary_container, fragment, "");
                fragmentTransaction.commitAllowingStateLoss();
            }
        };
        if (mPendingRunnable != null) {
            hander.postDelayed(mPendingRunnable, 300);
        }
        drawerLayout.closeDrawers();
        invalidateOptionsMenu();
    }

    public void gotoKontaktActivity(){
        Intent intent = new Intent(this, KonTaktActivity.class);
        startActivity(intent);
    }

    public void gotoDatenschutz(){
        Intent intent = new Intent(this, DatenchutzActivity.class);
        startActivity(intent);
    }

    public void gotoImpressum(){
        Intent intent = new Intent(this, ImpressumActivity.class);
        startActivity(intent);
    }

    public void gotoAgb(){
        Intent intent = new Intent(this, AGBActivity.class);
        startActivity(intent);
    }

    public void goToCompanies() {
        loadFragment(INDEX_COMPANIES, TAG_COMPANIES);
        CURRENT_TAG = TAG_COMPANIES;
    }

    public void goToMarketPlace() {
        loadFragment(INDEX_MARKET_PLACE, TAG_MARKET_PLACE);
        CURRENT_TAG = TAG_MARKET_PLACE;
    }

    public void goToRegulation() {
        loadFragment(INDEX_REGULATION, TAG_REGULATIONS);
        CURRENT_TAG = TAG_REGULATIONS;
    }

    public void goToHistoryRegulation() {
        loadFragment(INDEX_UPDATE_REGULATION, TAG_UPDATE_REGULATIONS);
        CURRENT_TAG = TAG_UPDATE_REGULATIONS;
    }

    private Fragment getFragment(int navIndex) {
        switch (navIndex) {
            case 0:
                DashBoardFragment dashBoardFragment = new DashBoardFragment();
                titleTv.setText(getBaseContext().getString(R.string.dashboard));
                shouldLoadHomeFragOnBackPress = false;
                return dashBoardFragment;

            case 1:
                NewsFragment newsFragment = new NewsFragment();
                titleTv.setText(getBaseContext().getString(R.string.news));
                shouldLoadHomeFragOnBackPress = true;
                return newsFragment;

            case 2:
                SettingsFragment settingsFragment = new SettingsFragment();
                titleTv.setText(getBaseContext().getString(R.string.settings));
                shouldLoadHomeFragOnBackPress = true;
                return settingsFragment;

            case 4:
                RegulationsFragment regulationsFragment = new RegulationsFragment();
                titleTv.setText(getBaseContext().getString(R.string.regulations));
                shouldLoadHomeFragOnBackPress = true;
                return regulationsFragment;

            case 5:
                EventFragment eventFragment = new EventFragment();
                titleTv.setText(getBaseContext().getString(R.string.event_title));
                shouldLoadHomeFragOnBackPress = true;
                return eventFragment;

            case 6:
                HistoryRegulationFragments updateRegulationFragments = new HistoryRegulationFragments();
                titleTv.setText(getBaseContext().getString(R.string.update_regulations_title));
                shouldLoadHomeFragOnBackPress = true;
                return updateRegulationFragments;

            case 7:
                Bundle bundleCompany = new Bundle();
                bundleCompany.putInt(Utils.KEY_FILTER_OPTION, 0);
                ServiceListFragment companyListFragment = new ServiceListFragment();
                companyListFragment.setArguments(bundleCompany);
                titleTv.setText(getBaseContext().getString(R.string.company).toUpperCase());
                shouldLoadHomeFragOnBackPress = true;
                return companyListFragment;

            case 8:
                Bundle bundleMarketPlace = new Bundle();
                bundleMarketPlace.putInt(Utils.KEY_FILTER_OPTION, 1);
                ServiceListFragment marketPlaceFragment = new ServiceListFragment();
                marketPlaceFragment.setArguments(bundleMarketPlace);
                titleTv.setText(getBaseContext().getString(R.string.marketplace).toUpperCase());
                shouldLoadHomeFragOnBackPress = true;
                return marketPlaceFragment;


            default:
                return new DashBoardFragment();
        }
    }

    private void logout() {
        boolean isLogin = SharePre.getBooleanFalse(SharePre.KEY_IS_LOGIN);
        if (isLogin) {
            showAlertLogout();
        } else {
            Utils.hideProgressDialog(progressDialog);
            Intent intent = new Intent(RefactorPrimaryBaseActivity.this, LoginActivity.class);
            startActivity(intent);
            finish();
        }
    }

    private void showAlertLogout() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(RefactorPrimaryBaseActivity.this);
        builder.setTitle(getString(R.string.logout_title_alert));
        builder.setMessage(getString(R.string.logout_content_alert));
        builder.setCancelable(false);
        builder.setNegativeButton("Nein", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        builder.setPositiveButton("Ja", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Utils.showProgressDialog(progressDialog);
                logoutFunction();
                dialogInterface.dismiss();
            }
        });
        builder.show();
    }

    private void logoutFunction() {
        APIService service = APIUtils.getAPIService();
        String accessToken = SharePre.getString(SharePre.KEY_CUSTOMER_ACCESS_TOKEN);
        service.logout("Bearer " + accessToken)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new DisposableObserver<ResponseBody>() {
                    @Override
                    public void onNext(ResponseBody responseBody) {
                        Utils.functionLogout();
                        loadFragment(INDEX_DASHBOARD, TAG_DASHBOARD);
                        getListCategories();
                        Utils.hideProgressDialog(progressDialog);
                    }

                    @Override
                    public void onError(Throwable e) {
                        Utils.functionLogout();
                        loadFragment(INDEX_DASHBOARD, TAG_DASHBOARD);
                        getListCategories();
                        Utils.hideProgressDialog(progressDialog);
                    }

                    @Override
                    public void onComplete() {
                        Utils.hideProgressDialog(progressDialog);
                    }
                });
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(Gravity.END)) {
            drawerLayout.closeDrawers();
            return;
        }

        // This code loads home fragment when back key is pressed
        // when user is in other fragment than home
        if (shouldLoadHomeFragOnBackPress) {
            // checking if user is on other navigation menu
            // rather than home
            CURRENT_TAG = TAG_DASHBOARD;
            loadFragment(0, CURRENT_TAG);
            return;

        }

        super.onBackPressed();
    }
}
