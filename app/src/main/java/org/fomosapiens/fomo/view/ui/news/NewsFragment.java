package org.fomosapiens.fomo.view.ui.news;


import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import org.fomosapiens.fomo.R;
import org.fomosapiens.fomo.Utils;
import org.fomosapiens.fomo.model.DashBoard;
import org.fomosapiens.fomo.model.News;
import org.fomosapiens.fomo.net.APIHelper;
import org.fomosapiens.fomo.view.adapter.AdapterDasBoardItem;
import org.fomosapiens.fomo.view.customview.FontChange;
import org.fomosapiens.fomo.view.interfacelistener.OnClickNewsDetailListener;
import org.fomosapiens.fomo.view.interfacelistener.OnResponseGetDashBoardListener;
import org.fomosapiens.fomo.view.interfacelistener.OnResponseGetListNewsListener;
import org.fomosapiens.fomo.view.interfacelistener.OnSetDataFinishListener;
import org.fomosapiens.fomo.view.ui.RefactorPrimaryBaseActivity;
import org.fomosapiens.fomo.view.ui.newsdetail.NewsDetailActivity;
import org.fomosapiens.fomo.view.ui.newsdetail.RefactorNewsDetailActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class NewsFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener, OnClickNewsDetailListener, OnSetDataFinishListener {

    @BindView(R.id.refactor_news_swipe_layout)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.refactor_news_recycleView)
    RecyclerView recyclerView;
    @BindView(R.id.refactor_news_title)
    TextView noPostTitle;

    private List<DashBoard> dashBoards;
    private int requestCount;
    private int size = 0;
    private Dialog progressDialog;

    public NewsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Utils.setFontName(getContext(), (ViewGroup)this.getView());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.refactor_fragment_news, container, false);
        ButterKnife.bind(this, view);
        progressDialog = new Dialog(getContext());

        getListNewsHolder();
        swipeRefreshLayout.setOnRefreshListener(this);

        return view;
    }

    @Override
    public void onRefresh() {
        if (!((RefactorPrimaryBaseActivity)getActivity()).isOnline()) {
            return;
        }

        getListNewsHolder();
    }

    @Override
    public void onLoadFinish() {
        Utils.hideProgressDialog(progressDialog);
    }

    @Override
    public void onClickItem(News news) {
        if (((RefactorPrimaryBaseActivity)getActivity()).isOnline()) {
            goToNewsDetailActivity(news);
        }
    }

    private void setAdapterForRecycler(List<DashBoard> dashBoards){
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        AdapterDasBoardItem adapterDasBoardItem = new AdapterDasBoardItem(dashBoards, getContext(), this, this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setAdapter(adapterDasBoardItem);
    }

    private void dismissProgressAndRefresh(){
        if (swipeRefreshLayout.isRefreshing())
            swipeRefreshLayout.setRefreshing(false);

        Utils.hideProgressDialog(progressDialog);
    }

    private void goToNewsDetailActivity(News news) {
        Intent intent = new Intent(getContext(), RefactorNewsDetailActivity.class);
        intent.putExtra(Utils.KEY_NEWS_ID, news.getId());
        startActivity(intent);
    }

    private void getListNewsHolder() {
        if (!((RefactorPrimaryBaseActivity)getActivity()).isOnline())
            return;
        if (!swipeRefreshLayout.isRefreshing()) {
            Utils.showProgressDialog(progressDialog);
        }

        requestCount = 0;
        APIHelper.getListNewsHolder(getDashBoardListener());
    }

    private OnResponseGetDashBoardListener getDashBoardListener() {
        OnResponseGetDashBoardListener dashBoardListener = new OnResponseGetDashBoardListener() {
            @Override
            public void OnRequestSuccess(List<DashBoard> mDashBoards) {
                if (mDashBoards==null || mDashBoards.size() == 0) {
                    dismissProgressAndRefresh();
                    noPostTitle.setVisibility(View.VISIBLE);
                    return;
                }

                noPostTitle.setVisibility(View.GONE);
                size = mDashBoards.size();
                dashBoards = mDashBoards;
                int position = 0;
                for (DashBoard dashBoard : mDashBoards){
                    getListNewsWithCategoryOrTag(dashBoard, position);
                    position++;
                }
            }

            @Override
            public void OnRequestError(Throwable e) {
                dismissProgressAndRefresh();
            }
        };
        return dashBoardListener;
    }

    private void getListNewsWithCategoryOrTag(DashBoard dashBoard, int position) {
        APIHelper.getListNewsWithCategoryOrTag(dashBoard, getOnResponseListNews(position));
    }

    private OnResponseGetListNewsListener getOnResponseListNews(final int position) {
        OnResponseGetListNewsListener responseGetListNewsListener = new OnResponseGetListNewsListener() {
            @Override
            public void OnRequestSuccess(final List<News> newsList, final DashBoard dashBoard) {
                requestCount++;
                dashBoards.get(position).setNewsList(newsList);

                if (requestCount == size) {
                    setAdapterForRecycler(dashBoards);
                    dismissProgressAndRefresh();
                }
            }

            @Override
            public void OnRequestError(Throwable e) {
                dismissProgressAndRefresh();
            }
        };
        return responseGetListNewsListener;
    }
}
