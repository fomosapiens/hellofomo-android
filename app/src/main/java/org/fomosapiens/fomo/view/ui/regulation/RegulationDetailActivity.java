package org.fomosapiens.fomo.view.ui.regulation;

import android.Manifest;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.method.LinkMovementMethod;
import android.text.style.URLSpan;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;

import org.fomosapiens.fomo.R;
import org.fomosapiens.fomo.SharePre;
import org.fomosapiens.fomo.Utils;
import org.fomosapiens.fomo.model.NewsDetail;
import org.fomosapiens.fomo.model.regulation.RegulationListData;
import org.fomosapiens.fomo.model.regulation.SingleRegulationResponse;
import org.fomosapiens.fomo.model.response.ResponseNewsDetail;
import org.fomosapiens.fomo.net.APIService;
import org.fomosapiens.fomo.net.APIUtils;
import org.fomosapiens.fomo.view.adapter.AdapterDetailCategory;
import org.fomosapiens.fomo.view.adapter.AdapterRegulationHistory;
import org.fomosapiens.fomo.view.customview.FontChange;
import org.fomosapiens.fomo.view.customview.PicassoImageGetter;
import org.fomosapiens.fomo.view.ui.BaseActivity;
import org.fomosapiens.fomo.view.ui.newsdetail.RefactorNewsDetailActivity;
import org.fomosapiens.fomo.view.ui.newsdetail.RefactorShareAttachmentActivity;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;

public class RegulationDetailActivity extends BaseActivity {

    @BindView(R.id.regulation_detail_toolbar_layout)
    android.support.v7.widget.Toolbar toolbar;
    @BindView(R.id.regulation_back_img)
    ImageView backImg;
    @BindView(R.id.regulation_detail_title)
    TextView title;
    @BindView(R.id.regulation_detail_content)
    TextView content;
    @BindView(R.id.regulation_detail_register)
    TextView registerDetail;
    @BindView(R.id.regulation_detail_art)
    TextView artDetail;
    @BindView(R.id.regulation_detail_subject)
    TextView subjectDetail;
    @BindView(R.id.regulation_detail_ebene)
    TextView beneDetail;
    @BindView(R.id.regulation_detail_legal)
    TextView legalDetail;
    @BindView(R.id.regulation_detail_authority)
    TextView authorityDetail;
    @BindView(R.id.regulation_detail_stand)
    TextView standDetail;
    @BindView(R.id.regulation_detail_regulation_content)
    TextView regulationContent;
    @BindView(R.id.regulation_detail_list_history)
    RecyclerView listHistory;
    @BindView(R.id.regulation_detail_download_btn)
    TextView downloadBtn;
    @BindView(R.id.regulation_list_empty)
    TextView historyEmpty;

    RegulationListData data;
    private Dialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_regulation_detail);
        ButterKnife.bind(this);

        progressDialog = new Dialog(this);
        setSupportActionBar(toolbar);

        // change font
        Utils.setFontName(this, (ViewGroup)this.findViewById(android.R.id.content));

        Gson gson = new Gson();
        data = gson.fromJson(getIntent().getStringExtra("regulation_detail_id"), RegulationListData.class);
        int regulationId = getIntent().getIntExtra("regulation_id", 0);

        if (data != null) {
            setData(data);
        }
        if (regulationId != 0) {
            getDetail(regulationId);
        }

        downloadBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (data != null) {
                    if (data.getDetails().get(0).getFile() != null) {
                        final String fileUrl = data.getDetails().get(0).getFile();
                        if (!fileUrl.equals("")) {
                            final String title = Utils.getFileName(fileUrl);
                            final File file = new File(getExternalFilesDir(null) + File.separator + title);
                            if (file.exists()) {
                                goToShareAcitivity(fileUrl, title);
                            } else {
                                downloadFileAttach(fileUrl, title);
                            }
                        }
                    }
                }
            }
        });

        backImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    private void setData(RegulationListData data) {
        title.setText(data.getDetails().get(0).getDescription());
        setTextFromHtml(data.getContent(), content);
        if (data.getRegisters() != null && data.getRegisters().size() != 0) {
            registerDetail.setText(data.getRegisters().get(0).getName());
        }
        if (data.getMasterDataInDetailResponse().getFieldOfLaws() != null && data.getMasterDataInDetailResponse().getFieldOfLaws().size() != 0) {
            artDetail.setText(data.getMasterDataInDetailResponse().getFieldOfLaws().get(0).getName());
        }

        if (data.getMasterDataInDetailResponse().getSpecification() != null && data.getMasterDataInDetailResponse().getSpecification().size() != 0) {
            subjectDetail.setText(data.getMasterDataInDetailResponse().getSpecification().get(0).getName());
        }
        if (data.getMasterDataInDetailResponse().getLevel() != null && data.getMasterDataInDetailResponse().getLevel().size() != 0) {
            beneDetail.setText(data.getMasterDataInDetailResponse().getLevel().get(0).getName());
        }
        if (data.getDetails() != null && data.getDetails().size() != 0) {
            regulationContent.setText(data.getDetails().get(0).getContent());
        }
        if (data.getMasterDataInDetailResponse().getLegalNature() != null && data.getMasterDataInDetailResponse().getLegalNature().size() != 0) {
            legalDetail.setText(data.getMasterDataInDetailResponse().getLegalNature().get(0).getName());
            if (artDetail.getText().toString().equals("")) {
                artDetail.setText(data.getMasterDataInDetailResponse().getLegalNature().get(0).getName());
            }
        }
        if (data.getMasterDataInDetailResponse().getAuthority() != null && data.getMasterDataInDetailResponse().getAuthority().size() != 0) {
            authorityDetail.setText(data.getMasterDataInDetailResponse().getAuthority().get(0).getName());
        }
        if (data.getRegulationDate() != null) {
            standDetail.setText(data.getRegulationDate());
        }

        if (data.getHistories().isEmpty()) {
            historyEmpty.setVisibility(View.VISIBLE);
        } else {
            historyEmpty.setVisibility(View.GONE);
            AdapterRegulationHistory adapter = new AdapterRegulationHistory(data.getHistories(),  RegulationDetailActivity.this);
            LinearLayoutManager layoutManager = new LinearLayoutManager(RegulationDetailActivity.this);
            listHistory.setLayoutManager(layoutManager);
            listHistory.setAdapter(adapter);
        }
    }

    private void getDetail(int newsId) {
        if (!isOnline()) {
            return;
        }
        Utils.showProgressDialog(progressDialog);
        APIService service = APIUtils.getAPIService();
        final String accessToken = Utils.getAccessToken();
        service.getRegulationDetail("Bearer " + accessToken, String.valueOf(newsId))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<SingleRegulationResponse>() {
                    @Override
                    public void onNext(SingleRegulationResponse singleRegulationResponse) {
                        data = singleRegulationResponse.getData();
                        setData(data);
                        Utils.hideProgressDialog(progressDialog);
                    }

                    @Override
                    public void onError(Throwable e) {
                        Utils.hideProgressDialog(progressDialog);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void downloadFileAttach(final String url, final String name) {
        Utils.showProgressDialog(progressDialog);
        if (isStoragePermissionGrand()) {
            APIService service = APIUtils.getAPIService();
            service.getImage(url)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new DisposableObserver<ResponseBody>() {
                        @Override
                        public void onNext(ResponseBody responseBody) {
                            boolean fileDownloaded = downloadImageSuccess(responseBody, name);
                            if (fileDownloaded) {
//                                goToShareAttach(name);
                                goToShareAcitivity(url, name);
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            Utils.hideProgressDialog(progressDialog);
                        }

                        @Override
                        public void onComplete() {

                        }
                    });
        } else {
            Utils.hideProgressDialog(progressDialog);
        }
    }

    public void goToShareAcitivity(String url, String name) {
//        Intent intent = new Intent(this, RefactorShareAttachmentActivity.class);
//        intent.putExtra("SEND_URL", url);
//        intent.putExtra("SEND_NAME", name);
//        startActivity(intent);
        Utils.hideProgressDialog(progressDialog);
//        Intent i = new Intent(Intent.ACTION_VIEW);
//        i.setData(Uri.parse(url));
//        startActivity(i);
        Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.setPackage("com.android.chrome");
        try {
            startActivity(i);
        } catch (ActivityNotFoundException e) {
            // Chrome is probably not installed
            // Try with the default browser
            i.setPackage(null);
            startActivity(i);
        }
    }

    private void setTextFromHtml(String content, TextView textView) {
        Spannable sequence;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            sequence = (Spannable) Html.fromHtml(content, Html.FROM_HTML_MODE_LEGACY, null, null);
        } else {
            sequence = (Spannable) Html.fromHtml(content, null, null);
        }
        SpannableStringBuilder strBuilder = new SpannableStringBuilder(sequence);
        textView.setText(strBuilder);
    }

    // check permission write storage
    public boolean isStoragePermissionGrand() {
        if (Build.VERSION.SDK_INT > 23) {
            if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) ==
                    PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        } else {
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @android.support.annotation.NonNull String[] permissions, @android.support.annotation.NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private boolean downloadImageSuccess(ResponseBody body, String name) {
        try {
            InputStream in = null;
            FileOutputStream out = null;

            File file = new File(getExternalFilesDir(null) + File.separator + name);

            try {
                byte[] fileReader = new byte[4096];

                in = body.byteStream();
                out = new FileOutputStream(file);

                while (true) {
                    int read = in.read(fileReader);

                    if (read == -1) {
                        break;
                    }
                    out.write(fileReader, 0, read);
                }

                out.flush();

                return true;
            } catch (IOException e) {
                return false;
            } finally {
                if (in != null) {
                    in.close();
                }
                if (out != null) {
                    out.close();
                }
            }
        } catch (IOException e) {
            return false;
        }
    }
}
