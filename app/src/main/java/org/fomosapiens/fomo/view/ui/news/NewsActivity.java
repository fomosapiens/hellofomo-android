package org.fomosapiens.fomo.view.ui.news;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import org.fomosapiens.fomo.R;
import org.fomosapiens.fomo.Utils;
import org.fomosapiens.fomo.model.DashBoard;
import org.fomosapiens.fomo.model.News;
import org.fomosapiens.fomo.net.APIHelper;
import org.fomosapiens.fomo.net.APIService;
import org.fomosapiens.fomo.net.APIUtils;
import org.fomosapiens.fomo.view.adapter.AdapterDasBoardItem;
import org.fomosapiens.fomo.view.interfacelistener.OnClickNewsDetailListener;
import org.fomosapiens.fomo.view.interfacelistener.OnResponseGetDashBoardListener;
import org.fomosapiens.fomo.view.interfacelistener.OnResponseGetListNewsListener;
import org.fomosapiens.fomo.view.interfacelistener.OnSetDataFinishListener;
import org.fomosapiens.fomo.view.ui.PrimaryBaseActivity;
import org.fomosapiens.fomo.view.ui.newsdetail.NewsDetailActivity;

import java.util.ArrayList;
import java.util.List;
import butterknife.ButterKnife;

/**
 * Created by PC162 on 7/7/2017.
 */

public class NewsActivity extends PrimaryBaseActivity implements OnClickNewsDetailListener, SwipeRefreshLayout.OnRefreshListener, OnSetDataFinishListener {

    public static String TAG = "org.fomosapiens.fomo.view.ui.news.NewsActivity";

    protected SwipeRefreshLayout anSwipeContainer;
    protected RecyclerView anRecyclerView;
    protected View rootView;

    private List<DashBoard> dashBoards;
    private int requestCount;
    private int size = 0;
    private Dialog progressDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        rootView = getLayoutInflater().inflate(R.layout.activity_news, getContainerView());
        progressDialog = new Dialog(this);
        setTitleToolbar();
        initLayout();
        getListNewsHolder();
    }

    @Override
    protected String getActivityName() {
        return TAG;
    }

    private void setTitleToolbar() {
        abTxtToolBarTitle.setText(getResources().getString(R.string.news));
    }


    private void initLayout() {
        anRecyclerView = ButterKnife.findById(rootView, R.id.anRecyclerView);
        anSwipeContainer = ButterKnife.findById(rootView, R.id.anSwipeContainer);
        anSwipeContainer.setOnRefreshListener(this);
    }

    private void setAdapterForRecycler(List<DashBoard> dashBoards){
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        AdapterDasBoardItem adapterDasBoardItem = new AdapterDasBoardItem(dashBoards, this, this, this);
        anRecyclerView.setLayoutManager(mLayoutManager);
        anRecyclerView.setAdapter(adapterDasBoardItem);
    }

    private void showDataNull() {
        Toast.makeText(this, "Data null", Toast.LENGTH_SHORT).show();
    }

    private void dismissProgressAndRefresh(){
        if (anSwipeContainer.isRefreshing())
            anSwipeContainer.setRefreshing(false);
        Utils.hideProgressDialog(progressDialog);
    }

    private void goToNewsDetailActivity(News news) {
        Intent intent = new Intent(this, NewsDetailActivity.class);
        intent.putExtra(Utils.KEY_NEWS_ID, news.getId());
        startActivity(intent);
    }

    // TODO: OnClickNewsDetailListener
    @Override
    public void onClickItem(News news) {
        if (isOnline())
            goToNewsDetailActivity(news);
    }

    // TODO: OnRefreshListener
    @Override
    public void onRefresh() {
        if (!isOnline())
            return;

        getListNewsHolder();
    }

    // TODO: OnSetDataFinishListener
    @Override
    public void onLoadFinish() {
        dismissProgressAndRefresh();
    }

    private void getListNewsHolder() {
        if (!isOnline())
            return;
        if (!anSwipeContainer.isRefreshing())
            Utils.showProgressDialog(progressDialog);
        requestCount = 0;
        APIHelper.getListNewsHolder(getDashBoardListener());
    }

    private OnResponseGetDashBoardListener getDashBoardListener() {
        OnResponseGetDashBoardListener dashBoardListener = new OnResponseGetDashBoardListener() {
            @Override
            public void OnRequestSuccess(List<DashBoard> mDashBoards) {
                if (mDashBoards==null || mDashBoards.size() == 0) {
                    dismissProgressAndRefresh();
                    return;
                }

                size = mDashBoards.size();
                dashBoards = mDashBoards;
                int position = 0;
                for (DashBoard dashBoard : mDashBoards){
                    getListNewsWithCategoryOrTag(dashBoard, position);
                    position++;
                }
            }

            @Override
            public void OnRequestError(Throwable e) {
                dismissProgressAndRefresh();
            }
        };
        return dashBoardListener;
    }

    private void getListNewsWithCategoryOrTag(DashBoard dashBoard, int position) {
        APIHelper.getListNewsWithCategoryOrTag(dashBoard, getOnResponseListNews(position));
    }

    private OnResponseGetListNewsListener getOnResponseListNews(final int position) {
        OnResponseGetListNewsListener responseGetListNewsListener = new OnResponseGetListNewsListener() {
            @Override
            public void OnRequestSuccess(final List<News> newsList, final DashBoard dashBoard) {
                requestCount++;
                dashBoards.get(position).setNewsList(newsList);

                if (requestCount == size)
                    setAdapterForRecycler(dashBoards);
            }

            @Override
            public void OnRequestError(Throwable e) {
                dismissProgressAndRefresh();
            }
        };
        return responseGetListNewsListener;
    }
}
