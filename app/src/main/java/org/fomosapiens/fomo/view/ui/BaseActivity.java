package org.fomosapiens.fomo.view.ui;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.fomosapiens.fomo.R;
import org.fomosapiens.fomo.SharePre;
import org.fomosapiens.fomo.Utils;
import org.fomosapiens.fomo.model.Category;
import org.fomosapiens.fomo.model.response.ResponseCategory;
import org.fomosapiens.fomo.net.APIUtils;
import org.fomosapiens.fomo.view.interfacelistener.OnPostMenuPositionEvent;
import org.fomosapiens.fomo.view.ui.dashboard.DashBoardActivity;
import org.fomosapiens.fomo.view.dialog.LoadingDialog;
import org.fomosapiens.fomo.view.interfacelistener.OnClickMenuItemListener;
import org.fomosapiens.fomo.view.ui.news.NewsActivity;
import org.fomosapiens.fomo.view.ui.newsartical.NewsArticleActivity;
import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by PC162 on 7/7/2017.
 */

public class BaseActivity extends AppCompatActivity {

    protected String getActivityName() {
        return "";
    }

    public boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(this.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo == null || !netInfo.isConnected()) {
//            Toast.makeText(this, "Es liegt ein Problem mit der Internetverbindung oder dem Server vor. Bitte versuchen Sie es später noch einmal.", Toast.LENGTH_SHORT).show();
            final AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Fehler! ");
            builder.setMessage("Es liegt ein Problem mit der Internetverbindung oder dem Server vor. Bitte versuchen Sie es später noch einmal.");
            builder.setCancelable(false);
            builder.setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });
            builder.show();
        }
        return netInfo != null && netInfo.isConnected();
    }


}
