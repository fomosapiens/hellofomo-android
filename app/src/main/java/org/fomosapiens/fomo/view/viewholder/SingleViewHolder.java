package org.fomosapiens.fomo.view.viewholder;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.fomosapiens.fomo.R;
import org.fomosapiens.fomo.Utils;
import org.fomosapiens.fomo.model.DashBoard;
import org.fomosapiens.fomo.model.News;
import org.fomosapiens.fomo.view.customview.FontChange;
import org.fomosapiens.fomo.view.customview.ViewSingleItem;
import org.fomosapiens.fomo.view.interfacelistener.OnClickNewsDetailListener;
import org.fomosapiens.fomo.view.interfacelistener.OnSetDataFinishListener;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by PC162 on 9/6/2017.
 */

public class SingleViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.adtsgViewLineTop)
    protected View adtsgViewLineTop;

    @BindView(R.id.adtsgTxtHeader)
    protected TextView adtsgTxtHeader;

    @BindView(R.id.adtsgLinearContainer)
    protected LinearLayout adtsgLinearContainer;

    @BindView(R.id.adtsgView)
    protected LinearLayout adtsgView;

    private Context context;
    private OnSetDataFinishListener onSetDataFinishListener;
    private OnClickNewsDetailListener onClickNewsDetailListener;

    public SingleViewHolder(View itemView, Context context, OnClickNewsDetailListener onClickNewsDetailListener, OnSetDataFinishListener onSetDataFinishListener) {
        super(itemView);
        this.context = context;
        this.onSetDataFinishListener = onSetDataFinishListener;
        this.onClickNewsDetailListener = onClickNewsDetailListener;
        ButterKnife.bind(this, itemView);
        Utils.setFontName(context, (ViewGroup)itemView);
    }

    public void setDashboard(List<DashBoard> dashboards, int position){
        if (position == dashboards.size() - 1)
            adtsgViewLineTop.setVisibility(View.GONE);
        else
            adtsgViewLineTop.setVisibility(View.VISIBLE);

        createSingleItemPart(dashboards.get(position));

    }

    private void createSingleItemPart(DashBoard dashBoard) {

        List<News> newsList = dashBoard.getNewsList();
        if (newsList==null || newsList.size()==0) {
            adtsgView.setVisibility(View.GONE);
            adtsgView.setLayoutParams(new RecyclerView.LayoutParams(0, 0));
            onSetDataFinishListener.onLoadFinish();
            return;
        } else {
            boolean isShowPicture = Utils.getIsShowPicture(dashBoard.getShow_picture());
            String sectionDescription = dashBoard.getSection_description();
            adtsgTxtHeader.setText(sectionDescription);
            adtsgViewLineTop.setVisibility(View.GONE);
            adtsgLinearContainer.setVisibility(View.VISIBLE);
            adtsgLinearContainer.removeAllViews();
            for (News news : newsList) {
                ViewSingleItem singleItem = new ViewSingleItem(context, news, onClickNewsDetailListener, isShowPicture);
                adtsgLinearContainer.addView(singleItem);
            }
        }
        onSetDataFinishListener.onLoadFinish();
    }
}
