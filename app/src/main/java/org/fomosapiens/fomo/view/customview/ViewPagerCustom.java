package org.fomosapiens.fomo.view.customview;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import org.fomosapiens.fomo.R;
import org.fomosapiens.fomo.Utils;

/**
 * Created by PC162 on 8/10/2017.
 */

public class ViewPagerCustom extends ViewPager {

    public ViewPagerCustom(Context context) {
        super(context);
        initLayout();
    }

    public ViewPagerCustom(Context context, AttributeSet attrs) {
        super(context, attrs);
    }
    private void initLayout(){
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.view_pager_custom, this, true);
    }


}
