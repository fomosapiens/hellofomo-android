package org.fomosapiens.fomo.view.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import org.fomosapiens.fomo.R;
import org.fomosapiens.fomo.Utils;
import org.fomosapiens.fomo.model.Author;
import org.fomosapiens.fomo.view.customview.FontChange;

import java.util.List;

import javax.microedition.khronos.opengles.GL;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by HuyMTB on 7/18/18.
 *
 */

public class AdapterNewsAuthors extends RecyclerView.Adapter<AdapterNewsAuthors.ViewHolder>{

    private List<Author> listAuthor;
    private Activity activity;

    public AdapterNewsAuthors(List<Author> listAuthor, Activity activity) {
        this.listAuthor = listAuthor;
        this.activity = activity;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.refactor_item_author, parent, false);
        Utils.setFontName(parent.getContext(), (ViewGroup)view);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Author author = listAuthor.get(position);

        holder.authorName.setText(author.getName());

        if (!author.getPrimaryImage().equals("")) {
            Glide.with(activity).load(author.getPrimaryImage()).into(holder.authorImg).onLoadFailed(ContextCompat.getDrawable(activity, R.drawable.ic_img_placeholder));
        }

        if (author.getCallbackUrl() != null) {
            holder.layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(author.getCallbackUrl()));
                    activity.startActivity(browserIntent);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return listAuthor.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.refactor_item_author_image)
        ImageView authorImg;
        @BindView(R.id.refactor_item_author_name)
        TextView authorName;
        @BindView(R.id.refactor_author_item)
        RelativeLayout layout;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
