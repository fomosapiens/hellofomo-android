package org.fomosapiens.fomo.view.customview;

import android.content.Context;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;

import org.fomosapiens.fomo.R;
import org.fomosapiens.fomo.model.Category;
import org.fomosapiens.fomo.view.interfacelistener.OnClickMenuItemListener;

/**
 * Created by PC162 on 8/15/2017.
 */

public class CustomTextView extends TextView implements View.OnClickListener {

    private OnClickMenuItemListener clickMenuItem;
    private Category category;

    public CustomTextView(Context context, Category category, OnClickMenuItemListener clickMenuItem) {
        super(context);
        this.category = category;
        this.clickMenuItem = clickMenuItem;
        setText(category.getName());
        setId(category.getId());
        setSingleLine(true);
        setPadding(0, (int) context.getResources().getDimension(R.dimen.activity_dashboard_margin_10),0,0);
        setEllipsize(TextUtils.TruncateAt.MIDDLE);
        setOnClickListener(this);
    }

    public CustomTextView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomTextView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

    }

    @Override
    public void onClick(View view) {
        clickMenuItem.onClickMenuItem(category);
    }
}
