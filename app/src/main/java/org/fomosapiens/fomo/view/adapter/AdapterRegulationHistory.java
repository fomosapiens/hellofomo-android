package org.fomosapiens.fomo.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.fomosapiens.fomo.R;
import org.fomosapiens.fomo.Utils;
import org.fomosapiens.fomo.model.regulation.RegulationListHistory;
import org.fomosapiens.fomo.view.customview.FontChange;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AdapterRegulationHistory extends RecyclerView.Adapter<AdapterRegulationHistory.ViewHolder> {

    private List<RegulationListHistory> listResponses;
    private Context context;

    public AdapterRegulationHistory(List<RegulationListHistory> listResponses, Context context) {
        this.context = context;
        this.listResponses = listResponses;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.refactor_item_regulation_history, parent, false);
        Utils.setFontName(context, (ViewGroup)view);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        RegulationListHistory data = listResponses.get(position);
        holder.date.setText(data.getDate());

        Spannable sequence;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            sequence = (Spannable) Html.fromHtml(data.getDescription(), Html.FROM_HTML_MODE_LEGACY, null, null);
        } else {
            sequence = (Spannable) Html.fromHtml(data.getDescription(), null, null);
        }
        SpannableStringBuilder strBuilder = new SpannableStringBuilder(sequence);

        holder.content.setText(strBuilder);
    }

    @Override
    public int getItemCount() {
        return listResponses.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.item_regulation_history_date)
        TextView date;
        @BindView(R.id.item_regulation_history_content)
        TextView content;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
