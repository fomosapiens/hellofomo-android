package org.fomosapiens.fomo.view.ui;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ImageButton;
import android.widget.TextView;

import org.fomosapiens.fomo.R;
import org.fomosapiens.fomo.SharePre;
import org.fomosapiens.fomo.Utils;
import org.fomosapiens.fomo.model.Kontakt;
import org.fomosapiens.fomo.net.APIService;
import org.fomosapiens.fomo.net.APIUtils;
import org.fomosapiens.fomo.view.customview.FontChange;
import org.fomosapiens.fomo.view.customview.PicassoImageGetter;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;


/**
 * Created by PC162 on 8/31/2017.
 */

public class ImpressumActivity extends BaseActivity implements View.OnClickListener{

    public static String TAG = "org.fomosapiens.fomo.view.ui.ImpressumActivity";

    @BindView(R.id.aipsTxtContent)
    protected TextView aipsTxtContent;

    @BindView(R.id.aipsImgBtnClose)
    protected ImageButton aipsImgBtnClose;

    private Dialog progressDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_impressum);
        ButterKnife.bind(this);
        progressDialog = new Dialog(this);

        // change font
        Utils.setFontName(this, (ViewGroup)this.findViewById(android.R.id.content));
        initLayout();
        getImpressum();
    }

    private void initLayout(){
        aipsImgBtnClose.setOnClickListener(this);
    }

    private void setTextForImpressum(String data){
        PicassoImageGetter imageGetter = new PicassoImageGetter(aipsTxtContent, getBaseContext());
        if (data== null || data.equals("") || data.equals(" ")) {
            Utils.hideProgressDialog(progressDialog);
            return;
        }
        CharSequence sequence;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            sequence = Html.fromHtml(data, Html.FROM_HTML_MODE_LEGACY, imageGetter, null);
        } else {
            sequence = Html.fromHtml(data, imageGetter, null);
        }
        aipsTxtContent.setText(sequence);
        Utils.hideProgressDialog(progressDialog);
    }

    private void getImpressum(){
        if (!isOnline())
            return;

        Utils.showProgressDialog(progressDialog);
        APIService service = APIUtils.getAPIService();
        String accessToken = Utils.getAccessToken();

        service.getImpressum("Bearer " + accessToken)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Kontakt>() {
                    @Override
                    public void onNext(@NonNull Kontakt kontakt) {
                        setTextForImpressum(kontakt.getData());
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    @Override
    public void onClick(View view) {
        finish();
    }
}
