package org.fomosapiens.fomo.view.ui.settings;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;

import org.fomosapiens.fomo.BuildConfig;
import org.fomosapiens.fomo.R;
import org.fomosapiens.fomo.SharePre;
import org.fomosapiens.fomo.Utils;
import org.fomosapiens.fomo.model.Login;
import org.fomosapiens.fomo.net.APIService;
import org.fomosapiens.fomo.net.APIUtils;
import org.fomosapiens.fomo.net.entity.LoginEntity;
import org.fomosapiens.fomo.view.customview.FontChange;
import org.fomosapiens.fomo.view.ui.RefactorPrimaryBaseActivity;

import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;


/**
 * A simple {@link Fragment} subclass.
 */
public class SettingsFragment extends Fragment {

    @BindView(R.id.refactor_settings_push_notification_check)
    CheckBox notificationCheck;

    private Timer timer;
    private TimerTask timerTask;

    public SettingsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Utils.setFontName(getContext(), (ViewGroup)this.getView());
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.refactor_fragment_settings, container, false);
        ButterKnife.bind(this, view);

        boolean noti = SharePre.getBoolean(SharePre.KEY_NOTIFICATION);
        if (noti) {
            notificationCheck.setChecked(true);
        } else {
            notificationCheck.setChecked(false);
        }

        notificationCheck.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    SharePre.saveBoolean(SharePre.KEY_NOTIFICATION, true);
                    FirebaseMessaging.getInstance().subscribeToTopic(BuildConfig.FIREBASE_TOPIC);
                } else {
                    SharePre.saveBoolean(SharePre.KEY_NOTIFICATION, false);
                    FirebaseMessaging.getInstance().unsubscribeFromTopic(BuildConfig.FIREBASE_TOPIC);
                }
            }
        });

        return view;
    }



}
