package org.fomosapiens.fomo.view.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.fomosapiens.fomo.R;
import org.fomosapiens.fomo.Utils;
import org.fomosapiens.fomo.model.News;
import org.fomosapiens.fomo.view.customview.FontChange;
import org.fomosapiens.fomo.view.fragment.FragmentDashBoardItem;
import org.fomosapiens.fomo.view.interfacelistener.OnClickNewsDetailListener;
import org.fomosapiens.fomo.view.ui.ViewUtil;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by PC162 on 7/6/2017.
 */

public class AdapterPagerDashBoard extends PagerAdapter {

    @BindView(R.id.fgdLinearContainer)
    protected RelativeLayout fgdLinearContainer;

    @BindView(R.id.fgdTxtTitle)
    protected TextView fgdTxtTitle;

    @BindView(R.id.fgdImgImage)
    protected ImageView fgdImgImage;

    @BindView(R.id.fgdTxtDate)
    protected TextView fgdTxtDate;

    private Context context;
    private List<News> newsList;
    private boolean isShowPicture;
    private OnClickNewsDetailListener onClickNewsDetailListener;

    public AdapterPagerDashBoard(Context context, List<News> newsList, OnClickNewsDetailListener onClickNewsDetailListener, boolean isShowPicture) {
        this.onClickNewsDetailListener = onClickNewsDetailListener;
        this.isShowPicture = isShowPicture;
        this.newsList = newsList;
        this.context = context;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.fragment_dash_board_item, container, false);
        ButterKnife.bind(this, itemView);
        setData(newsList.get(position));
        container.addView(itemView);
        Utils.setFontName(context, (ViewGroup)itemView);
        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return newsList == null ? 0 : newsList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    private void setData(News news) {
        if (news == null)
            return;
        fgdTxtTitle.setText(news.getTitle());
        fgdTxtDate.setText(Utils.getDate(news.getAvailable_date()));
        String imageUrl = news.getPrimary_image();

        if (isShowPicture) {
            ViewUtil.setImageForImageView(context, imageUrl, fgdImgImage);
        } else
            fgdImgImage.setVisibility(View.GONE);

        setOnClickForLinearContainer(news);
    }


    private void setOnClickForLinearContainer(final News mNews){
        fgdLinearContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickNewsDetailListener.onClickItem(mNews);
            }
        });
    }
}
