package org.fomosapiens.fomo.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.fomosapiens.fomo.R;
import org.fomosapiens.fomo.model.DashBoard;

import org.fomosapiens.fomo.model.News;
import org.fomosapiens.fomo.model.type.Layout;
import org.fomosapiens.fomo.view.customview.FontChange;
import org.fomosapiens.fomo.view.interfacelistener.OnClickNewsDetailListener;
import org.fomosapiens.fomo.view.interfacelistener.OnSetDataFinishListener;
import org.fomosapiens.fomo.view.viewholder.ListViewHolder;
import org.fomosapiens.fomo.view.viewholder.SingleViewHolder;
import org.fomosapiens.fomo.view.viewholder.SliderViewHolder;

import java.util.List;

/**
 * Created by PC162 on 9/6/2017.
 */

public class AdapterDasBoardItem extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int LAYOUT_SLIDER = 0;
    private final int LAYOUT_LIST = 1;
    private final int LAYOUT_SINGLE = 2;

    private Context context;
    private List<DashBoard> dashBoards;
    private OnSetDataFinishListener onSetDataFinishListener;
    private OnClickNewsDetailListener onClickNewsDetailListener;
    private List<News> newsList;

    public AdapterDasBoardItem(List<DashBoard> dashBoards, Context context, OnClickNewsDetailListener onClickNewsDetailListener, OnSetDataFinishListener onSetDataFinishListener) {
        this.context = context;
        this.dashBoards = dashBoards;
        this.onSetDataFinishListener = onSetDataFinishListener;
        this.onClickNewsDetailListener = onClickNewsDetailListener;
        this.newsList = newsList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case LAYOUT_SLIDER:
                View sliderView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_dashboard_slider_item, parent, false);
                return new SliderViewHolder(sliderView, context, onClickNewsDetailListener, onSetDataFinishListener);

            case LAYOUT_SINGLE:
                View singleView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_dashboard_single_item, parent, false);
                return new SingleViewHolder(singleView, context, onClickNewsDetailListener, onSetDataFinishListener);
            default:
                View listView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_dashboard_list_item, parent, false);
                return new ListViewHolder(listView, context, onClickNewsDetailListener, onSetDataFinishListener);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
//        DashBoard dashBoard = dashBoards.get(position);
        if (holder instanceof SliderViewHolder) {
            ((SliderViewHolder)holder).setDashboard(dashBoards, position);
        }
        else if (holder instanceof SingleViewHolder){
            ((SingleViewHolder)holder).setDashboard(dashBoards, position);
        }
        else if (holder instanceof ListViewHolder){
            ((ListViewHolder)holder).setDashboard(dashBoards, position);
        }
    }

    @Override
    public int getItemCount() {


        return dashBoards == null ? 0 : dashBoards.size();
    }

    @Override
    public int getItemViewType(int position) {
        DashBoard dashBoard = dashBoards.get(position);
        Layout mLayout = Layout.getLayout(dashBoard.getLayout());
        List<News> list = dashBoard.getNewsList();
        switch (mLayout){
            case SLIDER:
                if (list != null) {
                    return LAYOUT_SLIDER;
                }
            case SINGLE:
                if (list != null) {
                    return LAYOUT_SINGLE;
                }
            default:
                return LAYOUT_LIST;
        }
    }
}
