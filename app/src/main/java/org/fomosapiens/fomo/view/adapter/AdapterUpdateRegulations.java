package org.fomosapiens.fomo.view.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;

import org.fomosapiens.fomo.R;
import org.fomosapiens.fomo.Utils;
import org.fomosapiens.fomo.model.event.HistoryData;
import org.fomosapiens.fomo.view.customview.FontChange;
import org.fomosapiens.fomo.view.ui.regulation.RegulationDetailActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AdapterUpdateRegulations extends RecyclerView.Adapter<AdapterUpdateRegulations.ViewHolder> {

    private Context context;
    private List<HistoryData> listResponses;
    private SparseBooleanArray itemStateArray= new SparseBooleanArray();

    public AdapterUpdateRegulations(List<HistoryData> listResponses, Context context) {
        this.context = context;
        this.listResponses = listResponses;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.refactor_item_event, parent, false);
        Utils.setFontName(parent.getContext(), (ViewGroup)view);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final HistoryData data = listResponses.get(position);

        holder.topDate.setText(context.getString(R.string.change_from) + " " + data.getDate());
        setTextFromHtml(data.getRegulation().getDetails().get(0).getDescription(), holder.topTitle);
        setTextFromHtml(data.getDescription(), holder.contentTv);
//        holder.bind(position);
        holder.itemClickArea.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (holder.expandBtn.getText().toString().equals("+")) {
                    holder.contentLayout.setVisibility(View.VISIBLE);
                    holder.expandBtn.setText("-");
                } else if (holder.expandBtn.getText().toString().equals("-")) {
                    holder.contentLayout.setVisibility(View.GONE);
                    holder.expandBtn.setText("+");
                }
            }
        });
        holder.contentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Gson gson = new Gson();
                Intent intent = new Intent(context, RegulationDetailActivity.class);
                intent.putExtra("regulation_id", data.getRegulation().getId());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return listResponses.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.item_event_content_layout)
        ConstraintLayout contentLayout;
        @BindView(R.id.item_event_top_layout)
        ConstraintLayout itemClickArea;
        @BindView(R.id.item_event_expand)
        TextView expandBtn;
        @BindView(R.id.item_event_content_tv)
        TextView contentTv;
        @BindView(R.id.item_event_top_title)
        TextView topTitle;
        @BindView(R.id.item_event_top_date)
        TextView topDate;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
//            itemView.setOnClickListener(this);
            this.setIsRecyclable(false);
        }

        void bind(int position) {
            if (!itemStateArray.get(position, false)) {
                contentLayout.setVisibility(View.GONE);
                expandBtn.setText("+");
            } else {
                contentLayout.setVisibility(View.VISIBLE);
                expandBtn.setText("-");
            }
        }

        @Override
        public void onClick(View view) {
            int adapterPosition = getAdapterPosition();

            switch (view.getId()) {
                case R.id.item_event_top_layout:
                    if (!itemStateArray.get(adapterPosition, false)) {
                        contentLayout.setVisibility(View.GONE);
                        expandBtn.setText("+");
                        itemStateArray.put(adapterPosition, true);
                    }
                    else  {
                        contentLayout.setVisibility(View.VISIBLE);
                        expandBtn.setText("-");
                        itemStateArray.put(adapterPosition, false);
                    }
                    break;

                case R.id.item_event_content_layout:
                    break;
            }

        }
    }

    private void setTextFromHtml(String content, TextView textView) {
        Spannable sequence;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            sequence = (Spannable) Html.fromHtml(content, Html.FROM_HTML_MODE_LEGACY, null, null);
        } else {
            sequence = (Spannable) Html.fromHtml(content, null, null);
        }
        SpannableStringBuilder strBuilder = new SpannableStringBuilder(sequence);
        textView.setText(strBuilder);
    }

}
