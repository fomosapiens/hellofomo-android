package org.fomosapiens.fomo.view.ui.service;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.URLSpan;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;

import org.fomosapiens.fomo.R;
import org.fomosapiens.fomo.Utils;
import org.fomosapiens.fomo.model.service.ServiceData;
import org.fomosapiens.fomo.view.customview.PicassoImageGetter;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ServiceDetailActivity extends AppCompatActivity {

    @BindView(R.id.serviceToolbarTitle)
    TextView toolbarTitle;
    @BindView(R.id.serviceDetailBack)
    ImageView backBtn;
    @BindView(R.id.serviceImg)
    ImageView imageView;
    @BindView(R.id.serviceCopyRight)
    TextView copyRight;
    @BindView(R.id.serviceLogoImg)
    ImageView logoImg;
    @BindView(R.id.serviceTitle)
    TextView title;
    @BindView(R.id.serviceDesTitle)
    TextView desTitle;
    @BindView(R.id.serviceContentTitle)
    TextView contentTitle;
    @BindView(R.id.serviceDesContent)
    TextView desContent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_detail);
        ButterKnife.bind(this);
        Utils.setFontName(this, (ViewGroup)this.findViewById(android.R.id.content));

        // back button
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        // get data
        Intent intent = getIntent();
        int filterOption = intent.getIntExtra(Utils.KEY_FILTER_OPTION, 0);
        Gson gson = new Gson();
        ServiceData data = gson.fromJson(intent.getStringExtra(Utils.KEY_SERVICE_DETAIL), ServiceData.class);

        // general data
        Glide.with(this).load(data.getLogo()).into(logoImg);

        // specific data
        if (filterOption == 0) {
            toolbarTitle.setText(getString(R.string.company).toUpperCase());
            Glide.with(this).load(data.getDirectory().getTitleImage()).into(imageView);
            title.setText(data.getName());
            if (data.getDirectory() != null) {
                copyRight.setText(data.getDirectory().getCopyright());
            }
            Spannable sequence;
            PicassoImageGetter imageGetter = new PicassoImageGetter(desContent, getBaseContext());
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                if (data.getDirectory().getDescription() != null) {
                    sequence = (Spannable) Html.fromHtml(data.getDirectory().getDescription(), Html.FROM_HTML_MODE_LEGACY, imageGetter, null);
                    setTextHTML(sequence, desContent);
                }
            } else {
                if (data.getDirectory().getDescription() != null) {
                    sequence = (Spannable) Html.fromHtml(data.getDirectory().getDescription(), imageGetter, null);
                    setTextHTML(sequence, desContent);
                }
            }

        } else if (filterOption == 1) {
            toolbarTitle.setText(getString(R.string.marketplace).toUpperCase());
            Glide.with(this).load(data.getMarketplace().getTitleImage()).into(imageView);
            title.setText(data.getName());
            if (data.getMarketplace() != null) {
                copyRight.setText(data.getMarketplace().getCopyright());
            }
            desTitle.setVisibility(View.VISIBLE);
            contentTitle.setVisibility(View.VISIBLE);
            contentTitle.setText(data.getMarketplace().getProduct());
            Spannable sequence;
            PicassoImageGetter imageGetter = new PicassoImageGetter(desContent, getBaseContext());
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                if (data.getMarketplace().getDescription() != null) {
                    sequence = (Spannable) Html.fromHtml(data.getMarketplace().getDescription(), Html.FROM_HTML_MODE_LEGACY, imageGetter, null);
                    setTextHTML(sequence, desContent);
                }
            } else {
                if (data.getMarketplace().getDescription() != null) {
                    sequence = (Spannable) Html.fromHtml(data.getMarketplace().getDescription(), imageGetter, null);
                    setTextHTML(sequence, desContent);
                }
            }
        }
    }

    private void setTextHTML(Spannable sequence, TextView textView) {
        SpannableStringBuilder strBuilder = new SpannableStringBuilder(sequence);
        URLSpan[] urls = strBuilder.getSpans(0, sequence.length(), URLSpan.class);
        for (URLSpan span : urls) {
            makeLinkClickable(strBuilder, span);
        }
        textView.setText(strBuilder);
        textView.setMovementMethod(LinkMovementMethod.getInstance());
    }

    protected void makeLinkClickable(SpannableStringBuilder strBuilder, final URLSpan span) {
        int start = strBuilder.getSpanStart(span);
        int end = strBuilder.getSpanEnd(span);
        int flags = strBuilder.getSpanFlags(span);
        ClickableSpan clickable = new ClickableSpan() {
            public void onClick(View view) {
                startNewsOtherDetailFromThisDetail(span.getURL());
            }
        };
        strBuilder.setSpan(clickable, start, end, flags);
        strBuilder.removeSpan(span);
    }

    private void startNewsOtherDetailFromThisDetail(String url) {
        if (url==null || url.equals(""))
            return;
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        startActivity(browserIntent);
    }
}
