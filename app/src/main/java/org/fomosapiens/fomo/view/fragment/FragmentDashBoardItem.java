package org.fomosapiens.fomo.view.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import org.fomosapiens.fomo.R;
import org.fomosapiens.fomo.Utils;
import org.fomosapiens.fomo.model.News;
import org.fomosapiens.fomo.view.interfacelistener.OnClickNewsDetailListener;
import org.fomosapiens.fomo.view.ui.ViewUtil;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by PC162 on 7/6/2017.
 */

public class FragmentDashBoardItem extends Fragment implements View.OnClickListener {


    @BindView(R.id.fgdTxtTitle)
    protected TextView fgdTxtTitle;

    @BindView(R.id.fgdImgImage)
    protected ImageView fgdImgImage;

    @BindView(R.id.fgdTxtDate)
    protected TextView fgdTxtDate;

    private OnClickNewsDetailListener onClickNewsDetailListener;
    private boolean isShowPicture;
    private Context context;
    private News news;



    public static FragmentDashBoardItem newInstant(Context context, News news, OnClickNewsDetailListener onClickNewsDetailListener, boolean isShowPicture){
        FragmentDashBoardItem boardItem = new FragmentDashBoardItem();
        boardItem.onClickNewsDetailListener = onClickNewsDetailListener;
        boardItem.isShowPicture = isShowPicture;
        boardItem.context = context;
        boardItem.news = news;
        return boardItem;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_dash_board_item, container, false);
        ButterKnife.bind(this, rootView);
        rootView.setOnClickListener(this);
        setData();
        return rootView;
    }

    private void setData() {
        if (news == null)
            return;
        fgdTxtTitle.setText(news.getTitle());
        fgdTxtDate.setText(Utils.getDate(news.getAvailable_date()));
        String imageUrl = news.getPrimary_image();
        if (imageUrl == null || imageUrl.equals("") || imageUrl.equals(" ")) {
            fgdImgImage.setVisibility(View.GONE);
            return;
        }
        if (isShowPicture)
            ViewUtil.setImageForImageView(context, imageUrl, fgdImgImage);

        else
            fgdImgImage.setVisibility(View.GONE);
    }

    // TODO: OnClickListener
    @Override
    public void onClick(View view) {
        onClickNewsDetailListener.onClickItem(news);
    }
}
