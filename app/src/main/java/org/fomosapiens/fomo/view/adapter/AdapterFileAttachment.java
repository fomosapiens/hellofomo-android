package org.fomosapiens.fomo.view.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.fomosapiens.fomo.R;
import org.fomosapiens.fomo.Utils;
import org.fomosapiens.fomo.view.customview.FontChange;
import org.fomosapiens.fomo.view.ui.newsdetail.RefactorNewsDetailActivity;

import java.io.File;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by HuyMTB on 7/18/18.
 *
 */

public class AdapterFileAttachment extends RecyclerView.Adapter<AdapterFileAttachment.ViewHolder> {

    private Activity activity;
    private List<String> listFile;

    public AdapterFileAttachment(Activity activity, List<String> listFile) {
        this.activity = activity;
        this.listFile = listFile;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(activity).inflate(R.layout.refactor_item_file_attachment, parent, false);
        Utils.setFontName(parent.getContext(), (ViewGroup)view);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final String url  = listFile.get(position);
        final String title = Utils.getFileName(url);
        holder.title.setText(title);
        if (holder.getAdapterPosition() == listFile.size() -1) {
            holder.view.setBackgroundColor(ContextCompat.getColor(activity, R.color.color_white));
        } else {
            holder.view.setBackgroundColor(ContextCompat.getColor(activity, R.color.color_dark));
        }

        final File file = new File(activity.getExternalFilesDir(null) + File.separator + title);
//        if (file.exists()) {
//            holder.imageView.setImageResource(R.drawable.ic_file_download_2x);
//        } else {
//            holder.imageView.setImageResource(R.drawable.ic_folder_open_2x);
//        }

        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (file.exists()) {
                    ((RefactorNewsDetailActivity)activity).goToShareAcitivity(url, title);
                } else {
                    ((RefactorNewsDetailActivity)activity).downloadFileAttach(url, title);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return listFile.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.refactor_file_view)
        View view;
        @BindView(R.id.refactor_file_image)
        ImageView imageView;
        @BindView(R.id.refactor_file_title)
        TextView title;
        @BindView(R.id.refactor_file_layout)
        RelativeLayout layout;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
