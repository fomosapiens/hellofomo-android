package org.fomosapiens.fomo.view.ui;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.fomosapiens.fomo.R;
import org.fomosapiens.fomo.SharePre;
import org.fomosapiens.fomo.Utils;
import org.fomosapiens.fomo.model.Category;
import org.fomosapiens.fomo.model.DashBoard;
import org.fomosapiens.fomo.model.SubMenuItem;
import org.fomosapiens.fomo.model.response.ResponseCategory;
import org.fomosapiens.fomo.net.APIUtils;
import org.fomosapiens.fomo.view.adapter.AdapterMenuItem;
import org.fomosapiens.fomo.view.interfacelistener.OnClickMenuItemListener;
import org.fomosapiens.fomo.view.interfacelistener.OnPostMenuPositionEvent;
import org.fomosapiens.fomo.view.settings.SettingsActivity;
import org.fomosapiens.fomo.view.ui.dashboard.DashBoardActivity;
import org.fomosapiens.fomo.view.ui.news.NewsActivity;
import org.fomosapiens.fomo.view.ui.newsartical.NewsArticleActivity;
import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by PC162 on 8/29/2017.
 */

public class PrimaryBaseActivity extends BaseActivity implements View.OnClickListener, OnClickMenuItemListener {

    @BindView(R.id.abToolBar)
    protected Toolbar abToolBar;

    @BindView(R.id.abLinearRightDrawerToggle)
    protected LinearLayout abLinearRightDrawerToggle;

    @BindView(R.id.abDrawerRightMenuIconInside)
    protected LinearLayout abDrawerRightMenuIconInside;

    @BindView(R.id.abDrawerRightMenu)
    protected DrawerLayout abDrawerRightMenu;

    @BindView(R.id.abConstraintContainer)
    protected ConstraintLayout abConstraintContainer;

    @BindView(R.id.mnLinearNewsContainer)
    protected LinearLayout mnLinearNewsContainer;

    @BindView(R.id.abTxtToolBarTitle)
    protected TextView abTxtToolBarTitle;

//    @BindView(R.id.refactor_menu_recycle_view)
//    protected RecyclerView menuRecycleView;

    @BindView(R.id.mnTxtDashBoard)
    protected TextView mnTxtDashBoard;

    @BindView(R.id.mnTxtNews)
    protected TextView mnTxtNews;

    @BindView(R.id.mnKontakt)
    protected TextView mnKontakt;

    @BindView(R.id.mnDatenschutz)
    protected TextView mnDatenschutz;

    @BindView(R.id.mnImpressum)
    protected TextView mnImpressum;

    @BindView(R.id.mnAGB)
    protected TextView mnAGB;

    protected List<Category> categoryList;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_primary_base);
        ButterKnife.bind(this);
        setSupportActionBar(abToolBar);
        initLayout();
        if (categoryList == null)
            getListCategories();
    }

    private void initLayout(){
        abLinearRightDrawerToggle.setOnClickListener(this);
        abDrawerRightMenuIconInside.setOnClickListener(this);
        mnTxtDashBoard.setOnClickListener(this);
        mnDatenschutz.setOnClickListener(this);
        mnImpressum.setOnClickListener(this);
        mnTxtNews.setOnClickListener(this);
        mnKontakt.setOnClickListener(this);
        mnAGB.setOnClickListener(this);
    }

    protected void initRightMenuForNewsMenu(List<Category> categoryList){
        List<TextView> textViewList = ViewUtil.generateTxtForRightMenu(this,categoryList, this);
        for (TextView txtSubMenu : textViewList){
//            mnLinearNewsContainer.addView(txtSubMenu);
        }
    }

    private void goToNewsActivity(){
        Intent intent = new Intent(this, NewsActivity.class);
        startActivity(intent);
    }

    private void goToDashBoardActivity(){
        Intent intent = new Intent(this, DashBoardActivity.class);
        startActivity(intent);
    }

    private void gotoKontaktActivity(){
        Intent intent = new Intent(this, KonTaktActivity.class);
        startActivity(intent);
    }

    private void gotoDatenschutz(){
        Intent intent = new Intent(this, DatenchutzActivity.class);
        startActivity(intent);
    }

    private void gotoImpressum(){
        Intent intent = new Intent(this, ImpressumActivity.class);
        startActivity(intent);
    }

    private void gotoAgb(){
        Intent intent = new Intent(this, AGBActivity.class);
        startActivity(intent);
    }

    private void closeRightMenu(){
        abDrawerRightMenu.closeDrawer(GravityCompat.END);
    }

    private void openRightMenu(){
        abDrawerRightMenu.openDrawer(GravityCompat.END);
    }

    protected ViewGroup getContainerView(){
        return abConstraintContainer;
    }
    // TODO: OnClickListener
    @Override
    public void onClick(View view) {

        switch (view.getId()){
            case R.id.abLinearRightDrawerToggle:
                openRightMenu();
                break;
            case R.id.abDrawerRightMenuIconInside:
                closeRightMenu();
                break;
            case R.id.mnTxtDashBoard:
                if (!isOnline()){
                    closeRightMenu();
                    return;
                }
                if (!getActivityName().equals(DashBoardActivity.class.getName()))
                    goToDashBoardActivity();
                closeRightMenu();
                break;
            case R.id.mnTxtNews:
                if (!isOnline()){
                    closeRightMenu();
                    return;
                }
                if (!getActivityName().equals(NewsActivity.TAG))
                    goToNewsActivity();
                closeRightMenu();
                break;
            case R.id.mnKontakt:
                if (!isOnline()){
                    closeRightMenu();
                    return;
                }
                if (!getActivityName().equals(KonTaktActivity.TAG))
                    gotoKontaktActivity();
                closeRightMenu();
                break;
            case R.id.mnDatenschutz:
                if (!isOnline()){
                    closeRightMenu();
                    return;
                }
                if (!getActivityName().equals(DatenchutzActivity.TAG))
                    gotoDatenschutz();
                closeRightMenu();
                break;
            case R.id.mnImpressum:
                if (!isOnline()){
                    closeRightMenu();
                    return;
                }
                if (!getActivityName().equals(ImpressumActivity.TAG))
                    gotoImpressum();
                closeRightMenu();
                break;

            case R.id.mnAGB:
                if (!isOnline()){
                    closeRightMenu();
                    return;
                }
                if (!getActivityName().equals(AGBActivity.TAG))
                    gotoAgb();
                closeRightMenu();
                break;
        }
    }


    @Override
    public void onClickMenuItem(Category category) {
        if (!isOnline()) {
            closeRightMenu();
            return;
        }
        int position = categoryList.indexOf(category);
        if (!getActivityName().equals(NewsArticleActivity.TAG)) {
            goToNewsArticleActivity(position);
        } else {
            EventBus.getDefault().post(new OnPostMenuPositionEvent(position));
        }
        closeRightMenu();
    }

    private void goToNewsArticleActivity(int position){
        Intent intent = new Intent(this, NewsArticleActivity.class);
        Bundle bundle = new Bundle();
        bundle.putInt(Utils.KEY_RIGHT_MENU_POSITION, position);
        bundle.putParcelableArrayList(Utils.KEY_CATEGORY_LIST, (ArrayList<? extends Parcelable>) categoryList);
        intent.putExtra(Utils.KEY_CATEGORY_BUNDLE, bundle);
        startActivity(intent);
    }

    protected void getListCategories(){
        String accessToken = Utils.getAccessToken();
        APIUtils.getAPIService().getListCategories("Bearer " + accessToken)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<ResponseCategory>() {
                    @Override
                    public void onNext(@NonNull ResponseCategory responseCategory) {
                        categoryList = responseCategory.getData();
//                        List<Category> newList = new ArrayList<>();
//                        newList.add(new Category(0, "All"));
//                        for (int i = 0; i < categoryList.size(); i++) {
//                            newList.add(new Category(categoryList.get(i).getId(), categoryList.get(i).getName()));
//                        }
//                        newList.add(new Category(100, "Favorites"));
//                        menuListRefactor(newList);
                        initRightMenuForNewsMenu(categoryList);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        Log.e("BaseActivity","onError");
                    }

                    @Override
                    public void onComplete() {
                        Log.e("BaseActivity","onComplete");
                    }
                });
    }

}
