package org.fomosapiens.fomo.view.customview;

import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by HuyMTB on 7/20/18.
 */

public class FontChange {

    private Typeface typeface;

    public FontChange(Typeface typeface) {
        this.typeface = typeface;
    }

    public FontChange(AssetManager assetManager, String assetFontName) {
        typeface = Typeface.createFromAsset(assetManager, assetFontName);
    }

    public void replaceFonts(ViewGroup viewTree) {
        View child;
        for (int i = 0; i < viewTree.getChildCount(); i++) {
            child = viewTree.getChildAt(i);
            if (child instanceof ViewGroup) {
                replaceFonts((ViewGroup)child);
            } else if (child instanceof TextView) {
                ((TextView) child).setTypeface(typeface);
            }
        }
    }
}
