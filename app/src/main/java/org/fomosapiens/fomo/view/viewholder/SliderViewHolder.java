package org.fomosapiens.fomo.view.viewholder;

import android.view.View;
import android.content.Context;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.RadioGroup;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;

import org.fomosapiens.fomo.R;
import org.fomosapiens.fomo.Utils;
import org.fomosapiens.fomo.model.News;
import org.fomosapiens.fomo.model.DashBoard;
import org.fomosapiens.fomo.view.customview.FontChange;
import org.fomosapiens.fomo.view.interfacelistener.OnSetDataFinishListener;
import org.fomosapiens.fomo.view.ui.ViewUtil;
import org.fomosapiens.fomo.view.adapter.AdapterPagerDashBoard;
import org.fomosapiens.fomo.view.interfacelistener.OnClickNewsDetailListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by PC162 on 9/6/2017.
 */

public class SliderViewHolder extends RecyclerView.ViewHolder implements ViewPager.OnPageChangeListener {

    @BindView(R.id.adtslViewFirstLine)
    protected View adtslViewFirstLine;

    @BindView(R.id.adtslTxtHeader)
    protected TextView adtslTxtHeader;

    @BindView(R.id.adtslViewPager)
    protected ViewPager adtslViewPager;

    @BindView(R.id.adtslRadioGroup)
    protected RadioGroup adtslRadioGroup;

    @BindView(R.id.adtslView)
    protected LinearLayout adtslView;

    private OnClickNewsDetailListener onClickNewsDetailListener;
    private OnSetDataFinishListener onSetDataFinishListener;
    private AdapterPagerDashBoard adapterPagerDashBoard;
    private List<Integer> radioButtonIdList;
    private Context context;

    public SliderViewHolder(View itemView, Context context, OnClickNewsDetailListener onClickNewsDetailListener, OnSetDataFinishListener onSetDataFinishListener) {
        super(itemView);
        this.context = context;
        this.onSetDataFinishListener = onSetDataFinishListener;
        this.onClickNewsDetailListener = onClickNewsDetailListener;
        ButterKnife.bind(this, itemView);
        Utils.setFontName(context, (ViewGroup)itemView);
    }

    public void setDashboard(List<DashBoard> dashboards, int position) {

        if (position == dashboards.size() - 1)
            adtslViewFirstLine.setVisibility(View.GONE);
        else
            adtslViewFirstLine.setVisibility(View.VISIBLE);

        if (adapterPagerDashBoard==null)
            createSliderPart(dashboards.get(position));
        else {
            onSetDataFinishListener.onLoadFinish();
            return;
        }

    }

    public void hideDashBoard() {
        adtslView.setVisibility(View.VISIBLE);
    }

    private void createSliderPart(DashBoard dashBoard) {

        List<News> listNews = dashBoard.getNewsList();

        if (listNews == null || listNews.size() == 0) {
            adtslView.setVisibility(View.GONE);
            adtslView.setLayoutParams(new RecyclerView.LayoutParams(0, 0));
            onSetDataFinishListener.onLoadFinish();
        } else {
            String sectionDescription = dashBoard.getSection_description();
            boolean isShowPicture = Utils.getIsShowPicture(dashBoard.getShow_picture());
            adtslTxtHeader.setText(sectionDescription);
            adtslView.setVisibility(View.VISIBLE);
            createListRadioButtons(listNews.size());
            adtslViewPager.setVisibility(View.VISIBLE);
            adapterPagerDashBoard = new AdapterPagerDashBoard(context, listNews, onClickNewsDetailListener, isShowPicture);
            adtslViewPager.setAdapter(adapterPagerDashBoard);
            adtslViewPager.setOnPageChangeListener(this);
            onSetDataFinishListener.onLoadFinish();
        }
    }

    private void createListRadioButtons(int size) {
        radioButtonIdList = new ArrayList<>();
        radioButtonIdList = ViewUtil.generateRadioButtonList(context, size, adtslRadioGroup);
    }

    private void setCheckRadioGroup(int position) {
        adtslRadioGroup.check(radioButtonIdList.get(position));
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        setCheckRadioGroup(position);
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
