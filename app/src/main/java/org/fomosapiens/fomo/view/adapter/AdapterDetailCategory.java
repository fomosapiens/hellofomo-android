package org.fomosapiens.fomo.view.adapter;

import android.app.Activity;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.fomosapiens.fomo.R;
import org.fomosapiens.fomo.Utils;
import org.fomosapiens.fomo.model.Author;
import org.fomosapiens.fomo.model.LayoutSettings;
import org.fomosapiens.fomo.model.NewsDetail;
import org.fomosapiens.fomo.model.RelatedNews;
import org.fomosapiens.fomo.view.customview.FontChange;
import org.fomosapiens.fomo.view.customview.ItemOffsetDecoration;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by HuyMTB on 9/9/18.
 *
 */

public class AdapterDetailCategory extends RecyclerView.Adapter<AdapterDetailCategory.ViewHolder> {

    private NewsDetail newsDetail;
    private List<LayoutSettings> layoutSettingsList;
    private Activity context;

    public AdapterDetailCategory(NewsDetail newsDetail, Activity context) {
        this.newsDetail = newsDetail;
        this.context = context;
        layoutSettingsList = newsDetail.getLayoutSettings();
    }

    @Override
    public AdapterDetailCategory.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.refactor_item_detail_category, parent, false);
        Utils.setFontName(parent.getContext(), (ViewGroup)view);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AdapterDetailCategory.ViewHolder holder, int position) {
        LayoutSettings layoutSettings = layoutSettingsList.get(position);
        String title = layoutSettings.getKey();
        int value = layoutSettings.getValue();
        if (value == 1) {
            switch (title) {
                case "related_news" :
                    List<RelatedNews> relatedNews = newsDetail.getRelated_news();
                    if (relatedNews != null && !relatedNews.isEmpty()) {
                        holder.title.setText(context.getString(R.string.related_post));
                        holder.layout.setVisibility(View.VISIBLE);
                        AdapterNewsRelatedPost adapterNewsRelatedPost = new AdapterNewsRelatedPost(context, relatedNews);
                        LinearLayoutManager linearLayoutManager1 = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
                        holder.recyclerView.setLayoutManager(linearLayoutManager1);
                        holder.recyclerView.setAdapter(adapterNewsRelatedPost);
                    }
                    else {
                        holder.layout.setVisibility(View.GONE);
                        holder.layout.setLayoutParams(new RecyclerView.LayoutParams(0, 0));
                    }
                    break;

                case "gallery":
                    List<String> listImage = newsDetail.getGalleries();
                    if (listImage != null && !listImage.isEmpty()) {
                        holder.layout.setVisibility(View.VISIBLE);
                        holder.title.setText(context.getString(R.string.gallery));
                        AdapterNewsGallery adapterNewsGallery = new AdapterNewsGallery(context, listImage);
                        GridLayoutManager layoutManager = new GridLayoutManager(context,3, GridLayoutManager.VERTICAL, false);
                        ItemOffsetDecoration itemDecoration = new ItemOffsetDecoration(context, R.dimen._10sdp);
                        int spanCount = 3; // 3 columns
                        int spacing = (int) context.getResources().getDimension(R.dimen._10sdp);
                        boolean includeEdge = true;
                        holder.recyclerView.addItemDecoration(new ItemOffsetDecoration(spanCount, spacing, includeEdge));
                        holder.recyclerView.setLayoutManager(layoutManager);
//                        holder.recyclerView.addItemDecoration(itemDecoration);
                        holder.recyclerView.setAdapter(adapterNewsGallery);
                    }
                    else {
                        holder.layout.setVisibility(View.GONE);
                        holder.layout.setLayoutParams(new RecyclerView.LayoutParams(0, 0));
                    }

                    break;

                case "files":
                    List<String> attachList = newsDetail.getAttachments();
                    if (attachList != null && !attachList.isEmpty()) {
                        holder.title.setText(context.getString(R.string.file));
                        holder.layout.setVisibility(View.VISIBLE);
                        AdapterFileAttachment adapterFileAttachment = new AdapterFileAttachment(context, attachList);
                        LinearLayoutManager linearLayoutManager2 = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
                        holder.recyclerView.setLayoutManager(linearLayoutManager2);
                        holder.recyclerView.setAdapter(adapterFileAttachment);
                    }
                    else {
                        holder.layout.setVisibility(View.GONE);
                        holder.layout.setLayoutParams(new RecyclerView.LayoutParams(0, 0));
                    }

                    break;

                case "authors":
                    List<Author> authorList = newsDetail.getAuthors();
                    if (authorList != null && !authorList.isEmpty()) {
                        holder.layout.setVisibility(View.VISIBLE);
                        holder.title.setText(context.getString(R.string.author));
                        AdapterNewsAuthors adapterNewsAuthors = new AdapterNewsAuthors(authorList, context);
                        GridLayoutManager layoutManager3 = new GridLayoutManager(context,2, GridLayoutManager.VERTICAL, false);
                        ItemOffsetDecoration itemDecoration1 = new ItemOffsetDecoration(context, R.dimen._5sdp);
                        int spanCount = 2; // 3 columns
                        int spacing = (int) context.getResources().getDimension(R.dimen._5sdp);
                        boolean includeEdge = true;
                        holder.recyclerView.addItemDecoration(new ItemOffsetDecoration(spanCount, spacing, includeEdge));
                        holder.recyclerView.setLayoutManager(layoutManager3);
//                        holder.recyclerView.addItemDecoration(itemDecoration1);
                        holder.recyclerView.setAdapter(adapterNewsAuthors);
                    }
                    else {
                        holder.layout.setVisibility(View.GONE);
                        holder.layout.setLayoutParams(new RecyclerView.LayoutParams(0, 0));
                    }

                    break;

                case "tags":
                    break;
            }
        }
        else {
            holder.layout.setVisibility(View.GONE);
            holder.layout.setLayoutParams(new RecyclerView.LayoutParams(0, 0));
        }
    }

    @Override
    public int getItemCount() {
        return layoutSettingsList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.refactor_news_detail_list_item)
        RecyclerView recyclerView;
        @BindView(R.id.refactor_news_detail_title_item)
        TextView title;
        @BindView(R.id.refactor_news_detail_view_item)
        View view;
        @BindView(R.id.refactor_news_detail_layout_item)
        RelativeLayout layout;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
