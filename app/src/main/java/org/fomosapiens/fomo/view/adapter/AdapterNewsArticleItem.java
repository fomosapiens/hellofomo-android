package org.fomosapiens.fomo.view.adapter;

import android.content.Context;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.fomosapiens.fomo.R;
import org.fomosapiens.fomo.Utils;
import org.fomosapiens.fomo.model.News;
import org.fomosapiens.fomo.view.customview.FontChange;
import org.fomosapiens.fomo.view.interfacelistener.OnClickNewsDetailListener;
import org.fomosapiens.fomo.view.interfacelistener.OnRecyclerViewScrollListener;
import org.fomosapiens.fomo.view.ui.ViewUtil;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by PC162 on 8/15/2017.
 */

public class AdapterNewsArticleItem extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements View.OnClickListener {

    private final int VIEW_ITEM = 1;
    private final int VIEW_PROGRESS = 0;

    private List<News> newsList;
    private Context context;
    private  News news;
    private LinearLayoutManager mLinearLayoutManager;
    private OnClickNewsDetailListener onClickNewsDetailListener;
    private OnRecyclerViewScrollListener onRecyclerViewScrollListener;

    private int lastVisibleItem;
    private int totalItemCount;
    private Float mStartY;
    private boolean isLoadMore = false;
    private FontChange fontChange;

    public AdapterNewsArticleItem(Context context, List<News> newsList, OnClickNewsDetailListener onClickNewsDetailListener){
        this.onClickNewsDetailListener = onClickNewsDetailListener;
        this.newsList = newsList;
        this.context = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_ITEM) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_news_article_item, parent, false);
            Utils.setFontName(context, (ViewGroup)view);
            return new RecyclerViewHolder(view);
        } else {
            return new ProgressViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_news_article_item_load_more_item, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if  (holder instanceof ProgressViewHolder) {
            ((ProgressViewHolder) holder).adnaProgressBar.setIndeterminate(true);

        }

        else if (holder instanceof RecyclerViewHolder) {
            news = newsList.get(position);
            String imageUrl = news.getPrimary_image();

            String date = news.getAvailable_date() == null ? "" : news.getAvailable_date();
            String title = news.getTitle() == null ? "" : news.getTitle();

            ((RecyclerViewHolder) holder).adnaTxtDate.setText(Utils.getDate(date));
            ((RecyclerViewHolder) holder).adnaTxtTitle.setText(title);
            ViewUtil.setBackgroundForImageView(context, imageUrl, ((RecyclerViewHolder) holder).adnaImgIcon);
            ((RecyclerViewHolder) holder).adnaRelativeRoot.setTag(R.id.tag_viewholder, news);
            ((RecyclerViewHolder) holder).adnaTxtTitle.setTag(R.id.tag_viewholder, news);
            ((RecyclerViewHolder) holder).adnaRelativeRoot.setOnClickListener(this);
            ((RecyclerViewHolder) holder).adnaTxtTitle.setOnClickListener(this);
        }
    }

    @Override
    public int getItemCount() {
        return  newsList == null ? 0 : newsList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return newsList.get(position) != null ? VIEW_ITEM : VIEW_PROGRESS;
    }

    public void setRefreshData(){
        newsList.clear();
        notifyDataSetChanged();
    }

    public void addMoreItem(List<News> mNewsList){
        if (mNewsList == null || mNewsList.size() == 0) {
            removeProgressMore();
            return;
        }
        removeProgressMore();
        newsList.addAll(mNewsList);
        notifyItemRangeChanged(0, newsList.size());
    }

    public void setLinearLayoutManager(LinearLayoutManager linearLayoutManager){
        this.mLinearLayoutManager=linearLayoutManager;
    }

    public void setOnRecyclerViewListener(OnRecyclerViewScrollListener onRecyclerViewListener){
        this.onRecyclerViewScrollListener = onRecyclerViewListener;

    }

    public void removeProgressMore(){
        if (!isLoadMore)
            return;
        newsList.remove(newsList.size() - 1);
        notifyItemRemoved(newsList.size());
        isLoadMore = false;
    }

    public void setProgressMore() {
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                newsList.add(null);
                notifyItemInserted(newsList.size() - 1);
                isLoadMore = true;
            }
        });
    }

    public void setRecyclerView(RecyclerView mView){

        mView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                float loadMore = -200;
                switch (motionEvent.getAction()) {

                    case MotionEvent.ACTION_DOWN:
                        mStartY = motionEvent.getY();
                        break;

                    case MotionEvent.ACTION_UP:
                        if (mStartY == null)
                            break;

                        float temp = motionEvent.getY() - mStartY;
                        if (temp< loadMore && lastVisibleItem + 1  == totalItemCount && !isLoadMore) {
                            if (onRecyclerViewScrollListener != null) {
                                setProgressMore();
                                onRecyclerViewScrollListener.onLoadMoreListener();
                                break;
                            }
                        }
                        break;
                }
                return false;
            }
        });


        mView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                totalItemCount = mLinearLayoutManager.getItemCount();
                lastVisibleItem = mLinearLayoutManager.findLastCompletelyVisibleItemPosition();
            }
        });
    }

    @Override
    public void onClick(View view) {
        News news = (News) view.getTag(R.id.tag_viewholder);
        onClickNewsDetailListener.onClickItem(news);
    }

    static class RecyclerViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.adnaRelativeRoot)
        protected RelativeLayout adnaRelativeRoot;

        @BindView(R.id.adnaTxtTitle)
        protected TextView adnaTxtTitle;

        @BindView(R.id.adnaTxtDate)
        protected TextView adnaTxtDate;

        @BindView(R.id.adnaImgIcon)
        protected ImageView adnaImgIcon;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

    }

    static class ProgressViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.adnaProgressBar)
        protected ProgressBar adnaProgressBar;


        public ProgressViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }
}
