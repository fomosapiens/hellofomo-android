package org.fomosapiens.fomo.view.ui.regulation;


import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import org.fomosapiens.fomo.R;
import org.fomosapiens.fomo.SharePre;
import org.fomosapiens.fomo.Utils;
import org.fomosapiens.fomo.model.event.HistoryData;
import org.fomosapiens.fomo.model.event.HistoryResponse;
import org.fomosapiens.fomo.model.regulation.MasterDataResponse;
import org.fomosapiens.fomo.model.regulation.RegisterData;
import org.fomosapiens.fomo.model.regulation.RegisterResponse;
import org.fomosapiens.fomo.model.regulation.RegulationListElementData;
import org.fomosapiens.fomo.net.APIService;
import org.fomosapiens.fomo.net.APIUtils;
import org.fomosapiens.fomo.view.adapter.AdapterRegisterList;
import org.fomosapiens.fomo.view.adapter.ApdapterHistoryListLoadMore;
import org.fomosapiens.fomo.view.adapter.PaginationScrollListener;
import org.fomosapiens.fomo.view.customview.FontChange;
import org.fomosapiens.fomo.view.ui.authen.LoginActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * A simple {@link Fragment} subclass.
 */
public class HistoryRegulationFragments extends Fragment {

    @BindView(R.id.regulation_history_list)
    RecyclerView recyclerView;
    @BindView(R.id.history_art_spinner)
    Spinner artSpinner;
    @BindView(R.id.history_subject_spinner)
    Spinner subjectSpinner;
    @BindView(R.id.history_authority_spinner)
    Spinner authoritySpinner;
    @BindView(R.id.history_level_spinner)
    Spinner levelSpinner;
    @BindView(R.id.history_legal_spinner)
    Spinner legalSpinner;
    @BindView(R.id.history_search_edt)
    EditText edtSearch;
    @BindView(R.id.history_result_text)
    TextView resultTv;
    @BindView(R.id.history_register_list)
    RecyclerView registerListOnTop;
    @BindView(R.id.history_filter_reset)
    TextView resetFilter;
    @BindView(R.id.history_advance_btn)
    TextView advanceBtn;
    @BindView(R.id.history_advance_bg)
    ConstraintLayout advanceBg;
    @BindView(R.id.history_search_btn)
    TextView searchBtn;
    @BindView(R.id.historyRegisterWebView)
    WebView webView;
    @BindView(R.id.historyRegulationView)
    View coverView;

    private Dialog progressDialog;
    private boolean isShowAdvanceSearch = false;

    String specification_id;
    String level_id;
    String legal_nature_id;
    String authority_id;
    String register_id;
    String field_law_id;
    String keyword;

    List<RegisterData> listRegister;
    List<RegulationListElementData> listAuthority;
    List<RegulationListElementData> listSpecification;
    List<RegulationListElementData> listLegal;
    List<RegulationListElementData> listSubject;
    List<RegulationListElementData> listLevel;

    // loadmore
    private int PAGE_START = 1;
    private boolean isLoading = false;
    private boolean isLastPage = false;
    private int TOTAL_PAGES = 0;
    private int currentPage = PAGE_START;
    private int totalData = 0;
    private ApdapterHistoryListLoadMore adapterHistoryListLoadMore;

    public HistoryRegulationFragments() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_update_regulations, container, false);
        ButterKnife.bind(this, view);
        progressDialog = new ProgressDialog(getContext());
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Utils.setFontName(getContext(), (ViewGroup)this.getView());
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Utils.showProgressDialog(progressDialog);

        getListRegister();

        // set adapter
        // set adapter
        adapterHistoryListLoadMore = new ApdapterHistoryListLoadMore(getContext());
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapterHistoryListLoadMore);
        recyclerView.addOnScrollListener(new PaginationScrollListener(layoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                currentPage += 1;

                // mocking network delay for API call
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        getNextPageHistoryRegulations();
                    }
                }, 1000);
            }

            @Override
            public int getTotalPageCount() {
                return currentPage;
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });

        advanceBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isShowAdvanceSearch) {
                    advanceBg.setVisibility(View.GONE);
                    isShowAdvanceSearch = false;
                } else {
                    advanceBg.setVisibility(View.VISIBLE);
                    isShowAdvanceSearch = true;
                }
            }
        });
        searchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Utils.hideKeyboard(getContext(), edtSearch);
                Utils.showProgressDialog(progressDialog);
                keyword = edtSearch.getText().toString();
                if (keyword.equals("")) {
                    keyword = null;
                }
                adapterHistoryListLoadMore.removeAll();
                currentPage = PAGE_START;
                getFirstPageHistoryRegulations();
                if (isShowAdvanceSearch) {
                    advanceBg.setVisibility(View.GONE);
                    isShowAdvanceSearch = false;
                }
            }
        });
        resetFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Utils.showProgressDialog(progressDialog);
                specification_id = null;
                level_id = null;
                legal_nature_id = null;
                authority_id = null;
                field_law_id = null;
                keyword = null;

                artSpinner.setSelection(0);
                subjectSpinner.setSelection(0);
                legalSpinner.setSelection(0);
                levelSpinner.setSelection(0);
                authoritySpinner.setSelection(0);
                edtSearch.setText("");

                adapterHistoryListLoadMore.removeAll();
                currentPage = PAGE_START;
                getFirstPageHistoryRegulations();
            }
        });

        // spinner selection
        subjectSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i != 0) {
                    field_law_id = listSubject.get(i-1).getId().toString();
                } else {
                    field_law_id = null;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        artSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i != 0) {
                    specification_id = listSpecification.get(i-1).getId().toString();
                } else {
                    specification_id = null;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        legalSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i != 0) {
                    legal_nature_id = listLegal.get(i-1).getId().toString();
                } else {
                    legal_nature_id = null;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        levelSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i != 0) {
                    level_id = listLevel.get(i-1).getId().toString();
                } else {
                    level_id = null;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        authoritySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i != 0) {
                    authority_id = listAuthority.get(i-1).getId().toString();
                } else {
                    authority_id = null;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        // load webview
        WebViewClient client = new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                Utils.hideProgressDialog(progressDialog);
            }
        };
        webView.setWebViewClient(client);
    }

    private void getFirstPageHistoryRegulations() {
        APIService service = APIUtils.getAPIService();
        String accessToken = Utils.getAccessToken();
        service.getHistoryList("Bearer " + accessToken, PAGE_START, specification_id, level_id,
                legal_nature_id, authority_id, register_id, field_law_id, keyword)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<HistoryResponse>() {
                    @Override
                    public void onNext(HistoryResponse historyResponse) {
                        totalData = historyResponse.getTotal();
                        if (totalData != 0) {
                            TOTAL_PAGES = Math.round(totalData / 10) + 1;
                        } else {
                            TOTAL_PAGES = 0;
                        }

                        List<HistoryData> data = historyResponse.getData();
                        adapterHistoryListLoadMore.addAll(data);

                        if (currentPage <= TOTAL_PAGES) {
                            adapterHistoryListLoadMore.addLoadingFooter();
                            isLastPage = false;
                        } else {
                            isLastPage = true;
                        }

                        resultTv.setText("Suchergebnisse: " + data.size() + "/ " + totalData);
                        coverView.setVisibility(View.GONE);
                        Utils.hideProgressDialog(progressDialog);
                    }

                    @Override
                    public void onError(Throwable e) {
                        webView.getSettings().setJavaScriptEnabled(true);
                        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
                        webView.loadUrl(Utils.registerUrl);
                        webView.setVisibility(View.VISIBLE);
                        Utils.hideProgressDialog(progressDialog);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    private void getNextPageHistoryRegulations() {
        APIService service = APIUtils.getAPIService();
        String accessToken = Utils.getAccessToken();
        service.getHistoryList("Bearer " + accessToken, currentPage, specification_id, level_id,
                legal_nature_id, authority_id, register_id, field_law_id, keyword)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<HistoryResponse>() {
                    @Override
                    public void onNext(HistoryResponse historyResponse) {
                        if (isLoading) {
                            adapterHistoryListLoadMore.removeLoadingFooter();
                            isLoading = false;

                            List<HistoryData> listData = historyResponse.getData();
                            adapterHistoryListLoadMore.addAll(listData);

                            if (currentPage < TOTAL_PAGES) adapterHistoryListLoadMore.addLoadingFooter();
                            else isLastPage = true;

                            int totalCount = adapterHistoryListLoadMore.getItemCount();

                            if (totalCount >= totalData) {
                                totalCount = totalData;
                            }

                            resultTv.setText("Suchergebnisse: " + totalCount + "/ " + totalData);
                            coverView.setVisibility(View.GONE);
                            Utils.hideProgressDialog(progressDialog);
                        }

                    }

                    @Override
                    public void onError(Throwable e) {
                        webView.getSettings().setJavaScriptEnabled(true);
                        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
                        webView.loadUrl(Utils.registerUrl);
                        webView.setVisibility(View.VISIBLE);
                        Utils.hideProgressDialog(progressDialog);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    private void getListMasterData() {
        APIService service = APIUtils.getAPIService();
        String accessToken = Utils.getAccessToken();
        service.getMasterData("Bearer " + accessToken, register_id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<MasterDataResponse>() {
                    @Override
                    public void onNext(MasterDataResponse masterDataResponse) {
                        listAuthority = masterDataResponse.getData().getAuthorities();
                        listLegal = masterDataResponse.getData().getLegals();
                        listLevel = masterDataResponse.getData().getLevels();
                        listSpecification = masterDataResponse.getData().getSpecifications();
                        listSubject = masterDataResponse.getData().getFieldOfLaws();

                        setDataSpinner(subjectSpinner, listSubject);
                        setDataSpinner(levelSpinner, listLevel);
                        setDataSpinner(legalSpinner, listLegal);
                        setDataSpinner(artSpinner, listSpecification);
                        setDataSpinner(authoritySpinner, listAuthority);
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    private void getListRegister() {
        APIService service = APIUtils.getAPIService();
        String accessToken = Utils.getAccessToken();
        service.getRegulationRegisterList("Bearer " + accessToken, 1, 0, 10, null)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<RegisterResponse>() {
                    @Override
                    public void onNext(RegisterResponse registerResponse) {
                        listRegister = registerResponse.getData();
                        List<RegulationListElementData> listElementData = new ArrayList<>();
                        for (int i = 0; i < listRegister.size(); i++) {
                            RegulationListElementData data = new RegulationListElementData();
                            data.setId(listRegister.get(i).getId());
                            data.setName(listRegister.get(i).getName());
                            listElementData.add(data);
                        }
//                        setDataSpinner(registerSpinner, listElementData);

                        if (listRegister.size() > 0) {
                            register_id = String.valueOf(listRegister.get(0).getId());
                            getFirstPageHistoryRegulations();
                            AdapterRegisterList.ListRegisterClick click = new AdapterRegisterList.ListRegisterClick() {
                                @Override
                                public void itemClick(String registerId) {
                                    Utils.showProgressDialog(progressDialog);
                                    register_id = registerId;
                                    adapterHistoryListLoadMore.removeAll();
                                    getFirstPageHistoryRegulations();
                                    getListMasterData();
                                    isLoading = false;
                                    currentPage = PAGE_START;
                                }
                            };

                            // set adapter register list
                            AdapterRegisterList adapterRegisterList = new AdapterRegisterList(getContext(), listElementData, click);
                            LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
                            registerListOnTop.setLayoutManager(layoutManager);
                            registerListOnTop.setAdapter(adapterRegisterList);
                        } else {
                            getFirstPageHistoryRegulations();
                        }

                    }

                    @Override
                    public void onError(Throwable e) {
                        getFirstPageHistoryRegulations();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    private void setDataSpinner(Spinner spinner, List<RegulationListElementData> elementData) {
        List<String> data = new ArrayList<>();
        data.add(0, "All");
        if (elementData != null) {
            for (int i = 0; i < elementData.size(); i++) {
                data.add(elementData.get(i).getName());
            }
        }
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_item, data);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_list_item_single_choice);
        spinner.setAdapter(arrayAdapter);
    }
}

