package org.fomosapiens.fomo.view.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import org.fomosapiens.fomo.R;
import org.fomosapiens.fomo.Utils;
import org.fomosapiens.fomo.model.RelatedNews;
import org.fomosapiens.fomo.view.customview.FontChange;
import org.fomosapiens.fomo.view.ui.newsdetail.RefactorNewsDetailActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by HuyMTB on 7/18/18.
 */

public class AdapterNewsRelatedPost extends RecyclerView.Adapter<AdapterNewsRelatedPost.ViewHolder> {

    private List<RelatedNews> relatedNewsList;
    private Activity activity;

    public AdapterNewsRelatedPost(Activity activity, List<RelatedNews> list) {
        this.activity = activity;
        this.relatedNewsList = list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(activity).inflate(R.layout.refactor_item_news_related_post, parent, false);
        Utils.setFontName(parent.getContext(), (ViewGroup)view);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final RelatedNews item = relatedNewsList.get(position);

        holder.newsTitle.setText(item.getTitle());
        Glide.with(activity).load(item.getThumbnail()).into(holder.newsImage);

        holder.newsTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(activity, RefactorNewsDetailActivity.class);
                intent.putExtra(Utils.KEY_NEWS_ID, item.getId());
                activity.startActivity(intent);
            }
        });
        if (holder.getAdapterPosition() == relatedNewsList.size() -1) {
            holder.view.setBackgroundColor(ContextCompat.getColor(activity, R.color.color_white));
        } else {
            holder.view.setBackgroundColor(ContextCompat.getColor(activity, R.color.color_dark));
        }

    }

    @Override
    public int getItemCount() {
        return relatedNewsList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.refactor_related_post_image)
        ImageView newsImage;
        @BindView(R.id.refactor_related_post_title)
        TextView newsTitle;
        @BindView(R.id.refactor_related_post_view)
        View view;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
