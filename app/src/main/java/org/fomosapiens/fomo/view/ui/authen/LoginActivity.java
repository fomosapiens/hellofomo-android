package org.fomosapiens.fomo.view.ui.authen;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import org.fomosapiens.fomo.BuildConfig;
import org.fomosapiens.fomo.R;
import org.fomosapiens.fomo.SharePre;
import org.fomosapiens.fomo.Utils;
import org.fomosapiens.fomo.model.Login;
import org.fomosapiens.fomo.net.APIService;
import org.fomosapiens.fomo.net.APIUtils;
import org.fomosapiens.fomo.net.entity.LoginEntity;
import org.fomosapiens.fomo.view.customview.FontChange;
import org.fomosapiens.fomo.view.ui.BaseActivity;
import org.fomosapiens.fomo.view.ui.RefactorPrimaryBaseActivity;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.IOException;
import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.HttpException;

public class LoginActivity extends BaseActivity {

    @BindView(R.id.login_btn)
    Button loginBtn;
    @BindView(R.id.login_user_name_edt)
    EditText userNameEdt;
    @BindView(R.id.login_password_edt)
    EditText passwordEdt;
    @BindView(R.id.login_check_store_pass)
    CheckBox savePass;
    @BindView(R.id.login_forgot_pass)
    TextView forgotPass;

    private Dialog progressDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        progressDialog = new Dialog(this);
        Utils.setFontName(this, (ViewGroup)this.findViewById(android.R.id.content));

        // check is store pass
        boolean isCheckStorePass = SharePre.getBooleanFalse(SharePre.KEY_STORE_PASS);
        if (isCheckStorePass) {
            String user_name_store = SharePre.getString(SharePre.KEY_USERNAME);
            String pass_word = SharePre.getString(SharePre.KEY_PASSWORD);
            userNameEdt.setText(user_name_store);
            passwordEdt.setText(pass_word);
            savePass.setChecked(true);
        } else {
            savePass.setChecked(false);
            userNameEdt.setText("");
            passwordEdt.setText("");
        }

        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String userName = userNameEdt.getText().toString();
                String passWord = passwordEdt.getText().toString();
                if (userName.equals("")) {
                    Toast.makeText(LoginActivity.this, "The username field is required", Toast.LENGTH_SHORT).show();
                } else if (passWord.equals("")) {
                    Toast.makeText(LoginActivity.this, "The password field is required", Toast.LENGTH_SHORT).show();
                } else {
                    Utils.showProgressDialog(progressDialog);
                    login(userName, passWord);
                }
            }
        });

        forgotPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginActivity.this, ForgotPassActivity.class);
                startActivity(intent);
            }
        });
    }

    private void login(final String username, final String password){
        APIService service = APIUtils.getAPIService();
        service.loginWithPassword(new LoginEntity(username, password, BuildConfig.CLIENT_ID_PASSWORD, BuildConfig.CLIENT_SECRET_PASSWORD))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<Login>() {
                    @Override
                    public void onNext(@NonNull Login login) {
                        saveLogin(login);
                        storePass(username, password);
                        SharePre.saveBoolean(SharePre.KEY_IS_LOGIN, true);
                        Utils.hideProgressDialog(progressDialog);
                        goToMainActivity();
                    }

                    @Override
                    public void onError(@NonNull Throwable t) {
                        Log.e("LoginActivity",""+t.toString());
                        ResponseBody body = ((HttpException) t).response().errorBody();
                        Gson gson = new Gson();
                        TypeAdapter<Login> adapter = gson.getAdapter(Login.class);
                        try {
                            Login errorParser = adapter.fromJson(body.string());
                            String msg = errorParser.getMessage();
                            final AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
                            builder.setMessage(msg);
                            builder.setCancelable(false);
                            builder.setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                }
                            });
                            builder.show();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        Utils.hideProgressDialog(progressDialog);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    private void saveLogin(Login login){
        SharePre.saveString(SharePre.KEY_CUSTOMER_ACCESS_TOKEN, login.getAccess_token());
        SharePre.saveInt(SharePre.KEY_IS_LEGALLY_REGISTER, login.getIsLegallyRegister());
    }

    private void storePass(String username, String password) {
        if (savePass.isChecked()) {
            SharePre.saveBoolean(SharePre.KEY_STORE_PASS, true);
            SharePre.saveString(SharePre.KEY_USERNAME, username);
            SharePre.saveString(SharePre.KEY_PASSWORD, password);
        } else {
            SharePre.saveBoolean(SharePre.KEY_STORE_PASS, false);
            SharePre.saveString(SharePre.KEY_USERNAME, "");
            SharePre.saveString(SharePre.KEY_PASSWORD, "");
        }

    }

    private void goToMainActivity() {
        Intent intent = new Intent(LoginActivity.this, RefactorPrimaryBaseActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onBackPressed() {
        goToMainActivity();
    }
}
