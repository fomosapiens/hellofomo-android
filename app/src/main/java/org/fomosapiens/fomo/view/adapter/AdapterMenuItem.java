package org.fomosapiens.fomo.view.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.BinderThread;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import org.fomosapiens.fomo.R;
import org.fomosapiens.fomo.Utils;
import org.fomosapiens.fomo.model.Category;
import org.fomosapiens.fomo.model.SubMenuItem;
import org.fomosapiens.fomo.view.customview.FontChange;
import org.fomosapiens.fomo.view.settings.SettingsActivity;
import org.fomosapiens.fomo.view.ui.PrimaryBaseActivity;
import org.fomosapiens.fomo.view.ui.RefactorPrimaryBaseActivity;
import org.fomosapiens.fomo.view.ui.dashboard.DashBoardActivity;
import org.fomosapiens.fomo.view.ui.news.NewsActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by HuyMTB on 7/9/18.
 *
 */

public class AdapterMenuItem extends RecyclerView.Adapter<AdapterMenuItem.ViewHolder> {

    private List<SubMenuItem> menuItemList;
    private Activity context;
    private List<Category> categories;
    private MenuItemClick itemClick;

    public AdapterMenuItem(Activity context, List<SubMenuItem> menuItemList, List<Category> categories, MenuItemClick itemClick) {
        this.menuItemList = menuItemList;
        this.categories = categories;
        this.context = context;
        this.itemClick = itemClick;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.refactor_menu_recycle_item, parent, false);
        Utils.setFontName(context, (ViewGroup)view);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final SubMenuItem item = menuItemList.get(position);

        holder.title.setText(item.getTitle());

        if (item.getTitle().equals(context.getString(R.string.news))) {

            holder.expandItem.setVisibility(View.VISIBLE);
            AdapterSubMenuItem adapterMenuItem = new AdapterSubMenuItem(context, categories);
            LinearLayoutManager mLayoutManager = new LinearLayoutManager(context);
            holder.subMenuRecycleView.setLayoutManager(mLayoutManager);
            holder.subMenuRecycleView.setAdapter(adapterMenuItem);

        } else if (item.getTitle().equals(context.getString(R.string.legal_menu))) {

            holder.expandItem.setVisibility(View.VISIBLE);
            List<Category> categories = new ArrayList<>();
            categories.add(new Category(101, context.getString(R.string.agb)));
            categories.add(new Category(102, context.getString(R.string.datenschutz)));
            categories.add(new Category(103, context.getString(R.string.impressum)));
            categories.add(new Category(104, context.getString(R.string.kontakt)));
            AdapterSubMenuItem adapterMenuItem = new AdapterSubMenuItem(context, categories);
            LinearLayoutManager mLayoutManager = new LinearLayoutManager(context);
            holder.subMenuRecycleView.setLayoutManager(mLayoutManager);
            holder.subMenuRecycleView.setAdapter(adapterMenuItem);

        } else if (item.getTitle().equals(context.getString(R.string.register_menu_item))) {
            holder.expandItem.setVisibility(View.VISIBLE);
            List<Category> categories = new ArrayList<>();
            categories.add(new Category(201, context.getString(R.string.regulation)));
            categories.add(new Category(202, context.getString(R.string.update_regulations)));
            AdapterSubMenuItem adapterMenuItem = new AdapterSubMenuItem(context, categories);
            LinearLayoutManager mLayoutManager = new LinearLayoutManager(context);
            holder.subMenuRecycleView.setLayoutManager(mLayoutManager);
            holder.subMenuRecycleView.setAdapter(adapterMenuItem);
        } else if (item.getTitle().equals(context.getString(R.string.service))) {
            holder.expandItem.setVisibility(View.VISIBLE);
            List<Category> categories = new ArrayList<>();
            categories.add(new Category(301, context.getString(R.string.company)));
            categories.add(new Category(302, context.getString(R.string.marketplace)));
            AdapterSubMenuItem adapterMenuItem = new AdapterSubMenuItem(context, categories);
            LinearLayoutManager mLayoutManager = new LinearLayoutManager(context);
            holder.subMenuRecycleView.setLayoutManager(mLayoutManager);
            holder.subMenuRecycleView.setAdapter(adapterMenuItem);
        }

        holder.expandItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean expanded = item.isExpand();
                item.setExpand(!expanded);
                if (expanded) {
                    holder.subMenuRecycleView.setVisibility(View.GONE);
                    holder.expandItem.setText("+");
                } else {
                    holder.subMenuRecycleView.setVisibility(View.VISIBLE);
                    holder.expandItem.setText("-");
                }
            }
        });

        holder.title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((RefactorPrimaryBaseActivity)context).isOnline()) {
                    itemClick.titleClick(item.getTitle());
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return menuItemList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.refactor_menu_item)
        TextView title;
        @BindView(R.id.refactor_sub_menu_item)
        RecyclerView subMenuRecycleView;
        @BindView(R.id.refactor_expand_menu_item)
        TextView expandItem;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface MenuItemClick {
        void titleClick(String title);
    }
}
