package org.fomosapiens.fomo.view.ui.authen;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.TypeAdapter;

import org.fomosapiens.fomo.R;
import org.fomosapiens.fomo.SharePre;
import org.fomosapiens.fomo.Utils;
import org.fomosapiens.fomo.model.response.ForgotPassword;
import org.fomosapiens.fomo.model.ForgotPasswordEnity;
import org.fomosapiens.fomo.model.response.ForgotPasswordErrors;
import org.fomosapiens.fomo.net.APIService;
import org.fomosapiens.fomo.net.APIUtils;
import org.fomosapiens.fomo.view.ui.BaseActivity;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.HttpException;

public class ForgotPassActivity extends BaseActivity {

    @BindView(R.id.forgot_bg_send_email)
    ConstraintLayout emailBg;
    @BindView(R.id.forgot_bg_update_pass)
    ConstraintLayout updatePassBg;
    @BindView(R.id.forgot_email_edt)
    EditText emailEdt;
    @BindView(R.id.send_code_btn)
    Button sendCodeBtn;
    @BindView(R.id.send_email_btn)
    Button sendEmailBtn;
    @BindView(R.id.forgot_code_edt)
    EditText codeEdt;
    @BindView(R.id.forgot_pass_edt)
    EditText passEdt;
    @BindView(R.id.forgot_pass_confirm_edt)
    EditText passConfirmEdt;
    @BindView(R.id.forgotPassBack)
    ImageView backBtn;

    APIService service;
    String accessToken;
    private Dialog progressDialo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_pass);
        ButterKnife.bind(this);
        progressDialo = new Dialog(this);

        service = APIUtils.getAPIService();
        accessToken = SharePre.getString(SharePre.KEY_ACCESS_TOKEN);

        sendEmailBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email = emailEdt.getText().toString();
                if (email.equals("")) {
                    Toast.makeText(ForgotPassActivity.this, "The email field is required", Toast.LENGTH_SHORT).show();
                } else {
                    Utils.showProgressDialog(progressDialo);
                    sendEmailToGetCode(emailEdt.getText().toString());
                }
            }
        });

        sendCodeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String code = codeEdt.getText().toString();
                String pass = passEdt.getText().toString();
                String passConfirm = passConfirmEdt.getText().toString();
                if (code.equals("")) {
                    Toast.makeText(ForgotPassActivity.this, "The code field is required", Toast.LENGTH_SHORT).show();
                } else if (pass.equals("")) {
                    Toast.makeText(ForgotPassActivity.this, "The password field is required", Toast.LENGTH_SHORT).show();
                } else if (passConfirm.equals("")) {
                    Toast.makeText(ForgotPassActivity.this, "The password confirmation field is required", Toast.LENGTH_SHORT).show();
                } else {
                    if (!pass.equals(passConfirm)) {
                        Toast.makeText(ForgotPassActivity.this, "Passwort stimmt nicht mit der Best tigung cberein.", Toast.LENGTH_SHORT).show();
                    } else {
                        if (pass.length() < 6) {
                            Toast.makeText(ForgotPassActivity.this, "Passwort muss mindestens 6 Zeichen lang sein.", Toast.LENGTH_SHORT).show();
                        } else {
                            Utils.showProgressDialog(progressDialo);
                            sendCode(code, pass, passConfirm);
                        }
                    }
                }
            }
        });

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    private void sendEmailToGetCode(String email) {
        service.sendEmail("Bearer " + accessToken, new ForgotPasswordEnity(email))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<ForgotPassword>() {
                    @Override
                    public void onNext(ForgotPassword forgotPassword) {
                        showAlertDialog(forgotPassword.getData(), true, true);
                        Utils.hideProgressDialog(progressDialo);
                    }

                    @Override
                    public void onError(Throwable t) {
                        ResponseBody body = ((HttpException) t).response().errorBody();
                        Gson gson = new Gson();
                        TypeAdapter<ForgotPasswordErrors> adapter = gson.getAdapter(ForgotPasswordErrors.class);
                        try {
                            ForgotPasswordErrors errorParser = adapter.fromJson(body.string());
                            String msg = errorParser.getErrors().getEmail().get(0);
                            showAlertDialog(msg, true, false);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        Utils.hideProgressDialog(progressDialo);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    private void sendCode(String code, String pass, String rePass) {
        service.updatePassword("Bearer " + accessToken, new ForgotPasswordEnity(code, pass, rePass))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new DisposableObserver<ForgotPassword>() {
                    @Override
                    public void onNext(ForgotPassword forgotPassword) {
                        showAlertDialog(forgotPassword.getData(), false, true);
                        Utils.hideProgressDialog(progressDialo);
                    }

                    @Override
                    public void onError(Throwable t) {
                        ResponseBody body = ((HttpException) t).response().errorBody();
                        Gson gson = new Gson();
                        TypeAdapter<ForgotPassword> adapter = gson.getAdapter(ForgotPassword.class);
                        try {
                            ForgotPassword errorParser = adapter.fromJson(body.string());
                            String msg = errorParser.getMessage();
                            showAlertDialog(msg, true, false);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        Utils.hideProgressDialog(progressDialo);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

//    @Override
//    public void onBackPressed() {
//        Intent intent = new Intent(ForgotPassActivity.this, LoginActivity.class);
//        startActivity(intent);
//    }

    private void showAlertDialog(String msg, final boolean isSendMail, final boolean isSuccess) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(ForgotPassActivity.this);
        builder.setMessage(msg);
        builder.setCancelable(false);
        builder.setNegativeButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (isSendMail) {
                    dialogInterface.dismiss();
                    if (isSuccess) {
                        emailBg.setVisibility(View.GONE);
                        updatePassBg.setVisibility(View.VISIBLE);
                    }
                } else {
                    dialogInterface.dismiss();
                    if (isSuccess) {
                        onBackPressed();
                    }
                }
            }
        });
        builder.show();
    }
}
