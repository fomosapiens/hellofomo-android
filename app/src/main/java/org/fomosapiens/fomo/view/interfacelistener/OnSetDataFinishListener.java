package org.fomosapiens.fomo.view.interfacelistener;

/**
 * Created by PC162 on 8/16/2017.
 */

public interface OnSetDataFinishListener {
    void onLoadFinish();
}
