package org.fomosapiens.fomo.view.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;

import org.fomosapiens.fomo.R;
import org.fomosapiens.fomo.Utils;
import org.fomosapiens.fomo.model.regulation.RegulationListData;
import org.fomosapiens.fomo.view.customview.FontChange;
import org.fomosapiens.fomo.view.ui.regulation.RegulationDetailActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AdapterRegulationList extends RecyclerView.Adapter<AdapterRegulationList.ViewHolder> {

    private List<RegulationListData> listData;
    private Context context;

    public AdapterRegulationList(Context context, List<RegulationListData> listData) {
        this.context = context;
        this.listData = listData;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.refactor_item_regulation_list, parent, false);
        Utils.setFontName(context, (ViewGroup)view);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final RegulationListData data = listData.get(position);

        Spannable sequence;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            sequence = (Spannable) Html.fromHtml(data.getDetails().get(0).getDescription(), Html.FROM_HTML_MODE_LEGACY, null, null);
        } else {
            sequence = (Spannable) Html.fromHtml(data.getDetails().get(0).getDescription(), null, null);
        }
        SpannableStringBuilder strBuilder = new SpannableStringBuilder(sequence);
        if (data.getState().equals("valid")) {
            holder.kindOfRegulation.setVisibility(View.INVISIBLE);
        } else {
            holder.kindOfRegulation.setVisibility(View.VISIBLE);
            holder.kindOfRegulation.setText(context.getString(R.string.state_not_valid));
            holder.kindOfRegulation.setBackgroundColor(ContextCompat.getColor(context, R.color.regulation_not_valid_color_bg));
        }
        holder.title.setText(strBuilder);
        holder.regulationItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Gson gson = new Gson();
                String object = gson.toJson(data);
                Intent intent = new Intent(context, RegulationDetailActivity.class);
                intent.putExtra("regulation_detail_id", object);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return listData.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.regulation_item_kind_result)
        TextView kindOfRegulation;
        @BindView(R.id.regulation_item_title)
        TextView title;
        @BindView(R.id.regulation_item_view_bottom)
        View viewBottom;
        @BindView(R.id.regulation_item)
        LinearLayout regulationItem;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
