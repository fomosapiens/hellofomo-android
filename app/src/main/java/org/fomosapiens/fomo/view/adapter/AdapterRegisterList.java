package org.fomosapiens.fomo.view.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.fomosapiens.fomo.R;
import org.fomosapiens.fomo.Utils;
import org.fomosapiens.fomo.model.regulation.RegulationListElementData;
import org.fomosapiens.fomo.view.ui.RefactorPrimaryBaseActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AdapterRegisterList extends RecyclerView.Adapter<AdapterRegisterList.ViewHolder>{

    private Context context;
    private List<RegulationListElementData> list;
    private ListRegisterClick registerClick;
    private int rowIndex = 0;

    public AdapterRegisterList(Context context, List<RegulationListElementData> listElementData, ListRegisterClick click) {
        this.context = context;
        this.list = listElementData;
        this.registerClick = click;
    }

    @Override
    public AdapterRegisterList.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_header_text, parent, false);
        Utils.setFontName(context, (ViewGroup)view);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final AdapterRegisterList.ViewHolder holder, int position) {
        final RegulationListElementData data = list.get(position);
        holder.register.setText(data.getName());
        holder.register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (((RefactorPrimaryBaseActivity)context).isOnline()) {
                    rowIndex = holder.getAdapterPosition();
                    registerClick.itemClick(data.getId());
                    notifyDataSetChanged();
                }
            }
        });

        if (rowIndex == position) {
            holder.register.setBackground(ContextCompat.getDrawable(context, R.drawable.custom_black_btn_bg));
            holder.register.setTextColor(ContextCompat.getColor(context, R.color.color_white));
        } else {
            holder.register.setBackground(ContextCompat.getDrawable(context, android.R.color.transparent));
            holder.register.setTextColor(ContextCompat.getColor(context, R.color.color_black));
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.item_register_tv)
        TextView register;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface ListRegisterClick {
        void itemClick(String registerId);
    }
}
