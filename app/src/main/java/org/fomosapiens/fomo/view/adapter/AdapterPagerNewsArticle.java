package org.fomosapiens.fomo.view.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import org.fomosapiens.fomo.model.Category;
import org.fomosapiens.fomo.view.fragment.FragmentNewsArticleItem;

import java.util.List;

/**
 * Created by PC162 on 8/15/2017.
 */

public class AdapterPagerNewsArticle extends FragmentPagerAdapter {

    private List<Category> categoryList;
    private List<FragmentNewsArticleItem> fragmentNewsArticleItems;

    public AdapterPagerNewsArticle(FragmentManager fm, List<Category> categoryList, List<FragmentNewsArticleItem> fragmentNewsArticleItems) {
        super(fm);

        this.categoryList = categoryList;
        this.fragmentNewsArticleItems = fragmentNewsArticleItems;
    }

    @Override
    public CharSequence getPageTitle(int position) {

        return categoryList.get(position).getName();
    }

    @Override
    public Fragment getItem(int position) {

        return fragmentNewsArticleItems.get(position);
    }

    @Override
    public int getCount() {
        return fragmentNewsArticleItems.size();
    }
}
