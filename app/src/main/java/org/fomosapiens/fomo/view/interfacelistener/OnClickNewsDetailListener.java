package org.fomosapiens.fomo.view.interfacelistener;

import org.fomosapiens.fomo.model.News;

/**
 * Created by PC162 on 8/14/2017.
 */

public interface OnClickNewsDetailListener {
    void onClickItem(News news);
}
