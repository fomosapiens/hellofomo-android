package org.fomosapiens.fomo.view.ui.dashboard;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import org.fomosapiens.fomo.R;
import org.fomosapiens.fomo.Utils;
import org.fomosapiens.fomo.model.DashBoard;
import org.fomosapiens.fomo.model.News;
import org.fomosapiens.fomo.net.APIHelper;
import org.fomosapiens.fomo.view.adapter.AdapterDasBoardItem;
import org.fomosapiens.fomo.view.interfacelistener.OnClickNewsDetailListener;
import org.fomosapiens.fomo.view.interfacelistener.OnResponseGetDashBoardListener;
import org.fomosapiens.fomo.view.interfacelistener.OnResponseGetListNewsListener;
import org.fomosapiens.fomo.view.interfacelistener.OnSetDataFinishListener;
import org.fomosapiens.fomo.view.ui.PrimaryBaseActivity;
import org.fomosapiens.fomo.view.ui.newsdetail.NewsDetailActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;

/**
 * Created by PC162 on 7/6/2017.
 */

public class DashBoardActivity extends PrimaryBaseActivity implements OnClickNewsDetailListener, SwipeRefreshLayout.OnRefreshListener, OnSetDataFinishListener {

    public static String TAG = "org.fomosapiens.fomo.view.ui.dashboard.DashBoardActivity";

    protected SwipeRefreshLayout adbSwipeContainer;
    protected RecyclerView adbRecyclerView;
    protected View rootView;

    private List<DashBoard> dashBoards;
    private int requestCount;
    private int size = 0;
    private Dialog progressDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        rootView = getLayoutInflater().inflate(R.layout.activity_dash_board, getContainerView());
        progressDialog = new Dialog(DashBoardActivity.this);
        setToolBarTitle();
        initLayout();
        getListDashboard();
    }

    @Override
    protected String getActivityName() {
        return TAG;
    }

    protected void setToolBarTitle() {
        abTxtToolBarTitle.setText(getResources().getString(R.string.dashboard));
    }

    private void initLayout() {
        adbRecyclerView = ButterKnife.findById(rootView, R.id.adbRecyclerView);
        adbSwipeContainer = ButterKnife.findById(rootView, R.id.adbSwipeContainer);
        adbSwipeContainer.setOnRefreshListener(this);
    }

    private void setAdapterForRecycler(List<DashBoard> dashBoards){
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        AdapterDasBoardItem adapterDasBoardItem = new AdapterDasBoardItem(dashBoards, this, this, this);
        adbRecyclerView.setLayoutManager(mLayoutManager);
        adbRecyclerView.setAdapter(adapterDasBoardItem);
    }

    private void goToNewsDetailActivity(News news) {
        Intent intent = new Intent(this, NewsDetailActivity.class);
        intent.putExtra(Utils.KEY_NEWS_ID, news.getId());
        startActivity(intent);
    }

    private void dismissProgressAndRefresh(){
        if (adbSwipeContainer.isRefreshing())
            adbSwipeContainer.setRefreshing(false);
        Utils.hideProgressDialog(progressDialog);
    }

    // TODO: OnClickNewsDetailListener
    @Override
    public void onClickItem(News news) {
        if (isOnline())
            goToNewsDetailActivity(news);
    }

    // TODO: OnRefreshListener
    @Override
    public void onRefresh() {
        if (!isOnline())
            return;

        getListDashboard();
    }

    // TODO: OnSetDataFinishListener
    @Override
    public void onLoadFinish() {
        Utils.hideProgressDialog(progressDialog);
    }

    private void getListDashboard() {
        if (!isOnline())
            return;
        if (!adbSwipeContainer.isRefreshing())
            Utils.showProgressDialog(progressDialog);
        requestCount = 0;
        APIHelper.getListDashboard(getResponseGetDashBoardListener());
    }

    private OnResponseGetDashBoardListener getResponseGetDashBoardListener() {
        OnResponseGetDashBoardListener dashBoardListener = new OnResponseGetDashBoardListener() {
            @Override
            public void OnRequestSuccess(List<DashBoard> mDashBoards) {
                if (mDashBoards==null || mDashBoards.size() == 0) {
                    dismissProgressAndRefresh();
                    return;
                }

                size = mDashBoards.size();
                dashBoards = mDashBoards;
                int position = 0;
                for (DashBoard dashBoard : mDashBoards){
                    getListNewsWithCategoryOrTag(dashBoard, position);
                    position++;
                }
            }

            @Override
            public void OnRequestError(Throwable e) {
                dismissProgressAndRefresh();
            }
        };
        return dashBoardListener;
    }


    private void getListNewsWithCategoryOrTag(DashBoard dashBoard, int position) {
        APIHelper.getListNewsWithCategoryOrTag(dashBoard, getOnResponseListNews(position));
    }

    private OnResponseGetListNewsListener getOnResponseListNews(final int position) {
        OnResponseGetListNewsListener responseGetListNewsListener = new OnResponseGetListNewsListener() {
            @Override
            public void OnRequestSuccess(final List<News> newsList, final DashBoard dashBoard) {
                requestCount++;
                dashBoards.get(position).setNewsList(newsList);

                if (requestCount == size)
                    setAdapterForRecycler(dashBoards);
            }

            @Override
            public void OnRequestError(Throwable e) {
                dismissProgressAndRefresh();
            }
        };
        return responseGetListNewsListener;
    }
}
