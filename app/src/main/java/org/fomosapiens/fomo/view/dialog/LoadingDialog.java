package org.fomosapiens.fomo.view.dialog;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.github.rahatarmanahmed.cpv.CircularProgressView;

import org.fomosapiens.fomo.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by PC162 on 8/14/2017.
 */

public class LoadingDialog extends DialogFragment {

    @BindView(R.id.dpgProgressView)
    protected CircularProgressView dpgProgressView;

    public static LoadingDialog newInstance() {
        LoadingDialog f = new LoadingDialog();

        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = new Dialog(getActivity(), R.style.LoadingDialogTheme);
        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN);
        return dialog;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_fragment_loading, null);
        ButterKnife.bind(this, view);
        dpgProgressView.startAnimation();
        return view;
    }
}
