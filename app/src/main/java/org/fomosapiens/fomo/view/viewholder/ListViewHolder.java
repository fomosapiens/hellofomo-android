package org.fomosapiens.fomo.view.viewholder;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.fomosapiens.fomo.R;
import org.fomosapiens.fomo.Utils;
import org.fomosapiens.fomo.model.DashBoard;
import org.fomosapiens.fomo.model.News;
import org.fomosapiens.fomo.view.customview.FontChange;
import org.fomosapiens.fomo.view.customview.ViewListItem;
import org.fomosapiens.fomo.view.interfacelistener.OnClickNewsDetailListener;
import org.fomosapiens.fomo.view.interfacelistener.OnSetDataFinishListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by PC162 on 9/6/2017.
 */

public class ListViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.adtlViewTopLine)
    protected View adtlViewTopLine;

    @BindView(R.id.adtlTxtHeader)
    protected TextView adtlTxtHeader;

    @BindView(R.id.adtlLinearContainer)
    protected LinearLayout adtlLinearContainer;

    @BindView(R.id.adtlView)
    protected LinearLayout adtlView;

    private OnClickNewsDetailListener onClickNewsDetailListener;
    private OnSetDataFinishListener onSetDataFinishListener;
    private Context context;

    public ListViewHolder(View itemView, Context context, OnClickNewsDetailListener onClickNewsDetailListener, OnSetDataFinishListener onSetDataFinishListener) {
        super(itemView);
        this.onClickNewsDetailListener = onClickNewsDetailListener;
        this.onSetDataFinishListener = onSetDataFinishListener;
        this.context = context;
        ButterKnife.bind(this, itemView);
        Utils.setFontName(context, (ViewGroup)itemView);
    }

    public void setDashboard(List<DashBoard> dashboards, int position){
        if (position == dashboards.size() - 1)
            adtlViewTopLine.setVisibility(View.GONE);
        else
            adtlViewTopLine.setVisibility(View.VISIBLE);

        createListItemPart(dashboards.get(position));
    }

    private void createListItemPart(DashBoard dashBoard) {

        List<News> newsList = dashBoard.getNewsList();

        if (newsList == null || newsList.size()==0) {
            adtlView.setVisibility(View.GONE);
            adtlView.setLayoutParams(new RecyclerView.LayoutParams(0, 0));
            onSetDataFinishListener.onLoadFinish();
            return;
        } else {
            String sectionDescription = dashBoard.getSection_description();
            boolean isShowPicture = Utils.getIsShowPicture(dashBoard.getShow_picture());
            adtlTxtHeader.setText(sectionDescription);
            adtlView.setVisibility(View.VISIBLE);
            adtlLinearContainer.setVisibility(View.VISIBLE);
            adtlLinearContainer.removeAllViews();
            int listPosition = 0;
            int size = newsList.size() - 1;
            for (News news : newsList) {
                ViewListItem listItem;
                if (listPosition == size)
                    listItem = new ViewListItem(context, news, onClickNewsDetailListener, isShowPicture, true);
                else
                    listItem = new ViewListItem(context, news, onClickNewsDetailListener, isShowPicture, false);

                adtlLinearContainer.addView(listItem);
                listPosition++;
            }
        }
        onSetDataFinishListener.onLoadFinish();

    }
}
