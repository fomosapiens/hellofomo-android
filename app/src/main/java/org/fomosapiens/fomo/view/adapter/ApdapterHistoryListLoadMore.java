package org.fomosapiens.fomo.view.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;

import org.fomosapiens.fomo.R;
import org.fomosapiens.fomo.Utils;
import org.fomosapiens.fomo.model.event.HistoryData;
import org.fomosapiens.fomo.view.customview.FontChange;
import org.fomosapiens.fomo.view.ui.regulation.RegulationDetailActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ApdapterHistoryListLoadMore extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int ITEM = 0;
    private static final int LOADING = 1;
    private boolean isLoadingAdded = false;

    private Context context;
    private List<HistoryData> listResponses;

    public ApdapterHistoryListLoadMore(Context context) {
        this.context = context;
        listResponses = new ArrayList<>();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());


        switch (viewType) {
            case ITEM:
                viewHolder = getViewHolder(parent, inflater);
                break;

            case LOADING:
                View v2 = inflater.inflate(R.layout.item_progress, parent, false);
                viewHolder = new LoadingVH(v2);
        }
        return viewHolder;
    }

    @NonNull
    private RecyclerView.ViewHolder getViewHolder(ViewGroup parent, LayoutInflater inflater) {
        RecyclerView.ViewHolder viewHolder;
        View v1 = inflater.inflate(R.layout.refactor_item_event, parent, false);
        viewHolder = new RegulationVH(v1);
        Utils.setFontName(context, parent);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        final HistoryData data = listResponses.get(position);

        switch (getItemViewType(position)) {
            case ITEM:
                final RegulationVH regulationVH = (RegulationVH) holder;

                regulationVH.topDate.setText(context.getString(R.string.change_from) + " " + data.getDate());
                if (data.getRegulation() != null) {
                    setTextFromHtml(data.getRegulation().getDetails().get(0).getDescription(), regulationVH.topTitle);
                }
                if (data.getDescription() != null) {
                    setTextFromHtml(data.getDescription(), regulationVH.contentTv);
                }
                regulationVH.itemClickArea.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (regulationVH.expandBtn.getText().toString().equals("+")) {
                            regulationVH.contentLayout.setVisibility(View.VISIBLE);
                            regulationVH.expandBtn.setText("-");
                        } else if (regulationVH.expandBtn.getText().toString().equals("-")) {
                            regulationVH.contentLayout.setVisibility(View.GONE);
                            regulationVH.expandBtn.setText("+");
                        }
                    }
                });
                regulationVH.contentLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Gson gson = new Gson();
                        Intent intent = new Intent(context, RegulationDetailActivity.class);
                        intent.putExtra("regulation_id", data.getRegulation().getId());
                        context.startActivity(intent);
                    }
                });
                break;

            case LOADING:
                break;

        }

    }

    @Override
    public int getItemCount() {
        return listResponses == null ? 0 : listResponses.size();
    }

    @Override
    public int getItemViewType(int position) {
        return (position == listResponses.size() - 1 && isLoadingAdded) ? LOADING : ITEM;
    }

    /*
        Helper
    */
    public void add(HistoryData data) {
        listResponses.add(data);
        notifyItemInserted(listResponses.size() - 1);
    }

    public void addAll(List<HistoryData> listData) {
        for (HistoryData data : listData) {
            add(data);
        }
    }

    public void removeAll() {
        listResponses = new ArrayList<>();
        notifyDataSetChanged();
    }

    public void addLoadingFooter() {
        isLoadingAdded = true;
        add(new HistoryData());
    }

    public void removeLoadingFooter() {
        isLoadingAdded = false;

        int position = listResponses.size() - 1;
        HistoryData data = getItem(position);

        if (data != null) {
            listResponses.remove(position);
            notifyItemRemoved(position);
        }
    }

    public HistoryData getItem(int position) { return listResponses.get(position); }

    protected class RegulationVH extends RecyclerView.ViewHolder {

        @BindView(R.id.item_event_content_layout)
        ConstraintLayout contentLayout;
        @BindView(R.id.item_event_top_layout)
        ConstraintLayout itemClickArea;
        @BindView(R.id.item_event_expand)
        TextView expandBtn;
        @BindView(R.id.item_event_content_tv)
        TextView contentTv;
        @BindView(R.id.item_event_top_title)
        TextView topTitle;
        @BindView(R.id.item_event_top_date)
        TextView topDate;


        public RegulationVH(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            this.setIsRecyclable(false);
        }
    }

    protected class LoadingVH extends RecyclerView.ViewHolder {
        public LoadingVH(View itemView) {super(itemView);}
    }

    private void setTextFromHtml(String content, TextView textView) {
        Spannable sequence;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            sequence = (Spannable) Html.fromHtml(content, Html.FROM_HTML_MODE_LEGACY, null, null);
        } else {
            sequence = (Spannable) Html.fromHtml(content, null, null);
        }
        SpannableStringBuilder strBuilder = new SpannableStringBuilder(sequence);
        textView.setText(strBuilder);
    }
}
