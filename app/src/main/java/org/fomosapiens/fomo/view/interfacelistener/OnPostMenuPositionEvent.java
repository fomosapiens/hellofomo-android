package org.fomosapiens.fomo.view.interfacelistener;

/**
 * Created by PC162 on 8/28/2017.
 */

public class OnPostMenuPositionEvent {
    private int position;

    public  OnPostMenuPositionEvent(int position){
        this.position = position;
    }

    public int getPosition() {
        return position;
    }
}
