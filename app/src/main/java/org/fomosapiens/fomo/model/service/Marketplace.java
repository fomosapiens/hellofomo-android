package org.fomosapiens.fomo.model.service;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Marketplace {

    @SerializedName("show_in_marketplace")
    @Expose
    private Integer showInMarketplace;
    @SerializedName("title_image")
    @Expose
    private String titleImage;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("product")
    @Expose
    private String product;
    @SerializedName("copyright")
    @Expose
    private String copyright;

    public Integer getShowInMarketplace() {
        return showInMarketplace;
    }

    public void setShowInMarketplace(Integer showInMarketplace) {
        this.showInMarketplace = showInMarketplace;
    }

    public String getTitleImage() {
        return titleImage;
    }

    public void setTitleImage(String titleImage) {
        this.titleImage = titleImage;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getCopyright() {
        return copyright;
    }

    public void setCopyright(String copyright) {
        this.copyright = copyright;
    }
}
