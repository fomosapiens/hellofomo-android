package org.fomosapiens.fomo.model.regulation;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RegulationListData {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("content")
    @Expose
    private String content;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("regulation_date")
    @Expose
    private String regulationDate;
    @SerializedName("details")
    @Expose
    private List<RegulationListDetail> details = null;
    @SerializedName("histories")
    @Expose
    private List<RegulationListHistory> histories = null;
    @SerializedName("registers")
    @Expose
    private List<RegulationListElementData> registers;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("master_data")
    @Expose
    private MasterDataInDetailResponse masterDataInDetailResponse;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }


    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getRegulationDate() {
        return regulationDate;
    }

    public void setRegulationDate(String regulationDate) {
        this.regulationDate = regulationDate;
    }

    public List<RegulationListDetail> getDetails() {
        return details;
    }

    public void setDetails(List<RegulationListDetail> details) {
        this.details = details;
    }

    public List<RegulationListHistory> getHistories() {
        return histories;
    }

    public void setHistories(List<RegulationListHistory> histories) {
        this.histories = histories;
    }


    public List<RegulationListElementData> getRegisters() {
        return registers;
    }

    public void setRegisters(List<RegulationListElementData> registers) {
        this.registers = registers;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public MasterDataInDetailResponse getMasterDataInDetailResponse() {
        return masterDataInDetailResponse;
    }

    public void setMasterDataInDetailResponse(MasterDataInDetailResponse masterDataInDetailResponse) {
        this.masterDataInDetailResponse = masterDataInDetailResponse;
    }
}
