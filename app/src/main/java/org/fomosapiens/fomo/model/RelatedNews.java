package org.fomosapiens.fomo.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by PC162 on 8/8/2017.
 */

public class RelatedNews {

    @Expose
    @SerializedName("id")
    protected long id;

    @Expose
    @SerializedName("title")
    protected String title;

    @Expose
    @SerializedName("thumbnail")
    protected String thumbnail;

    public long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getThumbnail() {
        return thumbnail;
    }
}
