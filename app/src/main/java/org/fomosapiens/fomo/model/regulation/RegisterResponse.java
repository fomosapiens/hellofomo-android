package org.fomosapiens.fomo.model.regulation;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RegisterResponse {

    @SerializedName("data")
    @Expose
    private List<RegisterData> data = null;
    @SerializedName("has_more")
    @Expose
    private Boolean hasMore;
    @SerializedName("total")
    @Expose
    private Integer total;

    public List<RegisterData> getData() {
        return data;
    }

    public void setData(List<RegisterData> data) {
        this.data = data;
    }

}
