package org.fomosapiens.fomo.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by PC162 on 8/8/2017.
 */

public class News {

    @Expose
    @SerializedName("id")
    protected long id;

    @Expose
    @SerializedName("title")
    protected String title;

    @Expose
    @SerializedName("introduction")
    protected String introduction;

    @Expose
    @SerializedName("description")
    protected String description;

    @Expose
    @SerializedName("available_date")
    protected String available_date;

    @Expose
    @SerializedName("primary_image")
    protected String primary_image;

    protected String type;

    public long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getAvailable_date() {
        return available_date;
    }

    public String getPrimary_image() {
        return primary_image;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public News(String type) {
        this.type = type;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setAvailable_date(String available_date) {
        this.available_date = available_date;
    }

    public void setPrimary_image(String primary_image) {
        this.primary_image = primary_image;
    }

    public News() {
    }
}
