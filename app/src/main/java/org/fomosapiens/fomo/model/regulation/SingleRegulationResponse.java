package org.fomosapiens.fomo.model.regulation;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SingleRegulationResponse {

    @SerializedName("data")
    @Expose
    private RegulationListData data;

    public RegulationListData getData() {
        return data;
    }

    public void setData(RegulationListData data) {
        this.data = data;
    }


}
