package org.fomosapiens.fomo.model.event;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.fomosapiens.fomo.model.regulation.RegulationListData;

public class HistoryData {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("show_in_updates")
    @Expose
    private Boolean showInUpdates;
    @SerializedName("regulation")
    @Expose
    private RegulationListData regulation;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Boolean getShowInUpdates() {
        return showInUpdates;
    }

    public void setShowInUpdates(Boolean showInUpdates) {
        this.showInUpdates = showInUpdates;
    }

    public RegulationListData getRegulation() {
        return regulation;
    }

    public void setRegulation(RegulationListData regulation) {
        this.regulation = regulation;
    }

}
