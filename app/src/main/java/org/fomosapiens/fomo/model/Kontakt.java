package org.fomosapiens.fomo.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


/**
 * Created by PC162 on 8/31/2017.
 */

public class Kontakt {

    @Expose
    @SerializedName("errors")
    protected boolean errors;

    @Expose
    @SerializedName("data")
    protected String data;

    public String getData() {
        return data;
    }
}
