package org.fomosapiens.fomo.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by PC162 on 8/8/2017.
 */

public class Tag {

    @Expose
    @SerializedName("id")
    protected int id;

    @Expose
    @SerializedName("name")
    protected String name;
}
