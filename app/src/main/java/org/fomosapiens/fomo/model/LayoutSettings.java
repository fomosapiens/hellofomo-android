package org.fomosapiens.fomo.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LayoutSettings {

    @SerializedName("key")
    @Expose
    private String key;
    @SerializedName("value")
    @Expose
    private int value;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}
