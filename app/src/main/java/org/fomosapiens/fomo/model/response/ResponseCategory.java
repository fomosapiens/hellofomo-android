package org.fomosapiens.fomo.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.fomosapiens.fomo.model.Category;

import java.util.List;

/**
 * Created by PC162 on 8/8/2017.
 */

public class ResponseCategory {

    @Expose
    @SerializedName("errors")
    protected boolean errors;

    @Expose
    @SerializedName("data")
    protected List<Category> data;

    public List<Category> getData() {
        return data;
    }
}
