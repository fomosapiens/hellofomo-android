package org.fomosapiens.fomo.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by HuyMTB on 7/19/18.
 */

public class ResponseFont {

    @SerializedName("errors")
    @Expose
    private Boolean errors;
    @SerializedName("data")
    @Expose
    private String data;

    public Boolean getErrors() {
        return errors;
    }

    public void setErrors(Boolean errors) {
        this.errors = errors;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
