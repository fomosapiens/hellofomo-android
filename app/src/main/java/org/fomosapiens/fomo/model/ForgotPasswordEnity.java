package org.fomosapiens.fomo.model;

import org.fomosapiens.fomo.view.ui.authen.ForgotPassActivity;

public class ForgotPasswordEnity {

    private String email;
    private String code;
    private String password;
    private String password_confirmation;

    public ForgotPasswordEnity(String email) {
        this.email = email;
    }

    public ForgotPasswordEnity(String code, String password, String password_confirmation) {
        this.code = code;
        this.password = password;
        this.password_confirmation = password_confirmation;
    }
}
