package org.fomosapiens.fomo.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.fomosapiens.fomo.model.DashBoard;

import java.util.List;

/**
 * Created by PC162 on 8/8/2017.
 */

public class ResponseDashboard {
    @Expose
    @SerializedName("errors")
    protected boolean errors;

    @Expose
    @SerializedName("data")
    protected List<DashBoard> data;

    public List<DashBoard> getData() {
        return data;
    }
}
