package org.fomosapiens.fomo.model;

/**
 * Created by HuyMTB on 7/10/18.
 */

public class SubMenuItem {

    private String title;
    private boolean isExpand = true;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isExpand() {
        return isExpand;
    }

    public void setExpand(boolean expand) {
        isExpand = expand;
    }

    public SubMenuItem(String title) {
        this.title = title;
    }
}
