package org.fomosapiens.fomo.model.regulation;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MasterDataResponse {

    @SerializedName("data")
    @Expose
    private MasterData data;
    @SerializedName("errors")
    @Expose
    private Boolean errors;

    public MasterData getData() {
        return data;
    }

    public void setData(MasterData data) {
        this.data = data;
    }

    public Boolean getErrors() {
        return errors;
    }

    public void setErrors(Boolean errors) {
        this.errors = errors;
    }
}
