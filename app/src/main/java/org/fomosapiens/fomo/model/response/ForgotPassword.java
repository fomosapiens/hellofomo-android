package org.fomosapiens.fomo.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ForgotPassword {

    @Expose
    @SerializedName("errors")
    protected boolean errors;

    @Expose
    @SerializedName("data")
    protected String data;

    @Expose
    @SerializedName("message")
    protected String message;

    public boolean isError() {
        return errors;
    }

    public String getData() {
        return data;
    }

    public String getMessage() {
        return message;
    }
}
