package org.fomosapiens.fomo.model.service;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FilterLetter {

    @SerializedName("letter")
    @Expose
    private String letter;
    @SerializedName("total")
    @Expose
    private Integer total;

    public String getLetter() {
        return letter;
    }

    public void setLetter(String letter) {
        this.letter = letter;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

}
