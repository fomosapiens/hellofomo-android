package org.fomosapiens.fomo.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by PC162 on 8/8/2017.
 */

public class DashBoard {
    @Expose
    @SerializedName("section_name")
    protected String section_name;

    @Expose
    @SerializedName("section_description")
    protected String section_description;

    @Expose
    @SerializedName("section_type")
    protected String section_type;

    @Expose
    @SerializedName("layout")
    protected String layout;

    @Expose
    @SerializedName("show_picture")
    protected String show_picture;

    @Expose
    @SerializedName("type")
    protected String type;

    @Expose
    @SerializedName("type_value")
    protected String type_value;

    @Expose
    @SerializedName("data_count")
    protected String data_count;

    @Expose
    @SerializedName("data_offset")
    protected String data_offset;

    @Expose
    @SerializedName("ordering")
    protected String ordering;

    @Expose
    @SerializedName("top_news")
    protected String top_news;

    protected String category;

    protected List<News> newsList;

    public List<News> getNewsList() {
        return newsList;
    }

    public void setNewsList(List<News> newsList) {
        this.newsList = newsList;
    }

    public String getLayout() {
        return layout;
    }

    public String getSection_name() {
        return section_name;
    }

    public String getType() {
        return type;
    }

    public String getType_value() {
        return type_value;
    }

    public String getData_count() {
        return data_count;
    }

    public String getData_offset() {
        return data_offset;
    }

    public String getSection_description() {
        return section_description;
    }

    public String getShow_picture() {
        return show_picture;
    }

    public void setSection_name(String section_name) {
        this.section_name = section_name;
    }

    public void setSection_description(String section_description) {
        this.section_description = section_description;
    }

    public void setSection_type(String section_type) {
        this.section_type = section_type;
    }

    public void setLayout(String layout) {
        this.layout = layout;
    }

    public void setShow_picture(String show_picture) {
        this.show_picture = show_picture;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setType_value(String type_value) {
        this.type_value = type_value;
    }

    public void setData_count(String data_count) {
        this.data_count = data_count;
    }

    public void setData_offset(String data_offset) {
        this.data_offset = data_offset;
    }

    public void setOrdering(String ordering) {
        this.ordering = ordering;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getTop_news() {
        return top_news;
    }

    public void setTop_news(String top_news) {
        this.top_news = top_news;
    }
}


