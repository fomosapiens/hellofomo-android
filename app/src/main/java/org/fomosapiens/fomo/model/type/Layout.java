package org.fomosapiens.fomo.model.type;

/**
 * Created by PC162 on 8/14/2017.
 */

public enum Layout {

    SLIDER(0,"slider"),
    SINGLE(1,"single"),
    LIST(2,"list");

    private String type;
    private int key;

    private Layout(int key, String type) {
        this.type = type;
        this.key = key;
    }




    public static Layout getLayout(String mType) {
        for (Layout layout : Layout.values()) {
            if (layout.type.equals(mType)) {
                return layout;
            }
        }
        return null;
    }
}
