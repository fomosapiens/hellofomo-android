package org.fomosapiens.fomo.model.service;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Directory {

    @SerializedName("show_in_directory")
    @Expose
    private Integer showInDirectory;
    @SerializedName("title_image")
    @Expose
    private String titleImage;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("copyright")
    @Expose
    private String copyright;

    public Integer getShowInDirectory() {
        return showInDirectory;
    }

    public void setShowInDirectory(Integer showInDirectory) {
        this.showInDirectory = showInDirectory;
    }

    public String getTitleImage() {
        return titleImage;
    }

    public void setTitleImage(String titleImage) {
        this.titleImage = titleImage;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCopyright() {
        return copyright;
    }

    public void setCopyright(String copyright) {
        this.copyright = copyright;
    }
}
