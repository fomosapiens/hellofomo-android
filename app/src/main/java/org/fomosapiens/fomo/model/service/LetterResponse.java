package org.fomosapiens.fomo.model.service;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class LetterResponse {

    @SerializedName("data")
    @Expose
    private List<FilterLetter> data = null;
    @SerializedName("errors")
    @Expose
    private Boolean errors;

    public List<FilterLetter> getData() {
        return data;
    }

    public void setData(List<FilterLetter> data) {
        this.data = data;
    }

    public Boolean getErrors() {
        return errors;
    }

    public void setErrors(Boolean errors) {
        this.errors = errors;
    }
}
