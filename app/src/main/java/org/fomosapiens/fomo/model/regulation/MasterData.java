package org.fomosapiens.fomo.model.regulation;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MasterData {

    @SerializedName("specification")
    @Expose
    private List<RegulationListElementData> specifications = null;
    @SerializedName("field_of_laws")
    @Expose
    private List<RegulationListElementData> fieldOfLaws = null;
    @SerializedName("levels")
    @Expose
    private List<RegulationListElementData> levels = null;
    @SerializedName("legals")
    @Expose
    private List<RegulationListElementData> legals = null;
    @SerializedName("authority")
    @Expose
    private List<RegulationListElementData> authorities = null;


    public List<RegulationListElementData> getSpecifications() {
        return specifications;
    }

    public void setSpecifications(List<RegulationListElementData> specifications) {
        this.specifications = specifications;
    }

    public List<RegulationListElementData> getFieldOfLaws() {
        return fieldOfLaws;
    }

    public void setFieldOfLaws(List<RegulationListElementData> fieldOfLaws) {
        this.fieldOfLaws = fieldOfLaws;
    }

    public List<RegulationListElementData> getLevels() {
        return levels;
    }

    public void setLevels(List<RegulationListElementData> levels) {
        this.levels = levels;
    }

    public List<RegulationListElementData> getLegals() {
        return legals;
    }

    public void setLegals(List<RegulationListElementData> legals) {
        this.legals = legals;
    }

    public List<RegulationListElementData> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(List<RegulationListElementData> authorities) {
        this.authorities = authorities;
    }
}
