package org.fomosapiens.fomo.model.regulation;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TagData {

    @SerializedName("register_id")
    @Expose
    private Integer registerId;
    @SerializedName("value")
    @Expose
    private List<String> value = null;

    public Integer getRegisterId() {
        return registerId;
    }

    public void setRegisterId(Integer registerId) {
        this.registerId = registerId;
    }

    public List<String> getValue() {
        return value;
    }

    public void setValue(List<String> value) {
        this.value = value;
    }

}
