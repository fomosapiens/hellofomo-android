package org.fomosapiens.fomo.model.regulation;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GeneralResponse {
    @SerializedName("errors")
    @Expose
    private Boolean errors;
    @SerializedName("data")
    @Expose
    private GeneralData data;

    public Boolean getErrors() {
        return errors;
    }

    public void setErrors(Boolean errors) {
        this.errors = errors;
    }

    public GeneralData getData() {
        return data;
    }

    public void setData(GeneralData data) {
        this.data = data;
    }
}
