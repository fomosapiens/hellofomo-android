package org.fomosapiens.fomo.model.regulation;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GeneralData {

    @SerializedName("font")
    @Expose
    private String font;
    @SerializedName("force_customer_login")
    @Expose
    private Integer forceCustomerLogin;

    public String getFont() {
        return font;
    }

    public void setFont(String font) {
        this.font = font;
    }

    public Integer getForceCustomerLogin() {
        return forceCustomerLogin;
    }

    public void setForceCustomerLogin(Integer forceCustomerLogin) {
        this.forceCustomerLogin = forceCustomerLogin;
    }

}
