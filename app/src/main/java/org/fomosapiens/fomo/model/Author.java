package org.fomosapiens.fomo.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by PC162 on 8/16/2017.
 */

public class Author {
    @Expose
    @SerializedName("id")
    protected int id;

    @Expose
    @SerializedName("name")
    protected String name;

    @SerializedName("callback_url")
    @Expose
    private String callbackUrl;

    @SerializedName("primary_image")
    @Expose
    private String primaryImage;

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCallbackUrl() {
        return callbackUrl;
    }

    public void setCallbackUrl(String callbackUrl) {
        this.callbackUrl = callbackUrl;
    }

    public String getPrimaryImage() {
        return primaryImage;
    }
}
