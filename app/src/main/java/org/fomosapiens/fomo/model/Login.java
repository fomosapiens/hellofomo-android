package org.fomosapiens.fomo.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by PC162 on 8/7/2017.
 */

public class Login {
    @Expose
    @SerializedName("token_type")
    protected String token_type;

    @Expose
    @SerializedName("expires_in")
    protected long expires_in;

    @Expose
    @SerializedName("access_token")
    protected String access_token;

    @Expose
    @SerializedName("refresh_token")
    protected String refresh_token;

    @Expose
    @SerializedName("errors")
    protected boolean errors;

    @Expose
    @SerializedName("code")
    protected long code;

    @Expose
    @SerializedName("message")
    protected String message;

    @SerializedName("sub_plans")
    @Expose
    private List<Integer> subPlans = null;
    @SerializedName("is_legally_register")
    @Expose
    private Integer isLegallyRegister;


    public String getAccess_token() {
        return access_token;
    }


    public String getRefresh_token() {
        return refresh_token;
    }

    public String getMessage() {return message;}

    public boolean isErrors() {
        return errors;
    }

    public List<Integer> getSubPlans() {
        return subPlans;
    }

    public void setSubPlans(List<Integer> subPlans) {
        this.subPlans = subPlans;
    }

    public Integer getIsLegallyRegister() {
        return isLegallyRegister;
    }

    public void setIsLegallyRegister(Integer isLegallyRegister) {
        this.isLegallyRegister = isLegallyRegister;
    }
}
