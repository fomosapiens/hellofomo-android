package org.fomosapiens.fomo.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by PC162 on 8/11/2017.
 */

public class NewsDetail {
    @Expose
    @SerializedName("id")
    protected long id;

    @Expose
    @SerializedName("title")
    protected String title;

    @Expose
    @SerializedName("introduction")
    protected String introduction;

    @Expose
    @SerializedName("description")
    protected String description;

    @Expose
    @SerializedName("available_date")
    protected String available_date;
    @Expose
    @SerializedName("primary_image")
    protected String primary_image;

    @Expose
    @SerializedName("copyright_primary_image")
    protected String copyright_primary_image;

    @Expose
    @SerializedName("tags")
    protected List<Tag> tags;

    @Expose
    @SerializedName("categories")
    protected List<Category> categories;

    @Expose
    @SerializedName("authors")
    protected List<Author> authors;

    @Expose
    @SerializedName("galleries")
    protected List<String> galleries;

    @Expose
    @SerializedName("attachments")
    protected List<String> attachments;

    @Expose
    @SerializedName("related_news")
    protected List<RelatedNews> related_news;

    @SerializedName("layout_settings")
    @Expose
    protected List<LayoutSettings> layoutSettings;

    @SerializedName("url")
    @Expose
    protected String url;

    @SerializedName("meta_title")
    @Expose
    protected String meta_title;

    @SerializedName("meta_permalink")
    @Expose
    protected String meta_permalink;

    @SerializedName("meta_description")
    @Expose
    protected String meta_description;

    @SerializedName("share_information")
    @Expose
    protected ShareInformation shareInformation;

    public String getTitle() {
        return title;
    }

    public String getIntroduction() {
        return introduction;
    }

    public String getDescription() {
        return description;
    }

    public String getAvailable_date() {
        return available_date;
    }

    public String getPrimary_image() {
        return primary_image;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public List<Author> getAuthors() {
        return authors;
    }

    public List<String> getGalleries() {
        return galleries;
    }

    public List<String> getAttachments() {
        return attachments;
    }

    public List<RelatedNews> getRelated_news() {
        return related_news;
    }

    public long getId() {
        return id;
    }

    public List<LayoutSettings> getLayoutSettings() {
        return layoutSettings;
    }

    public String getUrl() {
        return url;
    }

    public String getMeta_title() {
        return meta_title;
    }

    public String getMeta_permalink() {
        return meta_permalink;
    }

    public String getMeta_description() {
        return meta_description;
    }

    public ShareInformation getShareInformation() {
        return shareInformation;
    }

    public String getCopyright_primary_image() {
        return copyright_primary_image;
    }

    public void setCopyright_primary_image(String copyright_primary_image) {
        this.copyright_primary_image = copyright_primary_image;
    }
}
