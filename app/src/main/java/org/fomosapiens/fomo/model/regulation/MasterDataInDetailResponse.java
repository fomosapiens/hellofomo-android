package org.fomosapiens.fomo.model.regulation;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MasterDataInDetailResponse {

    @SerializedName("specification")
    @Expose
    private List<RegulationListElementData> specification = null;
    @SerializedName("level")
    @Expose
    private List<RegulationListElementData> level = null;
    @SerializedName("legal_nature")
    @Expose
    private List<RegulationListElementData> legalNature = null;
    @SerializedName("authority")
    @Expose
    private List<RegulationListElementData> authority = null;
    @SerializedName("field_of_laws")
    @Expose
    private List<RegulationListElementData> fieldOfLaws = null;
    @SerializedName("tags")
    @Expose
    private List<TagData> tags;

    public MasterDataInDetailResponse() {
        tags = null;
    }

    public List<RegulationListElementData> getSpecification() {
        return specification;
    }

    public void setSpecification(List<RegulationListElementData> specification) {
        this.specification = specification;
    }

    public List<RegulationListElementData> getLevel() {
        return level;
    }

    public void setLevel(List<RegulationListElementData> level) {
        this.level = level;
    }

    public List<RegulationListElementData> getLegalNature() {
        return legalNature;
    }

    public void setLegalNature(List<RegulationListElementData> legalNature) {
        this.legalNature = legalNature;
    }

    public List<RegulationListElementData> getAuthority() {
        return authority;
    }

    public void setAuthority(List<RegulationListElementData> authority) {
        this.authority = authority;
    }

    public List<RegulationListElementData> getFieldOfLaws() {
        return fieldOfLaws;
    }

    public void setFieldOfLaws(List<RegulationListElementData> fieldOfLaws) {
        this.fieldOfLaws = fieldOfLaws;
    }

    public List<TagData> getTags() {
        return tags;
    }

    public void setTags(List<TagData> tags) {
        this.tags = tags;
    }
}
