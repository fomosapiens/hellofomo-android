package org.fomosapiens.fomo.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.fomosapiens.fomo.model.News;

import java.util.List;

/**
 * Created by PC162 on 8/11/2017.
 */

public class ResponseListNews {
    @Expose
    @SerializedName("errors")
    protected boolean errors;

    @Expose
    @SerializedName("data")
    protected List<News> data;

    public List<News> getData() {
        return data;
    }
}
