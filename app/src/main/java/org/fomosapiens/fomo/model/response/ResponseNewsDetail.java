package org.fomosapiens.fomo.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.fomosapiens.fomo.model.News;
import org.fomosapiens.fomo.model.NewsDetail;

import java.util.List;

/**
 * Created by PC162 on 8/14/2017.
 */

public class ResponseNewsDetail {
    @Expose
    @SerializedName("errors")
    protected boolean errors;

    @Expose
    @SerializedName("data")
    protected NewsDetail data;

    public NewsDetail getData() {
        return data;
    }
}
