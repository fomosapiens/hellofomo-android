package org.fomosapiens.fomo.net.entity;

/**
 * Created by PC162 on 8/8/2017.
 */

public class RefreshTokenEntity {


    private Builder builder;

    public RefreshTokenEntity(Builder builder) {
        this.builder = builder;
    }


    public static class Builder {

        private String client_id;
        private String client_secret;
        private String refresh_token;

        public Builder setClient_id(String client_id) {
            this.client_id = client_id;
            return this;
        }

        public Builder setClient_secret(String client_secret) {
            this.client_secret = client_secret;
            return this;
        }

        public Builder setRefresh_token(String refresh_token) {
            this.refresh_token = refresh_token;
            return this;
        }

        public RefreshTokenEntity build(){
            validate();
            return new RefreshTokenEntity(this);
        }

        private void validate(){}

    }
}
