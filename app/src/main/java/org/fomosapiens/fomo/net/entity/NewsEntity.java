package org.fomosapiens.fomo.net.entity;

/**
 * Created by PC162 on 8/8/2017.
 */

public class NewsEntity {

    private NewsEntity.Builder builder;

    public NewsEntity(NewsEntity.Builder builder) {
        this.builder = builder;
    }


    public static class Builder {

        private String id;
        private String lang;

        public NewsEntity.Builder setId(String id) {
            this.id = id;
            return this;
        }

        public NewsEntity.Builder setLang(String lang) {
            this.lang = lang;
            return this;
        }

        public NewsEntity build(){
            return new NewsEntity(this);
        }

        private void validate(){}

    }
}
