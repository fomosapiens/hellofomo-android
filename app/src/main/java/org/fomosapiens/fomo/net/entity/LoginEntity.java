package org.fomosapiens.fomo.net.entity;


import org.fomosapiens.fomo.view.ui.authen.LoginActivity;

/**
 * Created by PC162 on 8/8/2017.
 */

public class LoginEntity {

    private String client_id;
    private String  client_secret;
    private String  device_id;
    private String  fcm_token;
    private String  agent;
    private String username;
    private String password;

    public LoginEntity(String client_id, String client_secret, String device_id, String fcm_token, String agent) {
        this.client_id = client_id;
        this.client_secret = client_secret;
        this.device_id = device_id;
        this.fcm_token = fcm_token;
        this.agent = agent;
    }

    public LoginEntity(String username, String password, String client_id, String client_secret) {
        this.username = username;
        this.password = password;
        this.client_id = client_id;
        this.client_secret = client_secret;
    }

}
