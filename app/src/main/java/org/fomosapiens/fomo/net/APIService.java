package org.fomosapiens.fomo.net;

import org.fomosapiens.fomo.model.event.HistoryResponse;
import org.fomosapiens.fomo.model.regulation.GeneralResponse;
import org.fomosapiens.fomo.model.regulation.MasterDataResponse;
import org.fomosapiens.fomo.model.regulation.RegisterResponse;
import org.fomosapiens.fomo.model.regulation.RegulationListData;
import org.fomosapiens.fomo.model.regulation.RegulationListResponse;
import org.fomosapiens.fomo.model.regulation.SingleRegulationResponse;
import org.fomosapiens.fomo.model.response.ForgotPassword;
import org.fomosapiens.fomo.model.ForgotPasswordEnity;
import org.fomosapiens.fomo.model.Kontakt;
import org.fomosapiens.fomo.model.Login;
import org.fomosapiens.fomo.model.response.ResponseCategory;
import org.fomosapiens.fomo.model.response.ResponseDashboard;
import org.fomosapiens.fomo.model.response.ResponseFont;
import org.fomosapiens.fomo.model.response.ResponseListNews;
import org.fomosapiens.fomo.model.response.ResponseNewsDetail;
import org.fomosapiens.fomo.model.service.LetterResponse;
import org.fomosapiens.fomo.model.service.ServiceResponse;
import org.fomosapiens.fomo.net.entity.LoginEntity;
import org.fomosapiens.fomo.net.entity.RefreshTokenEntity;

import io.reactivex.Observable;
import io.reactivex.annotations.Nullable;
import okhttp3.ResponseBody;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Streaming;
import retrofit2.http.Url;


/**
 * Created by PC162 on 8/7/2017.
 */

public interface APIService {

    @Headers({
            "Content-Type: application/json",
            "Accept: application/json"
    })
    @POST("api/v1/auth")
    Observable<Login> login(@Body LoginEntity loginEntity);

    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST("api/v1/customer/auth")
    Observable<Login> loginWithPassword(@Body LoginEntity loginEntity);

    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @GET("api/v1/customer/logout")
    Observable<ResponseBody> logout(@Header("Authorization") String authorization);

    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST("api/v1/auth/refresh-token")
    Observable<Login> refreshToken(@Body RefreshTokenEntity refreshTokenEntity);

    @Headers({"Accept: application/json", "Content-Type: application/json",})
    @GET("api/v1/layout/dashboard")
    Observable<ResponseDashboard> getListDashBoard(@Header("Authorization") String authorization);

    @Headers({"Accept: application/json", "Content-Type: application/json",})
    @GET("api/v1/layout/news")
    Observable<ResponseDashboard> getListNewsHolder(@Header("Authorization") String authorization);

    @Headers({"Accept: application/json", "Content-Type: application/json",})
    @GET("api/v1/layout/category")
    Observable<ResponseDashboard> getListCategoryHolder(@Header("Authorization") String authorization);

    @Headers({"Accept: application/json", "Content-Type: application/json",})
    @GET("api/v1/categories")
    Observable<ResponseCategory> getListCategories(@Header("Authorization") String authorization);


    @Headers({"Accept: application/json", "Content-Type: application/json",})
    @GET("api/v1/news")
    Observable<ResponseListNews> dashboardAndNewsGetListNewsWithCategoryOrTag(
            @Header("Authorization") String authorization,
            @Query("lang") String valueLanguage,
            @Nullable @Query("top_news") int valueTopNews,
            @Nullable @Query("categories") String valueCategory,
            @Nullable @Query("tags") String valueTag,
            @Query("offset") String dataOffset,
            @Query("limit") String valueLimit);

    @Headers({"Accept: application/json", "Content-Type: application/json",})
    @GET("api/v1/news")
    Observable<ResponseListNews> getListNewsArticle(
            @Header("Authorization") String authorization,
            @Query("lang") String valueLanguage,
            @Query("categories") String valueCategory,
            @Query("limit") String valueLimit,
            @Query("page") String valuePage);

    @Headers({"Accept: application/json", "Content-Type: application/json",})
    @GET("api/v1/news")
    Observable<ResponseListNews> getListNewsArticleWithOutCategory(
            @Header("Authorization") String authorization,
            @Query("lang") String valueLanguage,
            @Query("limit") String valueLimit);

    @Headers({"Accept: application/json", "Content-Type: application/json",})
    @GET("api/v1/news/{newsId}")
    Observable<ResponseNewsDetail> getSingleNewsById(@Header("Authorization") String authorization,
                                                     @Path("newsId") String newsId,
                                                     @Query("lang") String valueLanguage);

    @Headers({"Accept: application/json", "Content-Type: application/json",})
    @GET("api/v1/layout/contact")
    Observable<Kontakt> getKontakt(@Header("Authorization") String authorization);

    @Headers({"Accept: application/json", "Content-Type: application/json",})
    @GET("api/v1/layout/agb")
    Observable<Kontakt> getAGB(@Header("Authorization") String authorization);

    @Headers({"Accept: application/json", "Content-Type: application/json",})
    @GET("api/v1/layout/impressum")
    Observable<Kontakt> getImpressum(@Header("Authorization") String authorization);

    @Headers({"Accept: application/json", "Content-Type: application/json",})
    @GET("api/v1/layout/privacy")
    Observable<Kontakt> getdateschuz(@Header("Authorization") String authorization);

    // download file attachment
    @GET
    @Streaming
    Observable<ResponseBody> getImage(@Url String url);

    @Headers({"Accept: application/json", "Content-Type: application/json",})
    @GET("api/v1/settings/font")
    Observable<ResponseFont> getFont(@Header("Authorization") String authorization);


    @Headers({"Accept: application/json", "Content-Type: application/json"})
    @POST("api/v1/customer/forgot-password")
    Observable<ForgotPassword> sendEmail(@Header("Authorization") String authorization,
                                         @Body ForgotPasswordEnity enity);

    @Headers({"Accept: application/json", "Content-Type: application/json"})
    @POST("api/v1/customer/update-password")
    Observable<ForgotPassword> updatePassword(@Header("Authorization") String authorization,
                                              @Body ForgotPasswordEnity enity);

    @Headers({"Accept: application/json", "Content-Type: application/json"})
    @GET("api/v1/customer/regulations")
    Observable<RegulationListResponse> getRegulationList(@Header("Authorization") String authorization,
                                                         @Nullable @Query("page") int page,
//                                                         @Nullable @Query("limit") int limit,
                                                         @Nullable @Query("specification_id") String specification_id,
                                                         @Nullable @Query("level_id") String level_id,
                                                         @Nullable @Query("legal_nature_id") String legal_nature_id,
                                                         @Nullable @Query("authority_id") String authority_id,
                                                         @Nullable @Query("register_id") String register_id,
                                                         @Nullable @Query("field_law_id") String field_law_id,
                                                         @Nullable @Query("keyword") String keyword);

    @Headers({"Accept: application/json", "Content-Type: application/json"})
    @GET("api/v1/regulations/{id}")
    Observable<SingleRegulationResponse> getRegulationDetail(@Header("Authorization") String authorization,
                                                             @Path("id") String id);

    @Headers({"Accept: application/json", "Content-Type: application/json"})
    @GET("api/v1/regulations/master-data")
    Observable<MasterDataResponse> getMasterData(@Header("Authorization") String authorization,
                                                 @Query("register_id") String registerId);

    @Headers({"Accept: application/json", "Content-Type: application/json"})
    @GET("api/v1/settings/general")
    Observable<GeneralResponse> getGeneralSetting(@Header("Authorization") String authorization);

    @Headers({"Accept: application/json", "Content-Type: application/json"})
    @GET("api/v1/customer/registers")
    Observable<RegisterResponse> getRegulationRegisterList(
            @Header("Authorization") String authorization,
            @Nullable @Query("page") int page,
            @Nullable @Query("offset") int offset,
            @Nullable @Query("limit") int limit,
            @Nullable @Query("lang") String lang
    );

    @Headers({"Accept: application/json", "Content-Type: application/json"})
    @GET("api/v1/customer/histories")
    Observable<HistoryResponse> getHistoryList(@Header("Authorization") String authorization,
                                               @Nullable @Query("page") int page,
                                               @Nullable @Query("specification_id") String specification_id,
                                               @Nullable @Query("level_id") String level_id,
                                               @Nullable @Query("legal_nature_id") String legal_nature_id,
                                               @Nullable @Query("authority_id") String authority_id,
                                               @Nullable @Query("register_id") String register_id,
                                               @Nullable @Query("field_law_id") String field_law_id,
                                               @Nullable @Query("keyword") String keyword);

    @Headers({"Accept: application/json", "Content-Type: application/json"})
    @GET("api/v1/companies/letters")
    Observable<LetterResponse> getLetters(@Header("Authorization") String authorization,
                                          @Nullable @Query("filter_option") int filterOption);

    @Headers({"Accept: application/json", "Content-Type: application/json"})
    @GET("api/v1/companies")
    Observable<ServiceResponse> getListService(@Header("Authorization") String authorization,
                                               @Nullable @Query("filter_option") int filterOption,
                                               @Nullable @Query("first_letter") String firstLetter);

}
