package org.fomosapiens.fomo.net;

import android.util.Log;

import org.fomosapiens.fomo.SharePre;
import org.fomosapiens.fomo.Utils;
import org.fomosapiens.fomo.model.DashBoard;
import org.fomosapiens.fomo.model.response.ResponseDashboard;
import org.fomosapiens.fomo.model.response.ResponseListNews;
import org.fomosapiens.fomo.model.type.Layout;
import org.fomosapiens.fomo.view.interfacelistener.OnResponseGetDashBoardListener;
import org.fomosapiens.fomo.view.interfacelistener.OnResponseGetListNewsListener;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by PC162 on 8/22/2017.
 */

public class APIHelper {

    private static APIService service = APIUtils.getAPIService();

    public static void getListDashboard(final OnResponseGetDashBoardListener dashBoardListener) {
        String accessToken = Utils.getAccessToken();
        service.getListDashBoard("Bearer " + accessToken)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<ResponseDashboard>() {
                    @Override
                    public void onNext(@NonNull ResponseDashboard responseDashboard) {
                        dashBoardListener.OnRequestSuccess(responseDashboard.getData());
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        dashBoardListener.OnRequestError(e);
                    }

                    @Override
                    public void onComplete() {
                        Log.e("DashBoardActivity", "onComplete");
                    }
                });
    }

    public static void getListNewsWithCategoryOrTag(final DashBoard dashBoard, final OnResponseGetListNewsListener onResponseGetListNewsListener){
        String accessToken = Utils.getAccessToken();
        String valueCategory;
        String valueTag;
        String valueType = dashBoard.getType_value();
        String fieldType = dashBoard.getType();
        String category = dashBoard.getCategory();
        int valueTopNews = Integer.parseInt(dashBoard.getTop_news());
        if (category != null && !category.equals("0")) {
            valueCategory = category;
            valueTag = null;
        } else if (fieldType.equals("category") && !valueType.equals("null") && !valueType.equals("")){
            valueCategory = valueType;
            valueTag = null;
        } else if (fieldType.equals("tag")){
            valueCategory = null;
            valueTag = valueType;
        } else {
            valueCategory = null;
            valueTag = null;
        }

        String valueLanguage = Utils.getLanguage();
        String valueLimit = dashBoard.getData_count();
        String valueOffSet = dashBoard.getData_offset();

        service.dashboardAndNewsGetListNewsWithCategoryOrTag("Bearer " + accessToken, valueLanguage, valueTopNews, valueCategory, valueTag, valueOffSet, valueLimit)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<ResponseListNews>() {
                    @Override
                    public void onNext(@NonNull ResponseListNews responseListNews) {
                        onResponseGetListNewsListener.OnRequestSuccess(responseListNews.getData(), dashBoard);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        onResponseGetListNewsListener.OnRequestError(e);
                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }

    public static void getListNewsHolder(final OnResponseGetDashBoardListener dashBoardListener){
        String accessToken = Utils.getAccessToken();
        service.getListNewsHolder("Bearer " + accessToken)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<ResponseDashboard>() {
                    @Override
                    public void onNext(@NonNull ResponseDashboard responseDashboard) {
                        dashBoardListener.OnRequestSuccess(responseDashboard.getData());
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        dashBoardListener.OnRequestError(e);
                    }

                    @Override
                    public void onComplete() {
                        Log.e("NewsActivity", "onComplete");
                    }
                });

    }

    public static void getListCategoryHolder(final OnResponseGetDashBoardListener dashBoardListener) {
        String accessToken = Utils.getAccessToken();
        service.getListCategoryHolder("Bearer " + accessToken)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<ResponseDashboard>() {
                    @Override
                    public void onNext(ResponseDashboard responseDashboard) {
                        dashBoardListener.OnRequestSuccess(responseDashboard.getData());
                    }

                    @Override
                    public void onError(Throwable e) {
                        dashBoardListener.OnRequestError(e);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

}
